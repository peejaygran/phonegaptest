/*jshint undef: false*/

// Mobilizr Import Dependancies
// Hugo McPhee 2015

moment().format();

// Load native UI library
//var gui = require('nw.gui'); //or global.window.nwDispatcher.requireNwGui() (see https://github.com/rogerwang/node-webkit/issues/707)
// Get the current window
//var win = gui.Window.get();
//win.setAlwaysOnTop(true);
//win.showDevTools();

/*global famous*/
// import dependencies
var Engine = famous.core.Engine;
var Modifier = famous.core.Modifier;
var Transform = famous.core.Transform;
var ImageSurface = famous.surfaces.ImageSurface;

var Entity                  = famous.core.Entity;

var StateModifier      = famous.modifiers.StateModifier;
var Draggable          = famous.modifiers.Draggable;
var Transitionable     = famous.transitions.Transitionable;
var SnapTransition     = famous.transitions.SnapTransition;
var WallTransition     = famous.transitions.WallTransition;
var Easing             = famous.transitions.Easing;
var View               = famous.core.View;
var Modifier           = famous.core.Modifier;
var Surface            = famous.core.Surface;
var Transform          = famous.core.Transform;
var InputSurface       = famous.surfaces.InputSurface;
var TextareaSurface    = famous.surfaces.TextareaSurface;
var ContainerSurface   = famous.surfaces.ContainerSurface;
var ImageSurface       = famous.surfaces.ImageSurface;
var Scrollview         = famous.views.Scrollview;
var Timer              = famous.utilities.Timer;
var Easing             = famous.transitions.Easing;
var RenderNode         = famous.core.RenderNode;
var RenderController   = famous.views.RenderController;
var MouseSync          = famous.inputs.MouseSync;
var ModifierChain      = famous.modifiers.ModifierChain;
var Flipper            = famous.views.Flipper;
//var FastClick          = famous.inputs.FastClick;
//var SizeAwareView = famous.views.SizeAwareView;

var LayoutController = famousflex.LayoutController;
var CollectionLayout = famousflex.layouts.CollectionLayout;
var DatePicker = famousflex.widgets.DatePicker;

var FlexScrollView = famousflex.FlexScrollView;
var ListLayout = famousflex.layouts.ListLayout;

//    var SequentialLayout = famous.views.SequentialLayout;

//================================================================================-
//region Img Cache
//|-1|-233|-598|864.23|520.06|0--------------------------------------------------------------------------------------------------------------------------~



// write log to console
ImgCache.options.debug = true;

// Set the image cache filepath 
ImgCache.options.localCacheFolder = "./userAndActivityPictures";

ImgCache.init(function () {
    
    // This part gets the imagePath from the mobilizr server, then saves it's own image path inside the app whenever it sends (or gets) the activity image from the server, everytime it uses the imgCache file , it checks if it exists, if it does then it uses it, if it doesn't then it re-gets and makes the cache of the image from the mobilizr server
    
    //    alert('ImgCache init: success!');

    // from within this function you're now able to call other ImgCache methods
    // or you can wait for the ImgCacheReady event
    
    
    // This part will read in the file url (either based on the activity (or user?) id or the imagePath stored in the database for that entry of a user or an activity)
    // Instead the new filepath will be stored in the in-app database for that thing
    // Thiscould be a function called cacheActivityImage (and there could be one called cacheUserImage) and this function is run on all new mobilizr users that have been synced, and the activity version is run on all new activities that are synced
    ImgCache.cacheFile('./id_1112.png', function () {
        console.log("Image Cached");
        
        ImgCache.getCachedFileURL('./id_1112.png', function (theOldPath, theNewPath) {
            if (theNewPath) {
                console.log("It was a success This is the file?");
                console.log(theNewPath);
                // already cached
                //    ImgCache.useCachedFile(target);
            } else {
                console.log("It was not a success");
            }
        });
    });


    //    ImgCache.options.localCacheFolder = "./otherPictures";
    ImgCache.cacheFile('https://i1.sndcdn.com/artworks-000016070329-w3y20g-t120x120.jpg', function () {
        //      ImgCache.useCachedFile(target);
        console.log("Image Cached2");
    });
    

}, function () {
    alert('ImgCache init: error! Check the log for errors');
});


//endregion
//================================================================================-

