"use strict";


// App into generic modules and app sepecifc construction

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Initialise the app and device
/////////////////////////////////////////////////////////////////////////////////////////////////////////

//====================================================-
//Development Shortcuts region
//|47|-1335|-898|500|200|1----------------------------------------------------------------------------------------------~
var gui = require('nw.gui');

var win = gui.Window.get();

// Development buttons
window.addEventListener("keydown", handleKeyDown, false);

function handleKeyDown(evt) {
    if (evt.keyCode == 112) { //f1
        win.showDevTools();
    }
    if (evt.keyCode == 113) { //f2
        win.close();
        win.closeDevTools();
    } else if (evt.keyCode == 116) { //f5
        win.reload();
    }

}






//endregion
//====================================================-


//====================================================-
//Img Cache region
//|-1|-233|-598|864.23|520.06|0----------------------------------------------------------------------------------------------~



// write log to console
ImgCache.options.debug = true;

// Set the image cache filepath 
ImgCache.options.localCacheFolder = "./userAndActivityPictures";

ImgCache.init(function () {

    // This part gets the imagePath from the mobilizr server, then saves it's own image path inside the app whenever it sends (or gets) the activity image from the server, everytime it uses the imgCache file , it checks if it exists, if it does then it uses it, if it doesn't then it re-gets and makes the cache of the image from the mobilizr server

    //    alert('ImgCache init: success!');

    // from within this function you're now able to call other ImgCache methods
    // or you can wait for the ImgCacheReady event


    // This part will read in the file url (either based on the activity (or user?) id or the imagePath stored in the database for that entry of a user or an activity)
    // Instead the new filepath will be stored in the in-app database for that thing
    // Thiscould be a function called cacheActivityImage (and there could be one called cacheUserImage) and this function is run on all new mobilizr users that have been synced, and the activity version is run on all new activities that are synced
    ImgCache.cacheFile('./id_1112.png', function () {
        console.log("Image Cached");

        ImgCache.getCachedFileURL('./id_1112.png', function (theOldPath, theNewPath) {
            if (theNewPath) {
                console.log("It was a success This is the file?");
                console.log(theNewPath);
                // already cached
                //    ImgCache.useCachedFile(target);
            } else {
                console.log("It was not a success");
            }
        });
    });


    //    ImgCache.options.localCacheFolder = "./otherPictures";
    ImgCache.cacheFile('https://i1.sndcdn.com/artworks-000016070329-w3y20g-t120x120.jpg', function () {
        //      ImgCache.useCachedFile(target);
        console.log("Image Cached2");
    });


}, function () {
    alert('ImgCache init: error! Check the log for errors');
});






//endregion
//====================================================-


//====================================================-
//Device Specific Initialisation region
//|1|-133|-555|614.25|482.11|1----------------------------------------------------------------------------------------------~
// Wait for device API libraries to load

//NOTE This is the device ready event listener, it may be better to have it somewhere else, if its good

// Handle the back button
function onBackKeyDown() {
    alert("Back Button Pressed");
}

Engine.on('load', function () {
    alert("Hello");
    scrollers.AllUpdates.scrollView.goToFirstPage();
});

//endregion
//====================================================-


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Defining parts of the app
/////////////////////////////////////////////////////////////////////////////////////////////////////////


//====================================================-
//Actively Checked Global variables region
//|22|1956|4173|500|500|1----------------------------------------------------------------------------------------------~
// Actively Checked and Changed variables
var hasSignedUp = false;
var scrollviewHasBeenTurnedOff = false;
var horizontalPageScrollHasBeenTurnedOff = false;
var sideMenuIsOut = false;
var canSubmitActivity = false;
var isScrollingVertically = false;
var isScrollingHorizontally = true;
var touchDownPosition;
var touchReleasePosition;
var touchDownTime;
var touchReleaseTime;
var startScrollPosition;
var startedScrollingHorizontally = false;
var maxStatusLetters = 64;
var theTimezoneOffset = GetTimezoneOffset();
// this is to determine if the "Done button" on the profile page goes back to, at the start it's "signup", then its usually "settings" going back to statuses/activity might be different
var profileCameFrom = "signup";

// Development Variables
var shouldUseMouse = true;

// What is read to see which activity the invitations are being sent out for, or which activity is being edited 
var currentlyEditingActivityId = null;

// These are the initial variables for adding a number to a temporary activity id
var globalTempActivityIdCounter = 0;

var isAValidPhoneNumberBoolean = false;

var activityTitleIsFocused = false;
var wherePartIsFocused = false;
var phoneNumberInputIsFocused = false;

// TODO could organise the checking variables into groups with objects
// like focusChecks.validNumber
// or possibly store the isFocused for every text input?
// Or have the object variable be a reference to the same isFocused property of the text input if it's added to any of the textInputs creation functions (as in any of the text or number input creation functions)?


var serverScriptUrl = "http://localhost/mobilizrMainServerScript.php";



// Html Helper Variables

    var stopFurtherClicks = "if (!e) var e = window.event; e.cancelBubble = true;if (e.stopPropagation) e.stopPropagation();if (e.preventDefault) e.preventDefault();";



//endregion
//====================================================-


//====================================================-
//Temporary Information Objects region
//|23|2687|3834|500|500|1----------------------------------------------------------------------------------------------~
// This is what gets edited from the create an activity pages
var temporaryActivityObject = {
    id: null,
    name: null,
    category: null,
    description: null,
    videoLink: null,
    picturePath: null,
    pictureDataUrl: null,
    createdBy: null,
    dateCreated: null,
    location: null,
    timeStarting: null,
    timeEnding: null,
    target: null,
    peopleJoined: null,
    peopleInvited: null,
    privacy: null,
    allowAnonymous: null,
    deletionDays: null
};

// This is what's edited from the signup pages
var temporaryUserObject = {
    id: null,
    joined: null,
    laston: null,
    p2: null,
    p1: null,
    email: null,
    picturePath: null,
    pictureDataUrl: null,
    name: null,
    dateJoined: null,
    lastSubscriptionDate: null,
    subscriptionType: null,
    activitiesCreated: null,
    activitiesJoined: null,
    hideActivityData: null,
    lastUpdated: null,
    p1Updated: null,
    p2Updated: null,
    emailUpdated: null,
    pictureUpdated: null,
    nameUpdated: null
};

// This is the information about the user
var theUser = {
    id: 7,
    joined: Math.floor(Date.now() / 1000),
    laston: Math.floor(Date.now() / 1000),
    p2: null,
    p1: "(03) 7117 1803",
    email: "Morbi@Vestibulumante.net",
    picturePath: "./Images/displayPicTest.png",
    name: "Dillon Fellow",
    dateJoined: Math.floor(Date.now() / 1000),
    lastSubscriptionDate: null,
    subscriptionType: null,
    activitiesCreated: 0,
    activitiesJoined: 0,
    hideActivityData: 0,
    lastUpdated: Math.floor(Date.now() / 1000),
    p1Updated: Math.floor(Date.now() / 1000),
    p2Updated: Math.floor(Date.now() / 1000),
    emailUpdated: Math.floor(Date.now() / 1000),
    pictureUpdated: Math.floor(Date.now() / 1000),
    nameUpdated: Math.floor(Date.now() / 1000)
};






//endregion
//====================================================-


//====================================================-
//Variable Names (Enumerators) region
//|24|2672|4414|500|500|1----------------------------------------------------------------------------------------------~
// These are numbers to represent each of the generic scroll bundle types (enums)
var genericScrollElements = Object.freeze({
    scrollSpacer: 0,
    scrollDescription: 1,
    accordionDescription: 2,
    textInput: 3,
    checkbox: 4,
    pictureCheckbox: 5,
    scrollTitle: 6,
    numberInput: 7,
    bullet: 8,
    textArea: 9,
    addAPicture: 10,
    textAccordion: 11,
    groupAccordion: 12,
    cancelOk: 13
});






//endregion
//====================================================-


//====================================================-
//Size Variables region
//|10|2758|-430|591.54|520.25|1----------------------------------------------------------------------------------------------~
// The themes font sizes
var fontSizes = {
    mega: '2.667em',
    large: '1.833em',
    medium: '1.500em',
    small: '1.167em',
    smaller: '0.917em',
    tiny: '0.667em'
};

// Sizes variables
var originalWidth = window.innerWidth;
var originalHeight = window.innerHeight;
var sideBarWidth = originalWidth - (originalWidth * 0.35);

var defaultFontWidth = GetDefaultFontSize()[0];
var defaultFontHeight = GetDefaultFontSize()[1];

// this is the height of the top bar on the main pages (the bar with the title of the current page in it), many different part sizes are based on this height
var standardScrollElementHeight = 44;
var smallScrollElementHeight = 32;
// Below are previously used values for this height, at the moment its a fixed height
//var standardScrollElementHeight = (0.085* originalHeight);
// var standardScrollElementHeight = (3.7 * defaultFontHeight);
// var standardScrollElementHeight = (0.085 * 550);


// atm it's in pixels and for the padding width and height but it could be in a percentage of the width or height?
var scrollingBoxPadding = 20;
var activityUpdatesBarHeight = standardScrollElementHeight * 0.8;
var shadowSize = defaultFontHeight * 0.39;
var textButtonWidth = 80;
var textButtonHeight = defaultFontHeight * 3;
var defaultIconSize = 40;
var tabsBarHeight = 40;
// var titleBarHeight = standardScrollElementHeight * 0.53;
var titleBarHeight = 32;
var sendStatusBrickHeight = originalHeight * 0.3;


// Picture Theming Templates / Default Values

// Resolution of an Activity Picture
var requestedWidthActivity = 500;
var requestedHeightActivity = 385;

// Resolution of a Users Profile Picture
var requestedWidthUser = 300;
var requestedHeightUser = 300;

// These are to get the corrosponding width based on height and vice versa, using the resolution ratio for activity images that was set (500*385)
var heightToWidthRatio = (requestedHeightActivity / requestedWidthActivity);
var widthToHeightRatio = (requestedWidthActivity / requestedHeightActivity);

// These are to get the width to heigh or height to width ratios of the screen
var screenHeightWidthRatio = (originalHeight / originalWidth);
var screenWidthHeightRatio = (originalWidth / originalHeight);

var averageScreenSize = (originalWidth + originalHeight) * 0.5;

var activityBoxWidth = averageScreenSize * 0.25;
var activityBoxHeight = originalHeight * 0.17;

var defaultPaddingVertical = 5;
var defaultPaddingHorizontal = 5;

var tempPrivacyCategoryBarHeight = GetNumberFromString(fontSizes.tiny) * 30.5;
// var standardScrollElementHeight = GetNumberFromString(fontSizes.smaller) * 70;

var targetBarSize = (originalWidth * 0.5) - (originalWidth * 0.1);






//endregion
//====================================================-


//====================================================-
//Transitions region
//|11|3538|-443|500|500|1----------------------------------------------------------------------------------------------~
// Transitions
Transitionable.registerMethod('snap', SnapTransition);

var snapTransition = {
    method: 'snap',
    period: 50,
    dampingRatio: 1, // 0.3 for springy elastic
    velocity: 0
};
var easeTransition = {
    duration: 50,
    curve: 'easeOut'
};
var easeTransitionQuick = {
    duration: 1,
    curve: 'linear'
};
var easeTransitionSlow = {
    duration: 250,
    curve: 'easeOut'
};

//endregion
//====================================================-


//====================================================-
//Colours region
//|12|3006|236|428.89|391.73|1----------------------------------------------------------------------------------------------~
// All the themes colours in the app
var themeColors = {
    main: '#90af3c',
    dark: '#222222',
    grey: '#606060',
    lightGrey: '#aaaaaa',
    light: '#ebebeb',
    darkGlass: 'rgba(0, 0, 0, 0.25)',
    lightGlass: 'rgba(235, 235, 235, 0.72)',
    greyHighlight: 'rgba(186, 186, 186, 0.34)',
    viewColour: '#fff'
};






//endregion
//====================================================-


//====================================================-
//Major App Containers region
//|14|2558|1472|500|500|1----------------------------------------------------------------------------------------------~


// These are the different sections of the app that can be shown by the main render controller
var appSections = {
    SignUpPages: null,
    CreateActivity: null,
    GettingStarted: null,
    MainPages: null,
    SideMenu: null,
    Contacts: null,
    SendMessage: null,
    ActivityPages: null,
    Settings: null
};
// These are the different views for different areas of the app, they are like the blank pages that get navigated to
var theViews = Object.freeze({
    SignUpPages: {
        // These are all just shown in the brick with its render controller
        IntroPanels: {
            view: null,
            id: 0
        },
        VerifyPhone: {
            view: null,
            id: 1
        },
        YourCountry: {
            view: null,
            id: 2
        },
        EnterCode: {
            view: null,
            id: 3
        },
        WelcomeEnter: {
            view: null,
            id: 4
        } //, 
        //        CustomizeProfile :  {view: null, id: 5} 
    },
    IntroPanelsPanels: {
        IntroPanel1: {
            view: null,
            id: 0
        },
        IntroPanel2: {
            view: null,
            id: 1
        },
        IntroPanel3: {
            view: null,
            id: 2
        }
    },
    GettingStarted: {
        IntroPanels: {
            view: null,
            id: 0
        },
        VerifyPhone: {
            view: null,
            id: 1
        },
        YourCountry: {
            view: null,
            id: 2
        },
        EnterCode: {
            view: null,
            id: 3
        },
        WelcomeEnter: {
            view: null,
            id: 4
        },
        CustomizeProfile: {
            view: null,
            id: 5
        }
    },
    MainPages: {
        Updates: {
            view: null,
            id: 0,
            isDisabled: false
        },
        Invites: {
            view: null,
            id: 1,
            isDisabled: false
        },
        Joined: {
            view: null,
            id: 2,
            isDisabled: false
        },
        Created: {
            view: null,
            id: 3,
            isDisabled: false
        }
    },
    SideMenu: {
        view: null,
        id: 0
    },
    CreateActivity: {
        Info: {
            view: null,
            id: 0,
            isDisabled: null
        },
        Category: {
            view: null,
            id: 1,
            isDisabled: null
        },
        Privacy: {
            view: null,
            id: 2,
            isDisabled: null
        },
        //        Organizer :         {view: null, id: 3, isDisabled : null}, 
        Where: {
            view: null,
            id: 3,
            isDisabled: null
        },
        When: {
            view: null,
            id: 4,
            isDisabled: null
        },
        Visuals: {
            view: null,
            id: 5,
            isDisabled: null
        },
        Reach: {
            view: null,
            id: 6,
            isDisabled: null
        }
    },
    ContactsPages: {
        People: {
            view: null,
            id: 0
        },
        Groups: {
            view: null,
            id: 1
        },
    },
    SendMessage: {
        view: null,
        id: 0
    },
    ActivityPages: {
        Main: {
            view: null,
            id: 0
        },
        Picture: {
            view: null,
            id: 1
        },
        UpdateTabs: {
            view: null,
            id: 2
        },
        Info: {
            view: null,
            id: 3
        }
    },
    ActivityUpdates: {
        Friends: {
            view: null,
            id: 0,
            isDisabled: null
        },
        Everyone: {
            view: null,
            id: 1,
            isDisabled: null
        },
        Custom: {
            view: null,
            id: 2,
            isDisabled: null
        }
    },
    ActivitySmallBox: {
        MainContainer: {
            view: null,
            id: 0
        }
    },
    SettingsPages: {
        Settings: {
            view: null,
            id: 0
        },
        Profile: {
            view: null,
            id: 1
        },
        Interests: {
            view: null,
            id: 2
        },
        HelpFAQ: {
            view: null,
            id: 3
        },
        ContactUs: {
            view: null,
            id: 4
        },
        GettingStarted: {
            view: null,
            id: 5
        },
        Suggestion: {
            view: null,
            id: 6
        },
        Notifications: {
            view: null,
            id: 7
        },
        Privacy: {
            view: null,
            id: 8
        },
        PaymentInfo: {
            view: null,
            id: 9
        },
        ChangeNumber: {
            view: null,
            id: 10
        },
        OldNumber: {
            view: null,
            id: 11
        },
        NewNumber: {
            view: null,
            id: 12
        },
        DeleteAccount: {
            view: null,
            id: 13
        }
    },
    PresetStatusPages: {
        Time: {
            view: null,
            id: 0
        },
        Mood: {
            view: null,
            id: 1
        },
    },
    BrickContents: {
        GetStarted: null,
        Contacts: null,
        InOrOut: null,
        ActivateActivity: null,
        SelectedInLong: null,
        SelectedInShort: null,
        SelectedOut: null,
        ChooseMessageSettings: null,
        NewMessage: null,
        AreYouSureRemove: null,
        CantRequestStatus: null,
        SomeOneElsesProfile: null,
        Declined: null
    }
});
// The are the containers each part of the actvity section (the activity pages) are on
var activityContainers = {
    Main: {
        container: null,
        mod: null,
        id: 0,
        renderController: null
    },
    Info: {
        container: null,
        mod: null,
        id: 1,
        renderController: null
    },
    UpdatesBrickThing: {
        container: null,
        mod: null,
        id: 2
    },
    UpdatesBar: {
        container: null,
        mod: null,
        id: 3
    },
    UpdatesTabs: {
        container: null,
        mod: null,
        id: 4,
        renderController: null
    }
};






//endregion
//====================================================-


//====================================================-
//Scrollers region
//|15|2002|1463|500|500|1----------------------------------------------------------------------------------------------~
// These are all the scrollers in the app, the parts that can be scrolling vertically through
var scrollers = {
    SideBar: {},
    Invitations: {},
    AllUpdates: {},
    JoinedActivities: {},
    CreatedActivities: {},
    //    CreateActivity : {
    Info: {},
    Category: {},
    Privacy: {},
    Where: {},
    When: {},
    Visuals: {},
    Reach: {},
    //    }
    //    Settings : {
    Settings: {},
    Profile: {},
    Interests: {},
    HelpFAQ: {},
    ContactUs: {},
    GettingStarted: {},
    Suggestion: {},
    Notifications: {},
    SetPrivacy: {},
    PaymentInfo: {},
    ChangeNumber: {},
    OldNumber: {},
    NewNumber: {},
    DeleteAccount: {},
    //    }
    //    Activity Pages : {
    ActivityInfo: {},
    ActivityUpdatesEveryone: {},
    ActivityUpdatesFriends: {},
    ActivityUpdatesChannels: {},
    //    }
    //    Brick Pages : {
    Declined: {},
    ContactsPeople: {},
    ContactsGroups: {},
    PremadeStatuses: {},
    PremadeStatusesMood: {},
    //    }
    //    SignUp Pages : {
    VerifyPhone: {},
    EnterCode: {}
    //    }
};
doThisForAll(scrollers, setInitialDataForAllScrollers);

var defaultScrollerData = {
    //    width : originalWidth - (scrollingBoxPadding*2),
    //    height : originalHeight - (scrollingBoxPadding*2),
    width: originalWidth,
    height: originalHeight - (standardScrollElementHeight), //(scrollingBoxPadding*2),
    zDepth: 1,
    heightOffset: (standardScrollElementHeight)

};
//region scrollers Data
scrollers.SideBar.data = {
    width: sideBarWidth,
    height: originalHeight - (standardScrollElementHeight * 0.7),
    zDepth: 502,
    heightOffset: (standardScrollElementHeight * 0.7) * 0.5
};
scrollers.ActivityInfo.data = {
    width: originalWidth,
    height: originalHeight,
    zDepth: 1,
    heightOffset: 0
};
scrollers.ActivityUpdatesFriends.data = {
    width: originalWidth,
    //     height: originalHeight - ((tabsBarHeight * 1.4) + activityUpdatesBarHeight) ,
    height: originalHeight - ((tabsBarHeight) + activityUpdatesBarHeight),
    zDepth: 1,
    heightOffset: (tabsBarHeight * 1.4) * 0.5
};
scrollers.ActivityUpdatesEveryone.data = {
    width: originalWidth,
    height: originalHeight - ((tabsBarHeight) + activityUpdatesBarHeight),
    zDepth: 1,
    heightOffset: (tabsBarHeight * 1.4) * 0.5
};
scrollers.ActivityUpdatesChannels.data = {
    width: originalWidth,
    height: originalHeight - ((tabsBarHeight) + activityUpdatesBarHeight),
    zDepth: 1,
    heightOffset: (tabsBarHeight * 1.4) * 0.5
};
scrollers.ContactsPeople.data = {
    width: originalWidth,
    //     height: originalHeight - (defaultFontHeight * 8),
    height: originalHeight - (tabsBarHeight * 2),
    zDepth: 1,
    heightOffset: (defaultFontHeight * 4)
};
scrollers.ContactsGroups.data = {
    width: originalWidth,
    height: originalHeight - (defaultFontHeight * 8),
    zDepth: 1,
    heightOffset: (defaultFontHeight * 4)
};
scrollers.PremadeStatuses.data = {
    width: originalWidth,
    height: originalHeight * 0.8 - (defaultFontHeight * 4.5),
    zDepth: 1,
    heightOffset: (-defaultFontHeight * 4)
};
scrollers.PremadeStatusesMood.data = {
    width: originalWidth,
    height: originalHeight * 0.8 - (defaultFontHeight * 4.5),
    zDepth: 1,
    heightOffset: (-defaultFontHeight * 4)
};
// endregion






//endregion
//====================================================-


//====================================================-
//Draggables region
//|16|2018|2079|500|500|1----------------------------------------------------------------------------------------------~
var draggables = {
    MainPages: {},
    SideBar: {},
    CreateActivity: {},
    ActivityUpdates: {},
    ActivitySmallBox: {},
    ContactsPages: {},
    PresetStatusPages: {},
    IntroPanelsPanels: {}
};
doThisForAll(draggables, setInitialDataForAllDraggables);

// region draggables data setting
draggables.MainPages.views = theViews.MainPages;
draggables.CreateActivity.views = theViews.CreateActivity;
draggables.ActivityUpdates.views = theViews.ActivityUpdates;
draggables.ActivitySmallBox.views = theViews.ActivitySmallBox;
draggables.ContactsPages.views = theViews.ContactsPages;
draggables.PresetStatusPages.views = theViews.PresetStatusPages;
draggables.IntroPanelsPanels.views = theViews.IntroPanelsPanels;
// endregion

draggables.MainPages.data = {
    name: "MainPages",
    id: 0,
    pageWidth: originalWidth,
    pageHeight: originalHeight,
    //                                            pageHeight: originalWidth - 80,
    yOffset: 0,
    pageAmount: 3,
    currentPage: 0,
    associatedScrollers: [scrollers.AllUpdates, scrollers.Invitations, scrollers.JoinedActivities, scrollers.CreatedActivities],
    zDepth: 1,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 4,
    onEndFunction: function () {
        dotLogic.CheckAndUpdateDots(draggables.MainPages);
    }
};
draggables.SideBar.data = {
    name: "SideMenu",
    id: 1,
    pageWidth: sideBarWidth,
    pageHeight: originalHeight - standardScrollElementHeight,
    yOffset: 0,
    pageAmount: 1,
    currentPage: 0,
    associatedScrollers: [scrollers.SideBar],
    zDepth: 5000,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 2,
};
draggables.CreateActivity.data = {
    name: "CreationPages",
    id: 2,
    pageWidth: originalWidth,
    pageHeight: originalHeight,
    yOffset: /*standardScrollElementHeight*/ 0,
    pageAmount: Object.keys(theViews.CreateActivity).length - 1,
    currentPage: 0,
    associatedScrollers: [scrollers.Info, scrollers.Category, scrollers.Privacy, scrollers.Where, scrollers.When, scrollers.Visuals, scrollers.Reach],
    zDepth: 2,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 1,
    onEndFunction: function () {
        dotLogic.CheckAndUpdateDots(draggables.CreateActivity);
        if (draggables.CreateActivity.data.currentPage >= (draggables.CreateActivity.data.lockedPageLimit)) {
            draggables.CreateActivity.container.goToPreviousPage();
        }
        if (draggables.CreateActivity.data.currentPage >= 5) {
            if (canSubmitActivity === false) {
                canSubmitActivity = true;
                setTextButtonColor(textButtons.createSubmit, themeColors.main);
            }
        }
        if (draggables.CreateActivity.data.currentPage == 3 || draggables.CreateActivity.data.currentPage == 5) {
            BlurTheDateInputBoxes();
        }
        if (draggables.CreateActivity.data.currentPage == 4) {
            scrollers.When.scrollElements[0].setFocused();
        }
        if (draggables.CreateActivity.data.currentPage == 2 || draggables.CreateActivity.data.currentPage == 4) {
            scrollers.Where.scrollElements[0].textInput.blur();
        }
        if (draggables.CreateActivity.data.currentPage == 3) {
            scrollers.Where.scrollElements[0].textInput.focus();
        }
        if (draggables.CreateActivity.data.currentPage === 0) {
            scrollers.Info.scrollElements[0].textInput.focus();
        }
        if (draggables.CreateActivity.data.currentPage == 1) {
            scrollers.Info.scrollElements[0].textInput.blur();
        }
    }
};
draggables.ActivityUpdates.data = {
    name: "ActivityUpdates",
    id: 3,
    pageWidth: originalWidth,
    pageHeight: originalHeight - activityUpdatesBarHeight,
    yOffset: standardScrollElementHeight,
    pageAmount: Object.keys(theViews.ActivityUpdates).length - 1,
    currentPage: 0,
    associatedScrollers: [scrollers.ActivityUpdatesFriends, scrollers.ActivityUpdatesEveryone, scrollers.ActivityUpdatesChannels],
    zDepth: 2,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 2,
    onEndFunction: function () {
        dotLogic.CheckAndUpdateDots(draggables.ActivityUpdates);
        tabsBarLogic.CheckAndUpdateTabs(draggables.ActivityUpdates);
        if (draggables.ActivityUpdates.data.currentPage >= (draggables.ActivityUpdates.data.lockedPageLimit)) {
            draggables.ActivityUpdates.container.goToPreviousPage();
        }
    }
};
draggables.ActivitySmallBox.data = {
    name: "ActivitySmallBox",
    id: 4,
    pageWidth: activityBoxWidth,
    pageHeight: activityBoxHeight,
    yOffset: 0,
    pageAmount: 1,
    currentPage: 0,
    associatedScrollers: [],
    zDepth: 500,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 2,
    onEndFunction: function () {
        if (draggables.ActivitySmallBox.data.currentPage == 1) {
            if (draggables.ActivitySmallBox.draggable.getPosition()[0] < -(activityBoxWidth - 10)) {
                activityContainers.Main.MaximiseActivity();
            }
        }
    }
};
draggables.ContactsPages.data = {
    name: "Contacts",
    id: 5,
    pageWidth: originalWidth,
    pageHeight: originalHeight /* - (defaultFontHeight * 6)*/ ,
    yOffset: 0 /*(originalHeight - (originalHeight - (defaultFontHeight * 6)))*/ ,
    pageAmount: Object.keys(theViews.ContactsPages).length - 1,
    currentPage: 0,
    associatedScrollers: [scrollers.ContactsPeople, scrollers.ContactsGroups],
    zDepth: 2,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 2,
    onEndFunction: function () {
        dotLogic.CheckAndUpdateDots(draggables.ContactsPages);
        tabsBarLogic.CheckAndUpdateTabs(draggables.ContactsPages);
    }
};
draggables.PresetStatusPages.data = {
    name: "PresetStatusPages",
    id: 5,
    pageWidth: originalWidth,
    pageHeight: originalHeight /* - (defaultFontHeight * 6)*/ ,
    yOffset: 0 /*(originalHeight - (originalHeight - (defaultFontHeight * 6)))*/ ,
    pageAmount: Object.keys(theViews.PresetStatusPages).length - 1,
    currentPage: 0,
    associatedScrollers: [scrollers.PremadeStatuses, scrollers.PremadeStatusesMood],
    zDepth: 2,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 2,
    onEndFunction: function () {
        dotLogic.CheckAndUpdateDots(draggables.PresetStatusPages);
        tabsBarLogic.CheckAndUpdateTabs(draggables.PresetStatusPages);
    }
};
draggables.IntroPanelsPanels.data = {
    name: "IntroPanelsPanels",
    id: 6,
    pageWidth: originalWidth,
    pageHeight: originalHeight,
    yOffset: 0,
    pageAmount: 2,
    currentPage: 0,
    associatedScrollers: [],
    zDepth: 1,
    isDisabled: false,
    associatedDotGroup: null,
    associatedTabs: [],
    lockedPageLimit: 3,
    onEndFunction: function () {
        dotLogic.CheckAndUpdateDots(draggables.IntroPanelsPanels);
    }
};






//endregion
//====================================================-


//====================================================-
//Tabs Bars and Dot Groups region
//|17|2584|2093|500|500|1----------------------------------------------------------------------------------------------~
// Tabs Bars
var tabsBars = {
    ActivityUpdates: {},
    Contacts: {},
    PresetStatusPages: {}
};
doThisForAll(tabsBars, setInitialDataForTabsBars);
tabsBars.ActivityUpdates.data = {
    searchBarIsShowing: false,
    searchIsFocused: false
};
tabsBars.Contacts.data = {
    searchBarIsShowing: false,
    searchIsFocused: false
};

// Dot Groups
var dotGroups = {
    MainPages: {},
    CreateActivity: {},
    ActivityUpdates: {},
    Contacts: {},
    PresetStatusPages: {},
    IntroPanelsPanels: {}

};
doThisForAll(dotGroups, setInitialDataForDotGroups);






//endregion
//====================================================-


//====================================================-
//Texts region
//|18|1994|2617|500|500|1----------------------------------------------------------------------------------------------~
// Title Texts
var titleTexts = {
    MainPages: {
        Updates: {
            view: null,
            text: "Updates",
            id: 0
        },
        Invites: {
            view: null,
            text: "Invites",
            id: 1
        },
        Joined: {
            view: null,
            text: "Joined",
            id: 2
        },
        Created: {
            view: null,
            text: "Created",
            id: 3
        }
    },
    CreateActivity: {
        Info: {
            view: null,
            text: "Info",
            id: 0
        },
        Category: {
            view: null,
            text: "Category",
            id: 1
        },
        Privacy: {
            view: null,
            text: "Privacy",
            id: 2
        },
        //                            Organizer   : {view : null, text : "Organizer",   id : 3},
        Where: {
            view: null,
            text: "Where",
            id: 3
        },
        When: {
            view: null,
            text: "When",
            id: 4
        },
        Visuals: {
            view: null,
            text: "Visuals",
            id: 5
        },
        Reach: {
            view: null,
            text: "Reach",
            id: 6
        }
    },
    SettingsPages: {
        Settings: {
            view: null,
            text: "Settings",
            id: 0
        },
        Profile: {
            view: null,
            text: "Profile",
            id: 1
        },
        Interests: {
            view: null,
            text: "Interests",
            id: 2
        },
        HelpFAQ: {
            view: null,
            text: "Help/FAQ",
            id: 3
        },
        ContactUs: {
            view: null,
            text: "Contact Us",
            id: 4
        },
        GettingStarted: {
            view: null,
            text: "Getting Started",
            id: 5
        },
        Suggestion: {
            view: null,
            text: "Suggestion",
            id: 6
        },
        Notifications: {
            view: null,
            text: "Notifications",
            id: 7
        },
        Privacy: {
            view: null,
            text: "Privacy",
            id: 8
        },
        PaymentInfo: {
            view: null,
            text: "Payment Info",
            id: 9
        },
        ChangeNumber: {
            view: null,
            text: "Change Number",
            id: 10
        },
        OldNumber: {
            view: null,
            text: "Old Number",
            id: 11
        },
        NewNumber: {
            view: null,
            text: "New Number",
            id: 12
        },
        DeleteAccount: {
            view: null,
            text: "Delete Account",
            id: 13
        }
    },
    PresetStatusPages: {
        Time: {
            view: null,
            text: "Timing",
            id: 0
        },
        Mood: {
            view: null,
            text: "Mood",
            id: 1
        }
    },
    SignUpPages: {
        IntroPanels: {
            view: null,
            text: "",
            id: 0
        },
        VerifyPhone: {
            view: null,
            text: "Verify Phone",
            id: 1
        },
        EnterCode: {
            view: null,
            text: "Verify Phone",
            id: 2
        },
        YourCountry: {
            view: null,
            text: "Verify Phone",
            id: 3
        },
        WelcomeEnter: {
            view: null,
            text: "Welcome",
            id: 4
        }
    }
};

var categoryNames = [
    "Beach Party",
    "Bush/Warehouse Parties",
    "Club",
    "Concert, Performance",
    "Conference",
    "Election",
    "Festival",
    "Fair",
    "Flash mob",
    "Fundraiser",
    "Gala, Ball, Official Dinner",
    "Launch, Ceremony",
    "Meeting, Networking Meetup",
    "Nightclub, Pub",
    "Other",
    "Parade",
    "Petition",
    "Retreat, Camp",
    "Screening",
    "Seminar, Talk",
    "Show, Exhibition",
    "Signing, Appearance",
    "Social experiment",
    "Sports Game, Race",
    "Workshop, Class"
];






//endregion
//====================================================-


//====================================================-
//Picture Buttons region
//|19|1670|3157|682.82|641.16|1----------------------------------------------------------------------------------------------~
// Picture Buttons
var pictureButtons = {
    Menu: {},
    Submit: {},
    tempCloseBrick: {},
    LightMenu: {},
    UpdatesMenu: {},
    ThreeDots: {},
    Share: {},
    CloseCross: {},
    ShowPresets: {},
    AddActivity: {}
};
doThisForAll(pictureButtons, setInitialDataForButtons);
pictureButtons.Menu.data = {
    name: "Menu",
    id: 0,
    alignX: 40 / originalWidth,
    alignY: 20 / originalHeight,
    zPos: 700,
    imagePath: "./Images/menu55.svg",
    imageDisabledPath: "./Images/menu55inactive.svg",
    pressedFunction: function () {
        // This toggles the sidemenu from in to out or vice versa
        toggleSideBar();
    }
};
pictureButtons.Submit.data = {
    name: "Submit",
    id: 1,
    alignX: 1 - (20 / originalWidth),
    alignY: 20 / originalHeight,
    zPos: 20,
    imagePath: "./Images/send12.svg",
    imageDisabledPath: "./Images/send12inactive.svg",
    pressedFunction: function () {
        mainRenderController.show(appSections.MainPages);
        theBrick.HideBrick();
    }
};
pictureButtons.tempCloseBrick.data = {
    name: "CloseBrick",
    id: 2,
    alignX: 40 / originalWidth,
    alignY: 40 / originalHeight,
    zPos: 1200,
    imagePath: "./Images/send12.svg",
    imageDisabledPath: "./Images/send12inactive.svg",
    pressedFunction: function () {
        theBrick.HideBrick();
    }
};
pictureButtons.LightMenu.data = {
    name: "LightMenu",
    id: 3,
    alignX: 18 / originalWidth,
    alignY: ((standardScrollElementHeight * 0.63) * 0.5) / originalHeight, // this originalHeight part has gotta be the height of the activity info page instead
    zPos: 1200,
    imagePath: "./Images/menu55Light.svg",
    imageDisabledPath: "./Images/send12.svg",
    customSize: 30,
    pressedFunction: function () {
        toggleSideBar();
    }
};
pictureButtons.UpdatesMenu.data = {
    name: "UpdatesMenu",
    id: 3,
    alignX: 18 / originalWidth,
    alignY: 0.5, // this originalHeight part has gotta be the height of the activity info page instead
    zPos: 1200,
    imagePath: "./Images/menu55inactive.svg",
    imageDisabledPath: "./Images/send12.svg",
    customSize: 30,
    pressedFunction: function () {
        toggleSideBar();
    }
};
pictureButtons.ThreeDots.data = {
    name: "ThreeDots",
    id: 4,
    alignX: 1 - (((textButtonWidth * 2) / originalWidth) * 0.50),
    alignY: ((standardScrollElementHeight * 0.63) * 0.5) / originalHeight,
    zPos: 1200,
    imagePath: "./Images/show8.svg",
    imageDisabledPath: null,
    pressedFunction: function () {
        if (theBrick.isUp === false) {
            theBrick.MoveBrickUp(originalHeight * 0.5);
        } else {
            theBrick.HideBrick();
        }
    }
};
pictureButtons.Share.data = {
    name: "Share",
    id: 5,
    alignX: 0.25,
    alignY: 0.5,
    zPos: 1200,
    imagePath: "./Images/share11.svg",
    imageDisabledPath: null,
    customSize: 30,
    pressedFunction: function () {
        alert("This doesn't do stuff yet?");
    }
};
pictureButtons.CloseCross.data = {
    name: "CloseCross",
    id: 6,
    alignX: 1 - (13 / originalWidth),
    alignY: 13 / originalHeight,
    zPos: 1200,
    imagePath: "./Images/cross93.svg",
    imageDisabledPath: null,
    customSize: 25,
    pressedFunction: function () {
        activityContainers.UpdatesTabs.postStatusBrick.HideBrick();
    }
};
pictureButtons.ShowPresets.data = {
    name: "ShowPresets",
    id: 6,
    alignX: (20 / originalWidth),
    alignY: 20 / originalHeight,
    zPos: 1200,
    imagePath: "./Images/showpresets.svg",
    imageDisabledPath: null,
    customSize: 30,
    pressedFunction: function () {
        activityContainers.UpdatesTabs.postStatusBrick.MoveBrickUp(originalHeight * 0.8, true);
    }
};
pictureButtons.AddActivity.data = {
    name: "ShowPresets",
    id: 6,
    alignX: 1 - (40 / originalWidth),
    alignY: 20 / originalHeight,
    zPos: 20,
    imagePath: "./Images/add186.svg",
    imageDisabledPath: null,
    //    customSize: 30,
    pressedFunction: function () {

        //         SubmitCodeToVerify();


        if (draggables.SideBar.data.currentPage === 0) {
            goToThisPageOld(draggables.SideBar, 1);
        }
        // This opens the create activity page (will change to set the brick content and open it)
        goToThisPage(draggables.CreateActivity, 0, true);
        if (canSubmitActivity === true) {
            canSubmitActivity = false;
            setTextButtonColor(textButtons.createSubmit, themeColors.lightGrey);
        }
        theBrick.SetBrickContent(appSections.CreateActivity);
        mainRenderController.hide();
        theBrick.MoveBrickUp(originalHeight - 5);
        scrollers.Info.scrollElements[0].textInput.focus();
        Timer.setTimeout(function () {
            scrollers.Info.scrollElements[0].textInput.focus();
        }, 400);

    }
};



//endregion
//====================================================-


//====================================================-
//Text Buttons region
//|20|893|3772|710.59|684.51|1----------------------------------------------------------------------------------------------~
// Text Buttons

// these could go in a positions variable
var textButtonRight = 1 - ((textButtonWidth / originalWidth) * 0.7);
var textButtonLeft = ((textButtonWidth / originalWidth) * 0.7);
var textButtonLeftB = 1 - ((textButtonWidth / originalWidth) * 0.35);

var textButtons = {
    //    Create: {
    //        view: null,
    //        data: null
    //    },
    Cancel: {},
    tempSubmit: {},
    createSubmit: {},
    Hide: {},
    UpdatesHide: {},
    PassItOn: {},
    CloseContacts: {},
    CloseDeclined: {},
    Send: {},
    SmallIn: {},
    SmallOut: {},
    SmallPassItOn: {},
    // SignUp Pages
    Welcome: {},
    VerifyNext: {},
    EnterCodeDone: {},
    WelcomePageEnter: {},
    BackToVerify: {},
    // Settings Pages
    ProfileDone: {},
    BackToSettings: {}
};
doThisForAll(textButtons, setInitialDataForButtons);
textButtons.Cancel.data = {
    name: "Cancel",
    id: 1,
    alignX: textButtonLeft,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Cancel",
    fontSize: fontSizes.small,
    pressedFunction: function () {
        mainRenderController.show(appSections.MainPages);
        theBrick.HideBrick();
    }
};
textButtons.tempSubmit.data = {
    name: "Submit",
    id: 2,
    alignX: textButtonRight,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Submit",
    fontSize: fontSizes.small,
    pressedFunction: function () {
        temporaryActivityObject.dateCreated = Math.floor(Date.now() / 1000);
        temporaryActivityObject.allowAnonymous = true ? 1 : 0;
        $('#activityPictureFileInput').click();
    }
};
textButtons.createSubmit.data = {
    name: "Submit",
    id: 3,
    alignX: textButtonRight,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Submit",
    fontSize: fontSizes.small,
    pressedFunction: function () {
        // This sends the activity to the server straight away instead of creating a sync command
        SendNewActivityToServer();
    }
};
textButtons.Hide.data = {
    name: "Hide",
    id: 4,
    alignX: textButtonLeftB,
    alignY: ((standardScrollElementHeight * 0.63) * 0.5) / originalHeight,
    zPos: 900,
    text: "Hide",
    fontSize: fontSizes.smaller,
    pressedFunction: function () {
        activityContainers.Main.MinimiseActivity();
    }
};
textButtons.PassItOn.data = {
    name: "PassItOn",
    id: 5,
    alignX: 0.75,
    alignY: 0.48,
    zPos: 900,
    text: "Pass It On",
    fontSize: fontSizes.smaller,
    pressedFunction: function () {
        activityContainers.Main.MinimiseActivity();
    }
};
textButtons.UpdatesHide.data = {
    name: "UpdatesHide",
    id: 6,
    alignX: textButtonLeftB,
    alignY: 0.48,
    zPos: 900,
    text: "Hide",
    fontSize: fontSizes.smaller,
    pressedFunction: function () {
        activityContainers.Main.MinimiseActivity();
    }
};
textButtons.CloseContacts.data = {
    name: "CloseContacts",
    id: 4,
    alignX: textButtonRight,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Close",
    fontSize: fontSizes.small,
    pressedFunction: function () {
        mainRenderController.show(appSections.MainPages);
        // completely hide the add new group button Mini Brick
        theViews.BrickContents.Contacts.ButtonBrick.CompletelyHideBrick();
        // completely hide the textInput Mini Brick
        theViews.BrickContents.Contacts.TextBrick.CompletelyHideBrick();
        theBrick.HideBrick();
    }
};
textButtons.CloseDeclined.data = {
    name: "CloseDeclined",
    id: 5,
    alignX: textButtonRight,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Close",
    fontSize: fontSizes.small,
    pressedFunction: function () {
        mainRenderController.show(appSections.MainPages);
        theBrick.HideBrick();
    }
};
textButtons.Send.data = {
    name: "Send",
    id: 5,
    alignX: textButtonRight,
    alignY: 0 /*40 / originalHeight*/ ,
    yPos: (sendStatusBrickHeight * 0.5) + (textButtonHeight * 0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "Send",
    fontSize: fontSizes.small,
    pressedFunction: function () {
        activityContainers.UpdatesTabs.postStatusBrick.HideBrick();
    }
};
textButtons.SmallIn.data = {
    name: "SmallIn",
    id: 5,
    alignX: 0.5 - ((textButtonWidth / (originalWidth - 8 * defaultPaddingHorizontal)) * 0.6),
    alignY: 1 /*40 / originalHeight*/ ,
    yPos: 0 - (standardScrollElementHeight * 0.5), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "IN",
    fontSize: fontSizes.small,
    defaultColor: themeColors.light,
    pressedFunction: function () {
        alert("Pressed the In Button");
    }
};
textButtons.SmallOut.data = {
    name: "SmallOut",
    id: 5,
    alignX: 0.5 + ((textButtonWidth / (originalWidth - 8 * defaultPaddingHorizontal)) * 0.6),
    alignY: 1 /*40 / originalHeight*/ ,
    yPos: 0 - (standardScrollElementHeight * 0.5), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "OUT",
    fontSize: fontSizes.small,
    defaultColor: themeColors.light,
    pressedFunction: function () {
        alert("Pressed the Out Button");
    }
};
textButtons.SmallPassItOn.data = {
    name: "SmallPassItOn",
    id: 6,
    alignX: 0.5,
    alignY: 1 /*40 / originalHeight*/ ,
    yPos: 0 - (standardScrollElementHeight * 0.5), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "Pass it ON",
    fontSize: fontSizes.small,
    defaultColor: themeColors.light,
    pressedFunction: function () {
        alert("Pressed the Pass It ON Button");
    }
};
textButtons.Welcome.data = {
    name: "Welcome",
    id: 7,
    alignX: 0.5,
    alignY: 1 /*40 / originalHeight*/ ,
    yPos: 0 - (standardScrollElementHeight * 0.9), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "Welcome",
    fontSize: fontSizes.large,
    defaultColor: themeColors.dark,
    pressedFunction: function () {
        mainRenderController.show(theViews.SignUpPages.VerifyPhone.view);
    }
};
textButtons.VerifyNext.data = {
    name: "VerifyNext",
    id: 8,
    alignX: textButtonRight,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Next",
    fontSize: fontSizes.medium,
    defaultColor: themeColors.lightGrey,
    pressedFunction: function () {
        //       if (isAValidPhoneNumberBoolean) {
        theUser.p1 = scrollers.VerifyPhone.scrollElements[1].numberInput.getValue().toString();
        console.log(theUser.p1);
        SubmitNumberToVerify();
        //         if (SubmitNumberToVerify()) {
        mainRenderController.show(theViews.SignUpPages.EnterCode.view);
        //         }
        //       }
    }
};
textButtons.EnterCodeDone.data = {
    name: "EnterCodeDone",
    id: 9,
    alignX: textButtonRight,
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Done",
    fontSize: fontSizes.medium,
    defaultColor: themeColors.main,
    pressedFunction: function () {
        SubmitCodeToVerify();
        // If the code was correct
        mainRenderController.show(theViews.SignUpPages.WelcomeEnter.view);
    }
};
textButtons.WelcomePageEnter.data = {
    name: "WelcomePageEnter",
    id: 10,
    alignX: 0.5,
    alignY: 0.95 /*40 / originalHeight*/ ,
    yPos: 0 - (standardScrollElementHeight * 0.85), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "Enter",
    fontSize: fontSizes.medium,
    defaultColor: themeColors.dark,
    pressedFunction: function () {
        mainRenderController.show(theViews.SettingsPages.Profile.view);
    }
};
textButtons.BackToVerify.data = {
    name: "BackToVerify",
    id: 11,
    alignX: textButtonLeft,
    alignY: 40 / originalHeight,
    //yPos: 0 - (standardScrollElementHeight * 0.9), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "Back",
    fontSize: fontSizes.medium,
    defaultColor: themeColors.lightGrey,
    pressedFunction: function () {
        mainRenderController.show(theViews.SignUpPages.VerifyPhone.view);
    }
};
textButtons.ProfileDone.data = {
    name: "ProfileDone",
    id: 12,
    alignX: 1 - ((textButtonWidth / originalWidth) * 0.7),
    alignY: 40 / originalHeight,
    zPos: 900,
    text: "Done",
    fontSize: fontSizes.medium,
    defaultColor: themeColors.main,
    pressedFunction: function () {
        mainRenderController.show(appSections.MainPages);
        SendNewUserToServer();
    }
};
textButtons.BackToSettings.data = {
    name: "BackToSettings",
    id: 13,
    alignX: 1 - ((textButtonWidth / originalWidth) * 0.54),
    alignY: 40 / originalHeight,
    //yPos: 0 - (standardScrollElementHeight * 0.9), //(sendStatusBrickHeight * 0.5) + (textButtonHeight *0.5) + (standardScrollElementHeight * 0.1),
    zPos: 900,
    text: "Back",
    fontSize: fontSizes.small,
    defaultColor: themeColors.grey,
    pressedFunction: function () {
        settingsRenderController.show(theViews.SettingsPages.Settings.view);
    }
};






//endregion
//====================================================-


//====================================================-
//Brick Initialisation region
//|21|2583|3239|500|500|1----------------------------------------------------------------------------------------------~
var theBrick = {};
var theActivityUpdatesBrick = {};
var datePickerBrick = {}; // This will be attatched to the create activity pages/ view
var screenDisabler = {};






//endregion
//====================================================-


//====================================================-
//Stored Phone Contacts Info region
//|25|4322|2340|500|500|1----------------------------------------------------------------------------------------------~
//This is what is returned from the phones contacts (but it's saved here instead of gotton from the phone for testing

//var newPhoneContactsJson = '[ { "name": "Person One Updated", "mobileAssignedContactId": "100", "email": "personone@safemail.net.au", "phonenumber1": "+61 418 818 082", "phonenumber2": "" }, { "name": "Person Two", "mobileAssignedContactId": "200", "email": "", "phonenumber1": "0431914572", "phonenumber2": "" }, { "name": "Person Three", "mobileAssignedContactId": "300", "email": "", "phonenumber1": "0435568662", "phonenumber2": "" }, { "name": "Person Four", "mobileAssignedContactId": "400", "email": "", "phonenumber1": "0405645082", "phonenumber2": "" }, { "name": "Person Five", "mobileAssignedContactId": "500", "email": "", "phonenumber1": "+61 433 017 302", "phonenumber2": "" }]';

var newPhoneContactsJson = '[{"contactname":"Chava","mobileAssignedContactId":"859","phonenumber1":"(02) 0875 56","phonenumber2":null,"email":"mollis@nonummyipsumnon.com"},{"contactname":"Rose","mobileAssignedContactId":"984","phonenumber1":"(02) 4690 28","phonenumber2":null,"email":"Mauris@nuncrisusvarius.ca"},{"contactname":"Armando","mobileAssignedContactId":"46","phonenumber1":"(09) 6980 08","phonenumber2":null,"email":"tempor.lorem@gravidasitamet.co.uk"}]';


//endregion
//====================================================-


//====================================================-
//Create Custom Database Information (for testing) region
//|26|4949|2271|500|500|1----------------------------------------------------------------------------------------------~
// Create DB and fill it with records
var bogusDataText = '{ "phoneContacts": [{ "phoneContactId": 1, "contactname": "Chava", "mobileAssignedContactId": 859, "mobilizrId": 1, "phonenumber1": "(02) 0875 56", "phonenumber2": null, "email": "mollis@nonummyipsumnon.com", "isSelected": false }, { "phoneContactId": 2, "contactname": "Rose", "mobileAssignedContactId": 984, "mobilizrId": null, "phonenumber1": "(02) 4690 28", "phonenumber2": null, "email": "Mauris@nuncrisusvarius.ca", "isSelected": false }, { "phoneContactId": 3, "contactname": "Armando", "mobileAssignedContactId": 46, "mobilizrId": 3, "phonenumber1": "(09) 6980 08", "phonenumber2": null, "email": "tempor.lorem@gravidasitamet.co.uk", "isSelected": false }, { "phoneContactId": 4, "contactname": "Uchenna", "mobileAssignedContactId": 9834, "mobilizrId": null, "phonenumber1": "(02) 4690 28", "phonenumber2": null, "email": "Mauris@nuncrisusvarius.ca", "isSelected": false }, { "phoneContactId": 5, "contactname": "Beccaz", "mobileAssignedContactId": 466, "mobilizrId": 2, "phonenumber1": "(09) 6980 08", "phonenumber2": null, "email": "tempor.lorem@gravidasitamet.co.uk", "isSelected": false }], "mobilizrUsers": [{ "id": 1, "Joined": 1421071637, "laston": 1421071637, "p2": null, "p1": 433024179, "email": null, "picture": "./Images/tempHaleyPic.png", "name": "Chava Chava", "lastSubscriptionDate": 1421071637, "subscriptionType": 0, "activitiesCreated": 0, "activitiesJoined": 0 }, { "id": 2, "Joined": 1421071637, "laston": 1421071637, "p2": null, "p1": 433024179, "email": null, "picture": "./Images/tempBeccaPic.png", "name": "Becca", "lastSubscriptionDate": 1421071637, "subscriptionType": 0, "activitiesCreated": 0, "activitiesJoined": 0 }, { "id": 3, "Joined": 1421071637, "laston": 1421071637, "p2": null, "p1": 433024179, "email": null, "picture": "./Images/displayPicTest.png", "name": "Armando Armando", "lastSubscriptionDate": 1421071637, "subscriptionType": 0, "activitiesCreated": 0, "activitiesJoined": 2 }, { "id": 4, "Joined": 1421071637, "laston": 1421071637, "p2": null, "p1": 433024179, "email": null, "picture": "./Images/displayPicTest.png", "name": "not hugo 5", "lastSubscriptionDate": 1421071637, "subscriptionType": 0, "activitiesCreated": 0, "activitiesJoined": 0 }], "activities": [{ "id": 1, "name": "Activity One", "category": "Category1", "description": "This is a description", "videoLink": null, "picture": "./id_1112.png", "createdBy": 7, "dateCreated": 1421071637, "location": "A Location", "timeStarting": 1421071637, "timeEnding": 1421071637, "target": null, "peopleJoined": 56, "peopleInvited": 110, "privacy": 0, "isAttending": true, "admins": [], "userIsAdmin": false, "allowAnonymous": false, "deletionDays": false, "lastEdited": 1421071637, "nameEdited": 1421071637, "categoryEdited": 1421071637, "descriptionEdited": 1421071637, "videoLinkEdited": 1421071637, "pictureEdited": 1421071637, "locationEdited": 1421071637, "timeStartingEdited": 1421071637, "timeEndingEdited": 1421071637, "targetEdited": 1421071637 }, { "id": 2, "name": "Activity Two", "category": "Category2", "description": "This is a description", "videoLink": null, "picture": "./id_1113.png", "createdBy": 1, "dateCreated": 1421071637, "location": "A Location", "timeStarting": 1421071637, "timeEnding": 1421071637, "target": null, "peopleJoined": 56, "peopleInvited": 110, "privacy": 0, "isAttending": true, "admins": [], "userIsAdmin": false, "allowAnonymous": false, "deletionDays": false, "lastEdited": 1421071637, "nameEdited": 1421071637, "categoryEdited": 1421071637, "descriptionEdited": 1421071637, "videoLinkEdited": 1421071637, "pictureEdited": 1421071637, "locationEdited": 1421071637, "timeStartingEdited": 1421071637, "timeEndingEdited": 1421071637, "targetEdited": 1421071637 }, { "id": 3, "name": "Activity Three", "category": "Category1", "description": "This is a description", "videoLink": null, "picture": "./id_1114.png", "createdBy": 3, "dateCreated": 1421071637, "location": "A Location", "timeStarting": 1421071637, "timeEnding": 1421071637, "target": 100, "peopleJoined": 33, "peopleInvited": 110, "privacy": 0, "isAttending": true, "admins": [], "userIsAdmin": false, "allowAnonymous": false, "deletionDays": false, "lastEdited": 1421071637, "nameEdited": 1421071637, "categoryEdited": 1421071637, "descriptionEdited": 1421071637, "videoLinkEdited": 1421071637, "pictureEdited": 1421071637, "locationEdited": 1421071637, "timeStartingEdited": 1421071637, "timeEndingEdited": 1421071637, "targetEdited": 1421071637 }], "groups": [{ "groupId": 1, "groupName": "First Group", "isSelected": false }, { "groupId": 2, "groupName": "Second Group", "isSelected": false }, { "groupId": 3, "groupName": "Third Group", "isSelected": false }], "channels": [{ "channelId": 1, "connectedToActivity": 1, "channelName": "Friends" }, { "channelId": 2, "connectedToActivity": 1, "channelName": "Everyone" }, { "channelId": 3, "connectedToActivity": 2, "channelName": "Friends" }], "statuses": [{ "statusId": 1, "activityId": 1, "statusType": 0, "statusText": "Im at the State Library entrance come and find me", "timeUpdated": "1422330140", "amountOfTimesUpdated": 1, "fromUserId": 1, "toUserId": null, "channelPostedTo": 1 }, { "statusId": 2, "activityId": 2, "statusType": 1, "statusText": "New Speaker:Anthony Bandcamp will speak on environ issues", "timeUpdated": "1421071638", "amountOfTimesUpdated": 1, "fromUserId": 3, "toUserId": null, "channelPostedTo": 1 }, { "statusId": 3, "activityId": 1, "statusType": 2, "statusText": "", "timeUpdated": "1421071639", "amountOfTimesUpdated": 1, "fromUserId": 2, "toUserId": null, "channelPostedTo": 1 }, { "statusId": 4, "activityId": 1, "statusType": 3, "statusText": "updated their description details", "timeUpdated": "1421071639", "amountOfTimesUpdated": 1, "fromUserId": 3, "toUserId": null, "channelPostedTo": "ChannelId1" }, { "statusId": 5, "activityId": 1, "statusType": 0, "statusText": "", "timeUpdated": 1421071700, "amountOfTimesUpdated": 0, "fromUserId": 1, "toUserId": null, "channelPostedTo": "ChannelId1" }], "invitations": [{ "invitationId": 70, "connectedActivity": 2, "sentFromUser": 2, "sentToUser": 7, "activityStartDate": 1425828454 }, { "invitationId": 72, "connectedActivity": 3, "sentFromUser": 1, "sentToUser": 7, "activityStartDate": 1425828569 }, { "invitationId": 73, "connectedActivity": 1, "sentFromUser": 3, "sentToUser": 7, "activityStartDate": 1425828654 }], "groupConnections": [{ "groupId": 1, "includesContact": 1 }, { "groupId": 1, "includesContact": 2 }, { "groupId": 2, "includesContact": 3 }, { "groupId": 3, "includesContact": 1 }], "channelConnections": [{ "channelId": 1, "includesUser": 2 }, { "channelId": 1, "includesUser": 3 }, { "channelId": 1, "includesUser": 4 }], "activityConnections": [{ "activityId": 1, "includesUser": 1 }, { "activityId": 1, "includesUser": 2 }, { "activityId": 3, "includesUser": 3 }]}';






//endregion
//====================================================-


//====================================================-
//Save/Store the Data to the App (+SyncCommand Queue) region
//|27|5572|2317|500|500|1----------------------------------------------------------------------------------------------~
// This is what one syncCommand looks like
var syncCommandQueue = [
    //    {type : "Send Activity To Server", parameters : {activityIdToSend : "tempActivityId1"}}
];

var newPhoneContacts = JSON.parse(newPhoneContactsJson);

var inAppBogusData = JSON.parse(bogusDataText);
var inAppDatabases = {
    phoneContacts: null,
    mobilizrUsers: null,
    activities: null,
    groups: null,
    channels: null,
    statuses: null,
    invitations: null,
    groupConnections: null,
    channelConnections: null
};

localStorage.clear();

doThisForAll(inAppBogusData, CreateTaffyDbForThese);





//endregion
//====================================================-


/////////////////////////////////////////////////////////////////////////////////////////////////////////
// functions for the app
/////////////////////////////////////////////////////////////////////////////////////////////////////////


//====================================================-
// Dots Tabs Bars and Contacts Functions region
//|2|725|-568|500|500|1----------------------------------------------------------------------------------------------~
// region Dots Logic
var dotLogic = {};
dotLogic.SetEnabled = function (theDot) {
    theDot.isDisabled = false;
    theDot.isActive = false;
    theDot.renderController.show(theDot.inactiveSurface.view);
};
dotLogic.SetDisabled = function (theDot) {
    theDot.isDisabled = true;
    theDot.isActive = false;
    theDot.renderController.show(theDot.disabledSurface.view);
};
dotLogic.SetActive = function (theDot) {
    theDot.isDisabled = false;
    theDot.isActive = true;
    theDot.renderController.show(theDot.activeSurface.view);
};
dotLogic.CheckAndUpdateDots = function (theDraggable) {
    // get the dotGroup from the draggable
    var theDotGroup = theDraggable.data.associatedDotGroup;

    // loop through the amount of dots?
    // loop through the page views (with an object looping thing
    // use the id of the draggable page to set the correct dot from the dots array like this dots[currentlyLoopedPage.id]
    for (var key in theDraggable.views) {
        if (theDraggable.views.hasOwnProperty(key)) {
            var tempPage = theDraggable.views[key];
            if (tempPage.id == theDraggable.data.currentPage) {
                dotLogic.SetActive(theDotGroup.dots[tempPage.id]);
            } else {
                if (tempPage.isDisabled === false) {
                    dotLogic.SetEnabled(theDotGroup.dots[tempPage.id]);
                } else if (tempPage.isDisabled === true) {
                    dotLogic.SetDisabled(theDotGroup.dots[tempPage.id]);
                }
            }

        }
    }
};
// endregion
// region Tabs Bar Logic
var tabsBarLogic = {};
tabsBarLogic.SetEnabled = function (theTab) {
    theTab.isDisabled = false;
    theTab.isActive = false;

    theTab.setProperties({
        color: themeColors.grey
    });
};
tabsBarLogic.SetDisabled = function (theTab) {
    theTab.isDisabled = true;
    theTab.isActive = false;

    theTab.setProperties({
        color: themeColors.lightGlass
    });
};
tabsBarLogic.SetActive = function (theTab) {
    theTab.isDisabled = false;
    theTab.isActive = true;

    theTab.setProperties({
        color: themeColors.main
    });
};
tabsBarLogic.ShowSearchBar = function (theTabsBar) {



    var tempRenderController = theTabsBar.container.mainContainer.renderController;
    tempRenderController.inTransformFrom(function (progress) {
        return Transform.translate(window.innerWidth * (1.0 - progress), 0, 0);
    });

    tempRenderController.outTransformFrom(function (progress) {
        return Transform.translate(window.innerWidth * progress - window.innerWidth, 0, 0);
    });
    tempRenderController.show(theTabsBar.container.mainContainer.searchBarContainer.view, null, function () {
        theTabsBar.container.mainContainer.searchBarContainer.inputSurface.focus();
    });
    theTabsBar.data.searchBarIsShowing = true;

};
tabsBarLogic.HideSearchBar = function (theTabsBar) {


    var tempRenderController = theTabsBar.container.mainContainer.renderController;
    tempRenderController.inTransformFrom(function (progress) {
        return Transform.translate(window.innerWidth * progress - window.innerWidth, 0, 0);
    });

    tempRenderController.outTransformFrom(function (progress) {
        return Transform.translate(window.innerWidth * (1.0 - progress), 0, 0);
    });

    tempRenderController.show(theTabsBar.container.mainContainer.tabsContainer.view);
    theTabsBar.data.searchBarIsShowing = false;
};
tabsBarLogic.CheckAndUpdateTabs = function (theDraggable) {
    // get the dotGroup from the draggable
    var theTabs = theDraggable.data.associatedTabs;

    // loop through the amount of dots?

    // loop through the page views (with an object looping thing
    // use the id of the draggable page to set the correct dot from the dots array like this dots[currentlyLoopedPage.id]

    for (var key in theDraggable.views) {
        if (theDraggable.views.hasOwnProperty(key)) {
            var tempPage = theDraggable.views[key];


            if (tempPage.id == theDraggable.data.currentPage) {
                tabsBarLogic.SetActive(theTabs[tempPage.id]);
            } else {
                if (tempPage.isDisabled === false) {
                    tabsBarLogic.SetEnabled(theTabs[tempPage.id]);
                } else {
                    tabsBarLogic.SetDisabled(theTabs[tempPage.id]);
                }
            }

        }
    }

    tabsBarLogic.UpdateTabIndicatorPosition = function (theTabsBar, theDraggable) {
        // get the indic
    };
};
// endregion
// region Contacts Logic
var contactsLogic = {};
contactsLogic.textIsFocused = false;
contactsLogic.GotoPassItOnMode = function () {
    theViews.BrickContents.Contacts.title.setContent("Now pass it ON!");
    theViews.BrickContents.Contacts.ButtonBrick.textSurface.setContent("Send 0 Invites");
};
// (if true, it means the person is just editing an existing group, if it's false it means the person has put some letters into the bottom
contactsLogic.isEditingGroup = false;
contactsLogic.currentlyEditingGroupId = null;
contactsLogic.isLookingAtContacts = false;
// ( its true if the person has just clicked on the contacts button in the sidebar and hasn't got to it from sending invitations)
contactsLogic.isSendingInvitations = false;
// (this is checked when the contacts page is opened (the brick is moved up with the contacts pages, if it's true, it sets the text of the bottom button to "Send 0 Invites" and every time a new contact or group is selected, that button text will be updated to show how many people are selected (for example "Send 3 Invites)
contactsLogic.TurnToSendInvitationMode = function () {
    contactsLogic.isSendingInvitations = true;
    // update the text in the bottom button bar


};
// (this updates the bottom button to have the "send this many invites" text, it also sets the contactsLogic.isSendingInvitations to true. it also sets the placeholder text in the Add To brick part to "(the category name) Favourites", if the user taps the textInput part , it fills in the text input with that placeholder name, but if they start typing, it first erases the suggested group name (each of those things are done in the on('focus') and the keypress events
contactsLogic.TurnToEditAGroupMode = function (groupId) {
    var tempGroupObject = inAppDatabases.groups({
        groupId: groupId
    }).first();
    contactsLogic.currentlyEditingGroupId = groupId;
    // set the placeholder for the text input box to the name of the group
    theViews.BrickContents.Contacts.TextBrick.textInput.setPlaceholder(tempGroupObject.name);
    // the the title of the contacts page to the name of the being edited group
    theViews.BrickContents.Contacts.title.setContent(tempGroupObject.name);
    theViews.BrickContents.Contacts.ButtonBrick.textSurface.setContent("Create New Group");
    // show the two contacts pages mini bricks if they are both hidden
    console.log("Both of the bricks should be moving up at the moment");
    theViews.BrickContents.Contacts.TextBrick.MoveBrickUp();
    theViews.BrickContents.Contacts.ButtonBrick.MoveBrickUp();
};
contactsLogic.TurnToCreateAGroupMode = function () {


    // set the placeholder for the text input box to the name of the group
    theViews.BrickContents.Contacts.TextBrick.textInput.setPlaceholder("new group");
    // the the title of the contacts page to the name of the being edited group
    theViews.BrickContents.Contacts.title.setContent(tempGroupObject.name);
    theViews.BrickContents.Contacts.ButtonBrick.textSurface.setContent("Create New Group");
    // show the two contacts pages mini bricks if they are both hidden
    theViews.BrickContents.Contacts.TextBrick.MoveBrickUp();
    theViews.BrickContents.Contacts.ButtonBrick.MoveBrickUp();
};
// ( this goes to th econtacts tab page and changes the title and updates the two bricks and shows them if they are hidden)
contactsLogic.aContactIsSelected = false;
// when a contact or group checkbox is selected (or clicked) it checks a few things, if the contacts pages are in "isSendingInvitations" then it updates the number at the bottom to match how many people are selected. And if the tap was to de-select a contact, it uses the in app database to see if 0 contacts are selected, if 0 are now selected it sets the contactsLogic.aContactIsSelected to false and hides the "Add to ths group" brick
//  if contactsLogic.aContactIsSelected set to false, if a contact is selected, it will set it to true and also show the "Add to this group" brick.
// endregion






//endregion
//====================================================-


//====================================================-
//For Initialising Variables in storage.js region
//|3|1400|-478|528.47|503.78|1----------------------------------------------------------------------------------------------~
function setInitialDataForAllScrollers(objectToLoop, loopedChildKey) {
    // this could possibly be better if the data that is being set is sent in through a parameter, so it would be visible what's being set from storage.js (where the function is being run from)

    objectToLoop[loopedChildKey] = {
        view: null,
        scrollElements: [],
        scrollView: null,
        container: null,
        modifier: null,
        data: null,
        containerModifier: null,
        node: null
    };
}

function setInitialDataForAllDraggables(objectToLoop, loopedChildKey) {
    objectToLoop[loopedChildKey] = {
        draggable: null,
        data: null,
        modifier: null,
        views: null,
        container: null,
        node: null
    };
}

function setInitialDataForTabsBars(objectToLoop, loopedChildKey) {

    objectToLoop[loopedChildKey] = {
        data: {},
        modifier: null,
        container: {
            mainContainer: {
                tabsContainer: {
                    renderController: null,
                    tabs: [],
                    tabIndicator: null,
                    modifier: null
                },
                searchBarContainer: {
                    renderController: null,
                    inputSurface: null,
                    modifier: null
                },
            },
            searchButtonContainer: {
                renderController: null,
                searchIcon: null,
                cancelIcon: null,
                modifier: null
            }
        }
    };
}

function setInitialDataForDotGroups(objectToLoop, loopedChildKey) {

    objectToLoop[loopedChildKey] = {
        renderController: null,
        data: null,
        dots: [],
        container: null,
        modifier: null
    };
}

function setInitialDataForButtons(objectToLoop, loopedChildKey) {

    objectToLoop[loopedChildKey] = {
        view: null,
        data: null
    };
}






//endregion
//====================================================-


//====================================================-
// Adding gui elements in main.js functions region
//|4|-75|71|500|500|1----------------------------------------------------------------------------------------------~
function getViewNameFromObject(theObject) {
    for (var name in theViews) {
        if (theViews[name] == theObject)
            return (name);
    }
    return ("");
}

function getScrollerNameFromObject(theObject) {
    for (var name in scrollers) {
        if (scrollers[name] == theObject)
            return (name);
    }
    return ("");
}

function addTitleTexts(viewGroupObject, loopedKey) {
    var tempViewName = getViewNameFromObject(viewGroupObject);
    //     console.log(tempViewName);
    viewGroupObject[loopedKey].view.add(titleTexts[tempViewName][loopedKey].view);
}






//endregion
//====================================================-


//====================================================-
//General Functions region
//|5|647|77|562.34|652.41|1----------------------------------------------------------------------------------------------~
function toggleSideBar() {
    scrollviewHasBeenTurnedOff = false;
    if (draggables.SideBar.data.currentPage === 0) {
        draggables.SideBar.data.currentPage = 1;
        goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
    } else if (draggables.SideBar.data.currentPage == 1) {
        draggables.SideBar.data.currentPage = 0;
        goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);

    }
}


function setTextButtonColor(theTextButton, theNewColor) {
    theTextButton.view.surface.setProperties({
        color: theNewColor
    });
}

function goToThisPageOld(theDraggable, pageNumber, runEndFunction) {
    theDraggable.data.currentPage = pageNumber;
    theDraggable.draggable.setPosition([-theDraggable.data.pageWidth * pageNumber, 0], snapTransition);
    if (theDraggable == draggables.SideBar) {
        if (pageNumber === 0) {
            screenDisabler.renderController.show(screenDisabler.view);
        } else if (pageNumber == 1) {
            screenDisabler.renderController.hide();
        }
    }
    if (runEndFunction) {
        theDraggable.data.onEndFunction();
    }
}

function goToThisPage(theDraggable, pageNumber, runEndFunction) {
    theDraggable.data.currentPage = pageNumber;

    theDraggable.container.goToPage(pageNumber);

    if (runEndFunction) {
        theDraggable.data.onEndFunction();
    }
}

function setNewLockedLimit(theDraggable, pageLimitNumber) {
    theDraggable.data.lockedPageLimit = pageLimitNumber;
    if (theDraggable.data.currentPage > pageLimitNumber) {
        goToThisPage(theDraggable, pageLimitNumber - 1);
    }

    // Loops through all the pages and if the id is under the locked page amount (minus one) is sets the page as disabled
    for (var key in theDraggable.views) {
        if (theDraggable.views.hasOwnProperty(key)) {
            var tempPage = theDraggable.views[key];
            if (tempPage.id < (theDraggable.data.lockedPageLimit)) {
                tempPage.isDisabled = false;
            } else {
                tempPage.isDisabled = true;
            }

        }
    }
    // Then this updates all the dots for the page if it has any?
    if (theDraggable.associatedDotGroup !== null) {
        dotLogic.CheckAndUpdateDots(theDraggable);
    }
}

function activitySmallBoxMovementLogic(theDraggables) {

    function DraggableStartFunction(theDraggable) {
        return function () {


            touchDownPosition = theDraggable.draggable.getPosition()[0];
            touchDownTime = performance.now();
            startedScrollingHorizontally = true;
        };
    }

    function DraggableUpdateFunction(theDraggable, theScrollers) {
        return function () {
            //        if (startedScrollingHorizontally == true) {
            var temporaryTouchPosition = theDraggable.draggable.getPosition()[0];
            var tempDistance = temporaryTouchPosition - touchDownPosition;
            // this could mabye be simplified a bit later
            // if the horizontal scrolling hasnt allready been disabled
            // if there's a scrollview
            if (horizontalPageScrollHasBeenTurnedOff === false) {
                if (scrollviewHasBeenTurnedOff === false) {
                    if (Math.abs(tempDistance) > 50) {
                        // at the moment it's a little bit inefficient because it enables and dissables All the scrollers associated with the horizontal draggable, it could instead , only disable the scroller that's specific to the page it's on (cause it only ever has to diable one scroller at a time)
                        for (var i = 0; i < theScrollers.length; i++) {
                            theScrollers[i].scrollView.setOptions({
                                speedLimit: 0
                            });





                            scrollviewHasBeenTurnedOff = true;
                        }
                    }
                }
            }
        };
    }

    function DraggableEndFunction(theDraggable, theScrollers) {
        return function () {
            var endFunctionToRun = theDraggable.data.onEndFunction;

            startedScrollingHorizontally = false;
            touchReleaseTime = performance.now();
            touchReleasePosition = theDraggable.draggable.getPosition()[0];
            var tempDistance = touchReleasePosition - touchDownPosition;
            var tempDeltaTime = touchReleaseTime - touchDownTime;

            var tempVelocity = tempDistance / tempDeltaTime;

            var velocityMagnitude = Math.abs(tempVelocity);
            var swipeToDirection;
            var standardHalfwayPoint = ((-tempPageWidth * theDraggable.data.currentPage) - (tempPageWidth / 2));
            var sideBarHalfwayPoint = (-(sideBarWidth / 2));
            var activitySmallBoxHalfwayPoint = (-(activityBoxWidth / 2));

            function SwipeThePageBasedOnDirection(swipeDir) {
                if (swipeDir === 0) {
                    if (theDraggable.data.currentPage > 0) {
                        theDraggable.data.currentPage -= 1;
                    }
                } else if (swipeDir == 1) {
                    if (theDraggable.data.currentPage < theDraggable.data.pageAmount && theDraggable.data.currentPage < (theDraggable.data.lockedPageLimit - 1)) {
                        theDraggable.data.currentPage += 1;
                    }
                }
                goToThisPageOld(theDraggable, theDraggable.data.currentPage);
            }
            // if the swipe it meant to bring the page that's to the right, the swipeToDirection will be 1 (and 0 if its to the left)
            if (tempVelocity > 0) {
                swipeToDirection = 0;
            } else {
                swipeToDirection = 1;
            }
            // if the magnitude of the velocity is under 0.4 then do this stuff
            if (velocityMagnitude < 0.6) {
                if (snapTransition.velocity !== 0) {
                    snapTransition.velocity = 0;
                }
                if (theDraggable == draggables.SideBar) {
                    if (draggables.SideBar.draggable.getPosition()[0] > sideBarHalfwayPoint) {
                        theDraggable.data.currentPage = 0;
                        goToThisPageOld(theDraggable, theDraggable.data.currentPage);
                    } else {
                        theDraggable.data.currentPage = 1;
                        goToThisPageOld(theDraggable, theDraggable.data.currentPage);
                    }
                } else if (theDraggable == draggables.ActivitySmallBox) {
                    if (draggables.ActivitySmallBox.draggable.getPosition()[0] > activitySmallBoxHalfwayPoint) {
                        theDraggable.data.currentPage = 0;
                        goToThisPageOld(theDraggable, theDraggable.data.currentPage);
                    } else {
                        theDraggable.data.currentPage = 1;
                        goToThisPageOld(theDraggable, theDraggable.data.currentPage);
                    }
                } else {
                    // to the left page
                    if (swipeToDirection === 0) {
                        if (theDraggable.draggable.getPosition()[0] > standardHalfwayPoint) {
                            SwipeThePageBasedOnDirection(swipeToDirection);
                        } else {
                            goToThisPageOld(theDraggable, theDraggable.data.currentPage);

                        }
                    }
                    // to the right page
                    else if (swipeToDirection == 1) {
                        if (theDraggable.draggable.getPosition()[0] < standardHalfwayPoint) {
                            SwipeThePageBasedOnDirection(swipeToDirection);
                        } else {
                            goToThisPageOld(theDraggable, theDraggable.data.currentPage);

                        }
                    }
                }
            } else {
                snapTransition.velocity = velocityMagnitude;
                SwipeThePageBasedOnDirection(swipeToDirection);



            }
            if (endFunctionToRun) {
                endFunctionToRun();
            }
            // if the scrollviews are stored in an object (instead of individual variables) the scrollview can be passed in as a parameter, also the scrollViewHasBeenTurnedOff could either be a property of the scrollview or a new object that stores which scrollviews are temporarily turned off, (if it turns out it needs to check it for each scrollview individually, it could work as just one variable not sure)
            // also this code could be encased in an if (scrollViewParameter) type thing, so it only runs if a scrollView has been passed in as a parameter
            for (var i = 0; i < theScrollers.length; i++) {
                theScrollers[i].scrollView.setOptions({
                    speedLimit: 5
                });




                scrollviewHasBeenTurnedOff = false;

            }
        };
    }

    for (var j = 0; j < theDraggables.length; j++) {
        var theScrollers = theDraggables[j].data.associatedScrollers;
        var tempPageWidth = theDraggables[j].data.pageWidth;

        goToThisPageOld(theDraggables[j], 1);
        theDraggables[j].draggable.on('start', DraggableStartFunction(theDraggables[j]));
        // all of this would only run if a scroll view has been passed in as a parameter too
        theDraggables[j].draggable.on('update', DraggableUpdateFunction(theDraggables[j], theScrollers));
        theDraggables[j].draggable.on('end', DraggableEndFunction(theDraggables[j], theScrollers));
    }
}

function draggablePagesMovementLogic(theDraggables) {


    function DraggableChangePageFunction(theDraggable) {
        return function () {
            var endFunctionToRun = theDraggable.data.onEndFunction;

            theDraggable.data.currentPage = theDraggable.container.getCurrentIndex();
            if (endFunctionToRun) {
                endFunctionToRun();
            }
        };
    }

    function DraggableStartFunction(theDraggable, theScrollers) {
        return function () {
            for (var i = 0; i < theScrollers.length; i++) {
                theScrollers[i].scrollView.setOptions({
                    enabled: false
                });
            }
            theDraggable.container.setOptions({
                touchMoveDirectionThresshold: undefined
            });
        };
    }

    function DraggableEndFunction(theDraggable, theScrollers) {
        return function () {

            for (var i = 0; i < theScrollers.length; i++) {
                theScrollers[i].scrollView.setOptions({
                    enabled: true
                });
            }
            theDraggable.container.setOptions({
                touchMoveDirectionThresshold: 0.3
            });

            if (theDraggable.data.currentPage >= (theDraggable.data.lockedPageLimit)) {
                theDraggable.container.goToPreviousPage();
            }
        };
    }

    for (var j = 0; j < theDraggables.length; j++) {
        var theScrollers = theDraggables[j].data.associatedScrollers;
        var tempPageWidth = theDraggables[j].data.pageWidth;
        goToThisPage(theDraggables[j], 1);
        theDraggables[j].container.on('pagechange', DraggableChangePageFunction(theDraggables[j]));
        theDraggables[j].container.on('scrollstart', DraggableStartFunction(theDraggables[j], theScrollers));
        theDraggables[j].container.on('scrollend', DraggableEndFunction(theDraggables[j], theScrollers));
    }
}

function hideAScrollElement(elementNumber, theScroller, shouldDelete) {
    // this deletes an element from the scrollview 

    // this doesnt work cause its trying to pass the variable as a reference which is cant do?
    var scrollElementToDelete = theScroller.scrollElements[elementNumber];

    scrollElementToDelete.originalHeight = scrollElementToDelete.container.getSize()[1];
    if (scrollElementToDelete.isHidden === true) {
        return;
    }

    scrollElementToDelete.isHidden = true;
    var opacity = new Transitionable(1);
    var sizeY = new Transitionable(scrollElementToDelete.container.getSize()[1]);

    // Performance implication - both of these functions execute on every tick.
    // They could be deleted after transition is complete if they were non-trivial
    // (allowing prototype function to resume)
    scrollElementToDelete.getSize = function () {
        var height = sizeY.get();
        return [undefined, height];
    };

    scrollElementToDelete.mod.opacityFrom(function () {
        return opacity.get();
    });

    opacity.set(0, {
            duration: 500
        },
        function () {
            // This bit animates the height of the deleted surface, causing the rest of the list to move up
            sizeY.set(0, {
                duration: 500,
                curve: Easing.outCirc
            }, function () {
                if (shouldDelete) {
                    //                    scrollElementToDelete.getSize = null;
                    //                    scrollElementToDelete.mod.opacityFrom = null;
                    //                    theScroller.scrollView.setPosition(0);
                    deleteThisElement(elementNumber, theScroller);
                }
                //                scrollElementToDelete.setSize(undefined, 0);
                //                delete scrollElementToDelete.getSize;
                delete scrollElementToDelete.mod.opacityFrom;
            });
        });
}

function tickACheckBox(theScroller, trueOrFalse, variableToSet) {
    // this was originally theCorrectIndex, but now it's an array incase the varaible matches multiple scrollElement elements in the scroller
    // find the scrollElements and set the content of the contianer to one of the two presaved contact html text elements
    var theCorrectIndexes = [];

    for (var i = 0; i < theScroller.scrollElements.length; i++) {
        if (theScroller.scrollElements[i].linkedVariable === variableToSet) {
            theCorrectIndexes.push(i);
        }
    }


    // This can find any of the scroll elements that have the linkedVariable property set to the same variableToSetParameter and then tick it
    if (theCorrectIndexes.length > 0) {
        for (var j = 0; j < theCorrectIndexes.length; j++) {
            if (trueOrFalse === true) {
                theScroller.isTicked = true;
                theScroller.scrollElements[theCorrectIndexes[j]].iconSurface.setContent("./Images/checkBoxTicked.svg");
            } else {
                theScroller.isTicked = false;
                theScroller.scrollElements[theCorrectIndexes[j]].iconSurface.setContent("./Images/checkBoxEmpty.svg");
            }
        }
    } else {
        console.error("There were no matches for the variable value that was looked for");
    }

}

function howManyContactsAreTicked() {
    // this loops through the whole contacts people scroler (only the people one) and checks to see how many are ticked, and returns that number
    var selectedContacts = inAppDatabases.phoneContacts({
        isSelected: true
    }).get();
    return selectedContacts.length;
}

function getSelectedContacts() {
    // This gets the currently selected contacts and returns them as an array
    return inAppDatabases.phoneContacts({
        isSelected: true
    }).get();
}

function tickAGroupCheckBox(trueOrFalse, recievedGroupId, isUntickedFromContact) {

    // if a group checkbox is ticked (and the group's isSelected is set to false, the group's isSelected = set to true and then all of the children contacts are ticked, if any of the children contacts are unticked, the group becomes unticked (however all the groups children that were previously ticked stay ticked
    // to untick all the children, the group checkbox has to be-re ticked and then unticked manually (by unticking the group, then all of its children will be unselected

    var theScroller = scrollers.ContactsGroups;

    //there'll only be one correct index for the selected group 
    var theGroupsIndexPosition;
    for (var i = 0; i < theScroller.scrollElements.length; i++) {
        if (theScroller.scrollElements[i].groupId) {
            if (theScroller.scrollElements[i].groupId === recievedGroupId) {
                theGroupsIndexPosition = i;
                break;
            }
        }
    }

    // the groups contacts are also stored in the groups scroll element but the groups scroll bundle isn't really linked to here , and finding it from the scroller could be just as much efort as getting the results from the inAppDatabase
    var groupsChildrenArray = inAppDatabases.groupConnections({
        groupId: recievedGroupId
    }).select("includesContact");
    // a taffy query in the inAppDatabases groupConnections for an array of the includesContact part of all the matches where the groupId part matches the passed in groupId

    if (trueOrFalse === true) {
        theScroller.isTicked = true;
        theScroller.scrollElements[theGroupsIndexPosition].container.setContent(theScroller.scrollElements[theGroupsIndexPosition].tempContentElementTicked);
        // if the gorup isn't selected then select all the groups children
        // loop through the groups children connections and tick the checkbox for each of those contacts
        for (var j = 0; j < groupsChildrenArray.length; j++) {
            tickAContactCheckBox(true, groupsChildrenArray[j], true);
        }
    } else {
        theScroller.isTicked = false;
        theScroller.scrollElements[theGroupsIndexPosition].container.setContent(theScroller.scrollElements[theGroupsIndexPosition].tempContentElement);
        if (!isUntickedFromContact) {
            // if the group is marked as already selected, then deselect all the children
            // do the same as above but instead of ticking the checkboxes untick them
            for (var k = 0; k < groupsChildrenArray.length; k++) {
                tickAContactCheckBox(false, groupsChildrenArray[k], true);
            }
        }
    }
}

function tickAContactCheckBox(trueOrFalse, recievedContactId, beingTickedAutomatically) {

    // This should only happen if it's being ticked manually
    // taffy db for any group conections the contact is in, if the returned array is larger than 0, run the tickAGroupCheckBox() function for all of those group ids
    if (!beingTickedAutomatically) {
        if (trueOrFalse === false) {
            var contactIsInTheseGroups = inAppDatabases.groupConnections({
                includesContact: recievedContactId
            }).select("groupId");
            for (var k = 0; k < contactIsInTheseGroups.length; k++) {
                var tempGroupObject = inAppDatabases.groups({
                    groupId: contactIsInTheseGroups[k]
                }).first();
                if (tempGroupObject.isSelected === true) {
                    tempGroupObject.isSelected = false;
                    tickAGroupCheckBox(tempGroupObject.isSelected, contactIsInTheseGroups[k], true);
                }
            }
        }
    } else if (beingTickedAutomatically === true) {
        var tempContactObject = inAppDatabases.phoneContacts({
            phoneContactId: recievedContactId
        }).first();
        tempContactObject.isSelected = trueOrFalse;
    }

    // check if the contact is a part of any groups and if it is, check if any of those groups are ticked, and if they are untick them (But untick them in a  different way to manually unticking them, so all the children stay ticked except for the child contact being unticked
    function tempLoopThroughAndTick(theScroller, theRecievedContactId) {

        // this was originally theCorrectIndex, but now it's an array incase the varaible matches multiple scrollElement elements in the scroller
        var theCorrectIndexes = [];
        for (var i = 0; i < theScroller.scrollElements.length; i++) {
            if (theScroller.scrollElements[i].contactId) {
                if (theScroller.scrollElements[i].contactId === theRecievedContactId) {
                    theCorrectIndexes.push(i);
                }
            }
        }
        // This can find any of the scroll elements that have the linkedVariable property set to the same variableToSetParameter and then tick it
        if (theCorrectIndexes.length > 0) {
            for (var j = 0; j < theCorrectIndexes.length; j++) {
                if (trueOrFalse === true) {
                    theScroller.scrollElements[theCorrectIndexes[j]].isTicked = true;
                    theScroller.scrollElements[theCorrectIndexes[j]].container.setContent(theScroller.scrollElements[theCorrectIndexes[j]].tempContentElementTicked);
                } else {
                    theScroller.scrollElements[theCorrectIndexes[j]].isTicked = false;
                    theScroller.scrollElements[theCorrectIndexes[j]].container.setContent(theScroller.scrollElements[theCorrectIndexes[j]].tempContentElement);
                }
            }
        } else {
            console.error("There were no matches for the variable value that was looked for");
        }
    }
    // this makes sure the contact is ticked n both the contacts people and contacts groups views
    tempLoopThroughAndTick(scrollers.ContactsPeople, recievedContactId);
    tempLoopThroughAndTick(scrollers.ContactsGroups, recievedContactId);

    // *the howManyContactsAreTicked has to be checked after its done ticking the new contacts so it can include the newly ticked or unticked contact
    if (trueOrFalse === true) {
        if (contactsLogic.aContactIsSelected === false) {
            contactsLogic.aContactIsSelected = true;

            contactsLogic.TurnToEditAGroupMode();
        }
    } else if (trueOrFalse === false) {
        if (howManyContactsAreTicked() < 1) {
            contactsLogic.aContactIsSelected = false;
            theViews.BrickContents.Contacts.TextBrick.HideBrick();
            theViews.BrickContents.Contacts.ButtonBrick.HideBrick();
        }
    }

}

function tickAGenericCheckBox(theScroller, recievedCheckBoxName, isNotABullet, beingTickedAutomatically) {
    // This should tick the checkbox with the same name string that was passed in, in the scrollView that was passed in., but also first untick all the other checkboxes (there should only ever be one other at most ticked) , since the check boxes work like bullet points (at the moment, I think)
    // There's no truOrFalse variable since the bullet point checkboxes can't be unticked? Actually that's not true mabye? they are unticked automatically, but I think it's all done within this function so it does'nt need to be passed in as a parameter (this function knows to untick all the checkboxes inthe scroller that don't match the name passed in)

    // This should only happen if it's being ticked manually








    // this was originally theCorrectIndex, but now it's an array incase the varaible matches multiple scrollElement elements in the scroller ( for bullet points this should usually only be 1, (mabye if a category is shown in the normal category list And in favourites it applies? but probably not, For now it could be one of each category, and arranged under the different headings/titles?)

    for (var i = 0; i < theScroller.scrollElements.length; i++) {
        if (isNotABullet === false) {
            if (theScroller.scrollElements[i].type && theScroller.scrollElements[i].type === "genericCheckbox") {
                if (theScroller.scrollElements[i].name === recievedCheckBoxName) {
                    theScroller.scrollElements[i].isTicked = true;
                    theScroller.scrollElements[i].container.setContent(theScroller.scrollElements[i].tempContentElementTicked);
                    if (theScroller === scrollers.Category) {
                        temporaryActivityObject.category = theScroller.scrollElements[i].name;

                    } else if (theScroller === scrollers.Privacy) {
                        //NOTE this number is just chnaged so instead of 0,2,4 it gets 0,1,2
                        var tempPrivacySetting = i - (i * 0.5);
                        temporaryActivityObject.privacy = tempPrivacySetting;

                    }

                } else {
                    theScroller.scrollElements[i].isTicked = false;
                    theScroller.scrollElements[i].container.setContent(theScroller.scrollElements[i].tempContentElement);
                }
            } else if (theScroller.scrollElements[i].type && theScroller.scrollElements[i].type === "genericCheckboxDescription") {


                if (theScroller.scrollElements[i].name === recievedCheckBoxName) {
                    showAScrollElement(i, theScroller);
                } else {
                    hideAScrollElement(i, theScroller, false);
                }
            }
        } else if (isNotABullet === true) {
            if (theScroller.scrollElements[i].type && theScroller.scrollElements[i].type === "genericCheckboxMultiple") {
                if (theScroller.scrollElements[i].name === recievedCheckBoxName) {

                    if (theScroller.scrollElements[i].isTicked === true) {
                        theScroller.scrollElements[i].isTicked = false;
                        if (theScroller === scrollers.Privacy) {
                            temporaryActivityObject.allowAnonymous = false;
                        }
                        theScroller.scrollElements[i].container.setContent(theScroller.scrollElements[i].tempContentElement);
                    } else if (theScroller.scrollElements[i].isTicked === false) {
                        theScroller.scrollElements[i].isTicked = true;
                        theScroller.scrollElements[i].container.setContent(theScroller.scrollElements[i].tempContentElementTicked);
                        if (theScroller === scrollers.Privacy) {
                            temporaryActivityObject.allowAnonymous = true;
                        }
                    }
                }
            }
        }
    }
}

function getNameOfTickedGenericCheckBox(theScroller) {
    var tickedCheckBoxName = null;
    for (var i = 0; i < theScroller.scrollElements.length; i++) {
        if (theScroller.scrollElements[i].type && theScroller.scrollElements[i].type === "genericCheckbox") {
            if (theScroller.scrollElements[i].isTicked === true) {
                tickedCheckBoxName = theScroller.scrollElements[i].name;
                break;
            }
        }
    }
    return tickedCheckBoxName;

}

function getNamesOfMultipleTickedGenericCheckBoxes(theScroller) {
    var tickedCheckBoxNames = [];
    for (var i = 0; i < theScroller.scrollElements.length; i++) {
        if (theScroller.scrollElements[i].type && theScroller.scrollElements[i].type === "genericCheckboxMultiple") {
            if (theScroller.scrollElements[i].isTicked === true) {
                tickedCheckBoxNames.push(theScroller.scrollElements[i].name);
            }
        }
    }
    return tickedCheckBoxNames;

}

function showAScrollElement(elementNumber, theScroller) {

    var scrollElementToShow = theScroller.scrollElements[elementNumber];
    var opacity = new Transitionable(0);
    var sizeY = new Transitionable(0);
    var heightTarget = standardScrollElementHeight;

    if (scrollElementToShow.isHidden === false) {
        return;
    }
    scrollElementToShow.isHidden = false;
    if (scrollElementToShow.originalHeight) {
        heightTarget = scrollElementToShow.originalHeight;
    }

    // Performance implication - both of these functions execute on every tick.
    // They could be deleted after transition is complete if they were non-trivial
    // (allowing prototype function to resume)
    scrollElementToShow.getSize = function () {
        var height = sizeY.get();
        return [undefined, height];
    };
    scrollElementToShow.mod.opacityFrom(function () {
        return opacity.get();
    });

    sizeY.set(heightTarget, {
        duration: 500,
        curve: Easing.outCirc
    }, function () {
        // Need to set surface size manually after the transition, otherwise the div height is 0
        // (even though space was made for it)
        scrollElementToShow.container.setSize([undefined, heightTarget]);
        // Fade in the item's content
        opacity.set(1, {
            duration: 500
        }, function () {
            delete scrollElementToShow.getSize;
            delete scrollElementToShow.mod.opacityFrom;
        });
    });

}

function deleteThisElement(number, theScroller) {
    Timer.setTimeout(function () {
        theScroller.scrollElements.splice(number, 1);
        theScroller.scrollView.sequenceFrom(theScroller.scrollElements);
    }, 10);
}

function BlurTheDateInputBoxes() {
    for (var i = 0; i < 2; i++) {
        if (scrollers.When.scrollElements[i].isFocused === true) {
            scrollers.When.scrollElements[i].setBlurred();
        }
    }
}

function PreventDraggableWhileScrolling(theDraggable) {
    // All of this code just prevents the page from moving while a scrollview is being slidded up or down....
    // some of the variables like "horizontalPageScrollHasBeenTurnedOff" may need to be stored in an array instead so there can be a copy of that variable for each of the scrollviews? that has this "preventing horizontal draggable" functionality applied to it (through a function)
    var theScrollers = theDraggable.data.associatedScrollers;
    var onStartClosure = function (n) {
        return function () {
            theDraggable.container.setOptions({
                enabled: false
            });
            theScrollers[n].scrollView.setOptions({
                touchMoveDirectionThresshold: undefined
            });
        };
    };
    var onEndScrollFunction = function (n) {
        return function () {
            theDraggable.container.setOptions({
                enabled: true
            });
            theScrollers[n].scrollView.setOptions({
                touchMoveDirectionThresshold: 0.3
            });
        };
    };
    for (var i = 0; i < theScrollers.length; i++) {
        theScrollers[i].scrollView.on('scrollstart', onStartClosure(i));
        theScrollers[i].scrollView.on('scrollend', onEndScrollFunction(i));
    }
}

function GetCurrentTime() {
    // this gets the current UTC Time in seconds
    return Math.floor(Date.now() / 1000);
}

function GetTimezoneOffset() {
    var tempCurrentDate = new Date();
    return tempCurrentDate.getTimezoneOffset();
}

// This converts a number to the number with "px" on the end
function ToPixelString(inputNumber) {
    var outputString = '' + inputNumber + 'px';
    return outputString;
}

// This gets a number (as a string) from a string that can also have letters
function GetNumberFromString(inputString) {
    var outputString = inputString.replace(/[^0-9.]/g, "");
    return outputString;
}

function GetDefaultFontSize(pa) {
    pa = pa || document.body;
    var who = document.createElement('div');

    who.style.cssText = 'display:inline-block; padding:0; line-height:1; position:absolute; visibility:hidden; font-size:1em';

    who.appendChild(document.createTextNode('M'));
    pa.appendChild(who);
    var fs = [who.offsetWidth, who.offsetHeight];
    pa.removeChild(who);
    return fs;
}

function SetAllChildrenToNull(objectToLoop, loopedKey) {
    objectToLoop[loopedKey] = null;
}

function preventFurtherClicks(event){
    if (!event) var event = window.event; 
    event.cancelBubble = true;
    if (event.stopPropagation) 
        event.stopPropagation();
    if (event.preventDefault) 
        event.preventDefault();
}




//endregion
//====================================================-


//====================================================-
//For Creation Functions region
//|6|1271|59|500|500|1----------------------------------------------------------------------------------------------~

// This is an object loop where the parameters for the function that run for each property requires "object" and "key" (property name) parameters
function doThisForAll(objectToLoop, functionToRun) {
    for (var key in objectToLoop) {
        if (objectToLoop.hasOwnProperty(key)) {
            functionToRun(objectToLoop, key);
        }
    }
}

function getPrivacyStringFromNumber(privacyNumber) {
    var tempPrivacyString = "";
    if (privacyNumber === 0) {
        tempPrivacyString = "Private";
    } else if (privacyNumber == 1) {
        tempPrivacyString = "Sharable";
    } else if (privacyNumber == 2) {
        tempPrivacyString = "Public";
    }
    return tempPrivacyString;
}

function calculateLetterAmountOnLine(widthOfLine, fontSize) {
    var tempNewLetterWidth = fontSize * defaultFontWidth * 0.64;
    var tempLetterAmount = widthOfLine / tempNewLetterWidth;
    return tempLetterAmount;
}

function amountToIncreaseStatusHeightBy(lengthOfString, lettersInALine, yIncreasePerLine) {
    for (var i = 1; i < 20; i++) {
        if (lengthOfString < i * lettersInALine) {
            return i * yIncreasePerLine;
        }
    }
}






//endregion
//====================================================-


//====================================================-
//For Manipulating ScrollElements region
//|51|1827|337|722.3|527|1----------------------------------------------------------------------------------------------~
function hideAnAccordion(theAccordionId, theScroller) {

    var theGroupIndex = null;
    var groupSize = null;
    for (var i = 0; i < theScroller.scrollElements.length; i++) {

        if (theScroller.scrollElements[i].id) {
            if (theScroller.scrollElements[i].id == theAccordionId) {

                theGroupIndex = i;
                theScroller.scrollElements[i].childrenHidden = true;
                groupSize = 1;
            }
        }
        if (theGroupIndex !== null) {
            break;
        }
    }

    if (theGroupIndex !== null) {

        for (var j = theGroupIndex + 1; j < (theGroupIndex + 1 + groupSize); j++) {

            hideAScrollElement(j, theScroller);
        }
    }

}

function showAnAccordion(theAccordionId, theScroller) {
    var theGroupIndex = null;
    var groupSize = null;
    for (var i = 0; i < theScroller.scrollElements.length; i++) {

        if (theScroller.scrollElements[i].id) {


            if (theScroller.scrollElements[i].id == theAccordionId) {

                theGroupIndex = i;
                theScroller.scrollElements[i].childrenHidden = false;
                groupSize = 1;
            }
        }
        if (theGroupIndex !== null) {
            break;
        }
    }

    if (theGroupIndex !== null) {

        for (var j = theGroupIndex + 1; j < (theGroupIndex + 1 + groupSize); j++) {

            showAScrollElement(j, theScroller);
        }
    }

}

function hideAGroup(theGroupId, theScroller) {


    var theGroupIndex = null;
    var groupSize = null;
    for (var i = 0; i < theScroller.scrollElements.length; i++) {

        if (theScroller.scrollElements[i].groupId) {


            if (theScroller.scrollElements[i].groupId == theGroupId) {

                theGroupIndex = i;

                theScroller.scrollElements[i].childrenHidden = true;
                groupSize = theScroller.scrollElements[i].groupsContacts.length;

            }
        }
        if (theGroupIndex !== null) {
            break;
        }
    }

    if (theGroupIndex !== null) {

        for (var j = theGroupIndex + 1; j < (theGroupIndex + 1 + groupSize); j++) {

            hideAScrollElement(j, theScroller);
        }
    }

}

function showAGroup(theGroupId, theScroller) {

    var theGroupIndex = null;
    var groupSize = null;
    for (var i = 0; i < theScroller.scrollElements.length; i++) {

        if (theScroller.scrollElements[i].groupId) {


            if (theScroller.scrollElements[i].groupId == theGroupId) {

                theGroupIndex = i;

                theScroller.scrollElements[i].childrenHidden = false;
                groupSize = theScroller.scrollElements[i].groupsContacts.length;

            }
        }
        if (theGroupIndex !== null) {
            break;
        }
    }

    if (theGroupIndex !== null) {

        for (var j = theGroupIndex + 1; j < (theGroupIndex + 1 + groupSize); j++) {

            showAScrollElement(j, theScroller);
        }
    }

}

function pressAGroupCheckBox(groupId) {
    var theGroupObject = inAppDatabases.groups({
        groupId: groupId
    }).first();
    theGroupObject.isSelected = !theGroupObject.isSelected;
    tickAGroupCheckBox(theGroupObject.isSelected, groupId);

}

function pressACheckBox(contactId) {
    var theContactObject = inAppDatabases.phoneContacts({
        phoneContactId: contactId
    }).first();
    theContactObject.isSelected = !theContactObject.isSelected;

    tickAContactCheckBox(theContactObject.isSelected, contactId);

}

function pressAGenericCheckBox(theName, theScrollerName, isNotABullet) {
    var tempScroller = scrollers[theScrollerName];


    tickAGenericCheckBox(tempScroller, theName, isNotABullet);

}






//endregion
//====================================================-


//====================================================-
//Checking Database Data region
//|7|-213|1069|494.44|544.81|1----------------------------------------------------------------------------------------------~
function GetUsersAtActivity(theActivityId) {
    return inAppDatabases.activityConnections({
        activityId: theActivityId
    }).select("includesUser");
}






//endregion
//====================================================-


//====================================================-
//Incoming and Outgoing Data Functions region
//|8|405|1281|763.48|636.55|1----------------------------------------------------------------------------------------------~


function CreateTaffyDbForThese(objectToLoop, tableName) {
    //    inAppDatabases[tableName] = TAFFY(inAppBogusData[tableName]);
    inAppDatabases[tableName] = TAFFY();
    inAppDatabases[tableName].store(tableName, false);
    var tempLocalSortabeDBString = "taffy_" + tableName;
    if (!localStorage[tempLocalSortabeDBString]) {
        inAppDatabases[tableName].merge(inAppBogusData[tableName]);
    }
}

function InterpretAndRunSyncCommand(theSyncCommand) {
    // This is what the syncCommandInterpreter function is

    if (theSyncCommand.type == "Send Activity To Server") {
        TestFunctionForSyncCommand(theSyncCommand.parameters.activityIdToSend);
    } else if (theSyncCommand.type == "SyncInAppContactsToServer") {
        SyncInAppContactsToServer(theSyncCommand.parameters.deletedContactIds, theSyncCommand.parameters.newContactIds, theSyncCommand.parameters.updatedContactIds);
    } else {
        console.error("The syncCommand type was misspelt or not given");
    }
}
// These functions update the elements shown inside different scrollers
function AddElementsToAllUpdates(theStatusIdArray) {
    for (var i = 0; i < theStatusIdArray.length; i++) {
        createStatusScrollElement(theStatusIdArray[i], scrollers.AllUpdates, null, null, true);
    }
}

function AddElementsToFriendsChanel(theActivityId) {
    // if the status type is 1 or 3 from event
    var tempFriendsStatuses = inAppDatabases.statuses({
        statusType: [0, 2],
        activityId: theActivityId
    }).order("timeUpdated").select("statusId");


    for (var i = 0; i < tempFriendsStatuses.length; i++) {
        createStatusScrollElement(tempFriendsStatuses[i], scrollers.ActivityUpdatesFriends, null, null, false);
    }
}

function AddElementsToEveryoneChannel(theActivityId) {
    var tempEveryonesStatuses = inAppDatabases.statuses({
        activityId: theActivityId
    }).order("timeUpdated").select("statusId");
    for (var i = 0; i < tempEveryonesStatuses.length; i++) {
        createStatusScrollElement(tempEveryonesStatuses[i], scrollers.ActivityUpdatesEveryone, null, null, false);
    }
}

// Functions that deal with data, but don't call a server function directly

function UpdateActivityStatuses(theActivityId) {
    // this is what runs when the pull to refresh is done inside and activities live updates brick, Or when an activity is opened (it automatically looks for new statuses) (although this isn't called directly by the pull to refresh or opening activity part, it gets called as a callback from the syncing statuses from server function)    

    var tempScrollElementsDatabase = TAFFY(scrollers.ActivityUpdatesEveryone.scrollElements);
    var lastUpdateTime = tempScrollElementsDatabase().order("timeUpdated").last().timeUpdated;
    // this part uses the last updated time to find any newer statuses and saves their id in an array
    var newFriendsStatusIdArray = inAppDatabases.statuses({
        activityId: theActivityId,
        statusType: [0, 2],
        "timeUpdated": {
            '>': lastUpdateTime
        }
    }).select("statusId");

    var newEveryoneStatusIdArray = inAppDatabases.statuses({
        activityId: theActivityId,
        "timeUpdated": {
            '>': lastUpdateTime
        }
    }).select("statusId");

    // this part loops through the just-created array and creates the found new statuses (they automatically get added to the scrollView)
    for (var i = 0; i < newFriendsStatusIdArray.length; i++) {
        createStatusScrollElement(newFriendsStatusIdArray[i], scrollers.ActivityUpdatesFriends, null, null, false);
    }
    for (var j = 0; j < newEveryoneStatusIdArray.length; j++) {
        createStatusScrollElement(newEveryoneStatusIdArray[j], scrollers.ActivityUpdatesEveryone, null, null, false);
    }

    scrollers.ActivityUpdatesFriends.scrollView.goToFirstPage();
    scrollers.ActivityUpdatesEveryone.scrollView.goToFirstPage();
}

function GetNewContactsArrayFromPhone() {
    var tempCounterTest = 0;

    function onSuccess(contacts) {
        var newContactsArrayTest = [];
        for (var i = 0; i < contacts.length; i++) {
            tempNewContactObject = {};
            tempNewContactObject.contactname = contacts[i].displayName;
            tempNewContactObject.mobileAssignedContactId = contacts[i].id;
            if (contacts[i].emails) {
                tempNewContactObject.email = contacts[i].emails[0].value;
            } else {
                tempNewContactObject.email = null;
            }
            if (contacts[i].phoneNumbers) {
                if (contacts[i].phoneNumbers[0].type === "mobile") {
                    tempNewContactObject.phonenumber1 = contacts[i].phoneNumbers[0].value;
                    if (contacts[i].phoneNumbers.length > 1) {
                        if (contacts[i].phoneNumbers[1].type === "mobile") {
                            if (contacts[i].phoneNumbers[1].value !== contacts[i].phoneNumbers[0].value) {
                                tempNewContactObject.phonenumber2 = contacts[i].phoneNumbers[1].value;
                            }
                        } else {
                            tempNewContactObject.phonenumber2 = null;
                        }
                    } else {
                        tempNewContactObject.phonenumber2 = null;
                    }
                } else {
                    if (contacts[i].phoneNumbers.length > 1) {
                        if (contacts[i].phoneNumbers[1].type === "mobile") {
                            tempNewContactObject.phonenumber2 = contacts[i].phoneNumbers[1].value;
                        } else {
                            tempNewContactObject.phonenumber2 = null;
                        }
                    }
                }
                newContactsArrayTest.push(tempNewContactObject);
            }
        }

        //            newPhoneContacts = JSON.stringify(newContactsArrayTest);
        newPhoneContacts = newContactsArrayTest;
        SyncPhoneContactsToInAppContacts();
    }

    function onError(contactError) {
        alert('onError!');
    }
    if (tempCounterTest === 0) {
        // find all contacts with 'Bob' in any name field
        var options = new ContactFindOptions();
        options.filter = "";
        options.multiple = true;
        options.desiredFields = [navigator.contacts.fieldType.id, navigator.contacts.fieldType.displayName, navigator.contacts.fieldType.name, navigator.contacts.fieldType.phoneNumbers, navigator.contacts.fieldType.emails, navigator.contacts.fieldType.id];
        var fields = [navigator.contacts.fieldType.phoneNumbers];
        navigator.contacts.find(fields, onSuccess, onError, options);
        tempCounterTest += 1;
    }

    //        navigator.contacts.pickContact(function(contact){


    //            if (contact.phoneNumbers[0].type === "mobile") {
    //
    //            }



    //        },function(err){
    //
    //        });


}

function AddNewPhoneContact(mobileAssignedId, name, phoneNumber1, phoneNumber2, email) {
    var tempPhoneContact = {};

    tempPhoneContact.phoneContactId = "temporaryId";
    tempPhoneContact.mobileAssignedContactId = mobileAssignedId;
    tempPhoneContact.connectedToUser = theUser.id;
    tempPhoneContact.mobilizrId = null;
    tempPhoneContact.contactname = name;
    tempPhoneContact.phonenumber1 = phoneNumber1;
    tempPhoneContact.phonenumber2 = phoneNumber2;
    tempPhoneContact.email = email;
    tempPhoneContact.isSelected = false;

    /*
    
    phoneContactId
    mobileSpecificContactId
    connectedToUser
    isOnMobilizr
    mobilizrId
    name
    phone number 1
    phone number 2
    email
    isSelected
    
    */

    inAppDatabases.phoneContacts.insert(tempPhoneContact);
}

function SyncPhoneContactsToInAppContacts() {
    //region This test syncs my contacts to a file on the server as json, so I can use it for testing without  a phone
    //    alert("about to send contacts to server");

    //    $.ajax({
    //        type: 'POST',  
    //        url: 'http://www.mobilizrapp.com/app/mobilizrMainServerScript.php', 
    //        data: {tempContacts : newPhoneContacts},
    //        success: function(response) {
    //            alert("Got contacts back from server");


    //        },
    //        error: function(response) {
    //            alert("Got error, no contacts");
    //        }
    //    });
    // endregion

    // first create a new taffyDb out of the contacts gotten from the phone
    var syncedPhoneContactsDB = TAFFY(newPhoneContacts);
    var syncedPhoneContactIds = syncedPhoneContactsDB().select("mobileAssignedContactId");
    var currentMobileContactsIds = inAppDatabases.phoneContacts().select("mobileAssignedContactId");
    //    var syncedPhoneContactIds = ["mobileId3","mobileId700"];

    // from here http://jsperf.com/difference-between-two-arrays-with-native-javascript-or/11
    function GetDifferenceFromArrays(newArray, originalArray) {
        var i, j, differenceArray = [],
            originalArray_i, newArray_i;
        newArray.sort();
        originalArray.sort();

        j = newArray.length - 1;
        newArray_i = newArray[j];
        for (i = originalArray.length - 1; i >= 0 && j >= 0; i--) {
            originalArray_i = originalArray[i];
            while (newArray_i > originalArray_i) {
                j--;
                newArray_i = newArray[j];
            }
            if (newArray_i !== originalArray_i) differenceArray.push(originalArray_i);
        }
        for (j = 0; j <= i; j++) {
            differenceArray.push(originalArray[j]);

        }
        return differenceArray;

    }

    var contactIdsToDelete = GetDifferenceFromArrays(syncedPhoneContactIds, currentMobileContactsIds);
    var newContactIds = GetDifferenceFromArrays(currentMobileContactsIds, syncedPhoneContactIds);
    var contactsToCheckForUpdate = inAppDatabases.phoneContacts({
        mobileAssignedContactId: syncedPhoneContactIds
    }).select("mobileAssignedContactId");

    // loop through the length of the new id array (either contactsToDelete or contactsToCheckForUpdate of them) and then either find and remove that contact from the inAppDatabase, or find and update that contact in the inAppDataBase
    // For updating contacts
    // FUTURE this part could make note of which part of the contact was updated, so only the updated info for the contact can be sent
    for (var i = 0; i < contactsToCheckForUpdate.length; i++) {
        // taffy contactIdsToDelete[i]
        // I think this function is working so far , might have to check it with more solid bogus data! 
        var tempContactEntry = inAppDatabases.phoneContacts({
            mobileAssignedContactId: contactsToCheckForUpdate[i]
        });
        var tempSyncedContactEntry = syncedPhoneContactsDB({
            mobileAssignedContactId: contactsToCheckForUpdate[i]
        });

        var tempShouldUpdate = false;

        //        tempContactEntry.update({mobileSpecificContactId: tempSyncedContactEntry.mobileSpecificContactId});

        if (tempContactEntry.first().phonenumber1 !== tempSyncedContactEntry.first().phonenumber1) {
            tempContactEntry.update({
                phonenumber1: tempSyncedContactEntry.first().phonenumber1
            });
            tempShouldUpdate = true;
        }
        if (tempContactEntry.first().phonenumber2 !== tempSyncedContactEntry.first().phonenumber2) {
            tempContactEntry.update({
                phonenumber2: tempSyncedContactEntry.first().phonenumber2
            });
            tempShouldUpdate = true;
        }
        if (tempContactEntry.first().email !== tempSyncedContactEntry.first().email) {
            tempContactEntry.update({
                email: tempSyncedContactEntry.first().email
            });
            tempShouldUpdate = true;
        }

        if (tempShouldUpdate !== true) {
            //contactsToCheckForUpdate.splice(i);
        }

        //        tempContactEntry.update({phonenumber1:tempSyncedContactEntry.first().phonenumber1});
        //        tempContactEntry.update({phonenumber2:tempSyncedContactEntry.first().phonenumber2});
        //        tempContactEntry.update({email:tempSyncedContactEntry.first().email});
    }
    // For Deleting contacts
    for (var j = 0; j < contactIdsToDelete.length; j++) {
        inAppDatabases.phoneContacts({
            mobileAssignedContactId: contactIdsToDelete[i]
        }).remove();
    }
    //    // For adding new contacts
    for (var k = 0; k < newContactIds.length; k++) {
        var tempNewContactsInfo = syncedPhoneContactsDB({
            mobileAssignedContactId: newContactIds[k]
        }).first();
        AddNewPhoneContact(tempNewContactsInfo.mobileAssignedContactId, tempNewContactsInfo.contactname, tempNewContactsInfo.phonenumber1, tempNewContactsInfo.phonenumber2, tempNewContactsInfo.email);

    }


    syncCommandQueue.push({
        type: "SyncInAppContactsToServer",
        parameters: {
            deletedContactIds: contactIdsToDelete,
            newContactIds: newContactIds,
            updatedContactIds: contactsToCheckForUpdate
        }
    });


}

function GetDataForEditedActivities() {
    // this sends all the activities paired with their "lastEdited" timestamp, the server then loops through the activities and checks if the last edited timestamp is newer on the server compared to the lastEdited timestamp handed in, if it is , it adds the acitivityId for the updated activity, all the data that has been changed and also the "lastEdited" timestamp for that activity from the server into one object in an array called editedActivitiesData (the sent back image data is a true or flase variable that indicates if the activities picture has been updated on the server).
    // The server then sends back back the editedActivitiesData in an array where the app goes goes through each of the edited activities and sets the new data for that activity in the in-app activities database
    // if the image is listed as having being updated on the server, the image chache will refresh it image (by using the ImgCache.cacheFile(theImagePath) function which will overwrite the previously cached image with whatever is now on the server (from the same image link it had, if thats how the imgCache works)

    ///// SERVER FUNCTION INVOLVED
}

function AddInvitationsToInAppDatabase() {
    // this loops through an array of the currently selected contacts and creates a new invitation (with it's activityId set to the handed in activity id (this does mean the activity has to be sent to the server before the invitations are created (so the invitations can have the server-assigned activityId in them))

    // this part creates a syncCommand to send all the invitations sent by the user to the server
}

function GetActivityDataFromApp(activityIdToGet) {
    return inAppDatabases.activities({
        id: activityIdToGet
    }).first();
}

function RemoveActivity(activityIdToGet) {
    inAppDatabases.activities({
        id: activityIdToGet
    }).remove();

    // this also creates a syncCommand (RemoveActivityFromServer) that sends the server-assigned activity id to the server where the server deletes that activity from the activity table
}

function AddNewActivityInTheApp( /*parameters to add to the new activity?*/ ) {
    // Not gonna be used? since there's a temporary activity object, and the server sending back the activity id (and mabye the image url path?) could add it to the actual app

    // when creating the activity object, this function assigns to it a temporary activity name like "tempOfflineActivityId1"
    // this bit adds the new activity to the in app database
    //       inAppDatabases.activities({id:activityIdToGet}).remove();
    var tempActivityId = "tempActivityId" + globalTempActivityIdCounter;
    globalTempActivityIdCounter += 1;
    // this function also adds a command to the commandQue to Send the new activity to the server
    syncCommandQueue.push({
        type: "Send Activity To Server",
        parameters: {
            activityIdToSend: tempActivityId
        }
    });
}

function JoinAnActivityInApp(activityIdToJoin) {
    // this function marks the user as "IN" for an activity in the in-app database by setting the isAttending property for the activity to true
    // it also deletes the invitation from the in-app invitation database

    // typically at some stage after this is run SendRSVPToServer() will be called 

}

function DeclineAnActivityInApp(activityIdToJoin) {
    // this function marks the user as "OUT" for an activity in the in-app database by setting the isAttending property for the activity to false
    // it also deletes the invitation from the in-app invitation database

    // typically at some stage after this is run SendRSVPToServer() will be called 

}

function SendStatusToInAppDatabase() {

    // this uses the handed in parameters to create a new status and it then saves it to the in-app status database

    // this part creates a syncCommand to send that status to the server
}

function AddNewGroupToInAppDatabase() {
    // this will create the group and add it to the groups in-app database 
    // (it wont populate the group yet it'll just create the actual group 
}

function UpdateGroupsIncludedContacts() {
    // this will loop through all the contacts in the group and check if they are selected in the in-app contacts database, if they arent it deletes that contact-group conntection from the contact-group connections in-app database

    // this will add all the currently selected contacts to the contact-group connection in app database (if they arent already)

    // if there is noone currently selected it will delete the group (and the contact-group connections) from the groups (and contact-group connections) in-app databases

}

function SetToSyncGroupsWithServer() {
    // this sends a syncCommand to the syncCommandQueue to sync all the group with the server, its done through this function so it can be added to the que first (and the other update
}

function TestUploadActivityImageToServer() {
    //TestUploadActivityImageToServerDATA
}

// Server Connected Functions

function CheckAndSyncRelatedMobilizrUsers() {

    // NOTE this one requires the contacts to be synced first?

    var syncingRelatedUsers = {};
    tempSyncingRelatedUsers.usersId = theUser.id;

    // this first check will just send the users Id and get back all the mobilizr users
    $.ajax({
        type: 'POST',
        url: serverScriptUrl,
        data: {
            backendPostType: "CheckAndSyncRelatedMobilizrUsers",
            data: syncingRelatedUsers
        },
        success: function (response) {
            alert("CheckAndSyncRelatedMobilizrUsersDATA was successfull");
            //            var tempResult = JSON.parse(response);

        },
        error: function (response) {
            //            alert("CheckAndSyncRelatedMobilizrUsersDATA wasn't successfull");
        }
    });

    // this sends an array of pairs of mobilizrIds from the in app database which each have an properties of all of that users lastEdited timestamps.

    // the server then finds all of the mobilizr users with phone numbers that match phone numbers stored inside contacts for that user (the contacts have to be synced first), and if it finds any userids that arent on the sent in mobilizrId's list then it adds them to a newMobilizrUsers array. It also loops through the sent in mobilizrId's list and if any of the mobilizrId mateched users are no longer on the server it adds their mobilizrId to an array called deletedMobilizrUserIds

    //  The server then loops through and checks if the "lastEdited" variable for each user is newer on the server than the one sent in, and if it is it finds which of the specific pieces of data are newer in the server and stores them in an object called updatedMobilizrUserData which pairs mobilizrIds with their new data (the picture link will stay the same, so instead for the picture data it sends back a true or false variable for the app to determine if the picture has been updated on the server.

    // the server then sends back the three things: newMobilizrUsers, deletedMobilizrUserIds and updatedMobilizrUserData  in a JSON object

    // the app runs through all the deletedMobilizrUserIds mobilizrId's and deleted those users from the in-app database (and also removes the relevant connected to mobilizrId stuff from the in-app contact

    // the app also runs through the new mobilizr users and adds them to the in-app mobilizr users database
    // the app also runs though the edited mobilizr users and sets the corrosponding data to each of the updated mobilizr users stored in the in-app mobilizr users database. For the image, if the image is listed as having being updated on the server, the image chache will refresh the image (by using the ImgCache.cacheFile(theImagePath) function which will overwrite the previously cached image with whatever is now on the server (from the same image link it had)

    ///// SERVER FUNCTION INVOLVED
}

function GetAllActivityDataFromServer() {
    // this sends an array of objects (converted to JSON) containing all the (server assigned) activity id's of each of the activities in the in-app database paired with that activities in-app isAttending value, the server then loops through them and checks if any of the id's been deleted from the server and stores the (server assigned) id's of those deleted activities into an array. It also adds the user-activity connection for each of the activities based on the isAttending part handed in (if it's not already in the server table) it also deletes the connection if the paired isAttending value is set to false. It then finds all of the activities the user is invited to and stores the linkedActivity (the actual activity data and not just the activity Id) from those invitations in an array. it also stores all of the invitations in an array. The server then sends back each of the generated arrays (deletedActivities, invitedToActivites and invitations) in json format back to the app

    // here the app loops through the deleted activity id's and deletes the corrosponding activities from the in-app database

    // this loops through the returned invitedToActivities and adds each of them to the in-app activity database

    // this loops through all the returned invitations and stores each of them in the in-app invitations array

    ///// SERVER FUNCTION INVOLVED
}

function SendEditedActivityToServer(editedActivityId) {
    // this takes in parameters of which parts of an activity has been edited? if the timestamp for any of the thisPartEdited bits of data for an activity are newer than the "lastEdited" (set on the server) timestamp, then it will put that piece of updated data into an object (this object also has the activityId it is editing).

    // this function is also run when an admin is set for the activity (and it wont be slower? because if no event data has been edited it will only do the updating admins part)

    // The object is sent to the server and then the server goes through it and sets each of the new pieces of data for the activity,
    // The server will always go through and check the admins array sent in and delete any activity admin connections of mobilizrUsers who aren't in the sent in array, and add the new user-admin connections for users sent in that arent already stored in the user-admin connections table
    // If there are multiple parts of an activity edited, the server will create a generic status in the everyone channel for that activity that says something like "This activity has been updated", if only one part of the activity has been updated, it will create a status in the same place that says something like "This activities (the data that was updated) has been updated"
    // there's the potential to create a status to alert people they are either newly an admin for an activity or have been removed as an admin for an activity

    // when the server is done (updating the activities info) it also sets the "lastEdited" property of the edited activity to the current time (as a timestamp) and sends back the "lastEdited" value, where the app sets the in-app database value for that activities "lastEdited" property to the "lastEdited" timestamp it recieved from the server

    ///// SERVER FUNCTION INVOLVED

}

function SendAllCreatedInvitationsToServer() {
    // this get all the stored invitations marked as sent from the user and sends them to the server

    // the server then gives each one a unique server-assigned invitation id and adds it to the servers invitations table

    // when the app gets the message back from the server that they were successfully added the app deletes all the invitations marked as sent by the user from the in-app invitations database

    ///// SERVER FUNCTION INVOLVED
}

function RemoveActivityFromServer(activityIdToDelete) {
    // this sends the server-assigned activity id to the server where the server deletes that activity from the activity table

    ///// SERVER FUNCTION INVOLVED
}

function SendNewActivityToServer( /*tempNewActivityObject*/ ) {

    temporaryActivityObject.dateCreated = Math.floor(Date.now() / 1000);
    //            temporaryActivityObject.allowAnonymous = scrollers.Where.scrollElements[7].isTicked;
    temporaryActivityObject.allowAnonymous = true ? 1 : 0;
    // this part sends the activity to the server
    $.ajax({
        type: 'POST',
        //        url: 'http://www.mobilizrapp.com/app/mobilizrMainServerScript.php',
        url: serverScriptUrl,
        data: {
            backendPostType: "SendNewActivityToServer",
            data: temporaryActivityObject
        },
        success: function (response) {
            alert(response);
        }
    });
    // the server also adds the user to the user-activity connections table?
    // The server also creates two new channels for the activity (a "Friends" and "Everyone" channel)

    // this first creates an in app activity object, and sets the values to the passed in temp activity object
    // this part saves the returned server generated activity id back to the activity object

    ///// SERVER FUNCTION INVOLVED
}

function SubmitNumberToVerify() {
    // this sends the users phone number to the server (it does't create a new user yet) the server generates a random number and letter code, then stores a temporary value in a database which is a phone number and the correct code, when the code is sent in the phone number is sent in again and then the phone number and code matches database is checked to see if they match, and if they do, it returns a true which allows the app to continue to the next step.

    if (isAValidPhoneNumberBoolean) {
        var temporaryDataObject = {
            phoneNumber: theUser.p1
        };
        console.log(temporaryDataObject);
        $.ajax({
            type: 'POST',
            //        url: 'http://www.mobilizrapp.com/app/mobilizrMainServerScript.php',
            url: serverScriptUrl,
            data: {
                backendPostType: "SubmitNumberToVerify",
                data: temporaryDataObject
            },
            success: function (response) {
                console.log(response);
            }
        });
        return true;
    } else {
        return false;
    }
}

function SubmitCodeToVerify() {
    var tempCode = scrollers.EnterCode.scrollElements[1].numberInput.getValue().toString();
    var temporaryDataObject = {
        phoneNumber: theUser.p1,
        code: tempCode
    };
    $.ajax({
        type: 'POST',
        //        url: 'http://www.mobilizrapp.com/app/mobilizrMainServerScript.php',
        url: serverScriptUrl,
        data: {
            backendPostType: "SubmitCodeToVerify",
            data: temporaryDataObject
        },
        success: function (response) {
            console.log(response);
        }
    });
}

function SyncGroupWithServer() {
    // NOTE: Ideally all these group functions would only try to sync groups that the app knows has been edited
    // since groups cant be edited externally (there's no need to chck for any external changes to a group)

    // this sends to the server a list of group ids

    // the server then checks if there are any new groups that arent on the server, and if they aren't it sets a server-assigned group id to each one and adds it to the server groups table, it also sends back an array of objects with pairs of the sent through temporaryGroupId and the new server assigned id, so the groups in the app can be assigned the server created group id's, and if they are on the server but not on the sent over list it deletes the remaning groups (based on their server-assigned group id's) relevant to that user from the server. When it deletes a group from the server it also deletes all the user-group connections database entries related to that group.

    // The app assigns the sent back variables to the in-app groups database, it then also renames the groupId part of all the group-contact connections in-app database entries that have the old temporary group id's in them

    // it then sends all the group-contact connections from the in-app database to the server , where the server adds any group-contact connections that are new to the server table and deletes any that are on the server but not on the sent through list.

    ///// SERVER FUNCTION INVOLVED

}

function GetNewChannelsFromServer() {
    // this sends an array of all the channel id's from the in-app channel database (and the user id) and the server then compares the handed in channel id's with an array of channel id's related to that user (related to the user-channel connections table), if it finds channels related to the user that werent handed in it puts them in an array called newChannels, and if it doesnt find a channel matching one of the channel id's sent in, it will add that sent in channel id to an array called deletedChannelIds. The server will then send back a json object with the two arrays as properties

    // using the sent back deletedChannelIds array the app will delete all the channels with channel id's from the sent back array

    // The app will then add all the channels sent back from the server to the in-app channel database

    ///// SERVER FUNCTION INVOLVED
}

function SendAStatusToTheServer(temporaryStatusId) {
    // TODO make sure the quotation marks in the status text have \ escape characters before them, before sending to the server

    // this function also returns a server assigned statusId that the app uses to replace the temporary statusId it gave to the status in the in-app status database

    // (the statuses are deleted from the database when the activity is over? and the app will remove the content of a status in its in-app database if it's over 24 hours old (based on its timestamp) (so the status is still stored as what it was sent as on the server, it is just no longer displayed on users phones anymore)

    ///// SERVER FUNCTION INVOLVED
}

function SendRSVPToServer() {
    // this sends the users mobilizrId and an activityId and whether or not that person is attending that activity
    // the server uses that data to add the user to the servers activity-user connections table if they are attending, if they arent it does nothing here
    // the server then deletes the invitation for that activity from the servers invitations table.

    ///// SERVER FUNCTION INVOLVED
}

function GetStatusesFromServer() {
    // this function finds the status with the latest timestamp and sends that timestamp to the server, it also sends an array of all the statusId's from the in-app database. The server sends back all the statuses connected to that user (through the users connected channels) that are newer than the sent in timestamp in an array called newStatuses (this means the function on the server will also find statuses that have been updated since the timestamp for those statuses will be set to a later time), The server will also loop through the statusId's from each of the sent in statusId's and if it can find one it adds it to an array called deletedStatuses.

    // the app then loops through the new statuses sent back, if it cant find a status in the in-app database that has the same statusId as the currently looped sent in status, it adds the whole status to the in-app status database. If the statusId of one of the sent in statuses is allready in the in-app status database, it replaces the data for that status with the new data sent in. The app also loops through all of the deleted statusId's sent in and deletes the corrosponding stsuses from the in-app status database

    ///// SERVER FUNCTION INVOLVED
}

function SendNewUserToServer() {
    // this function is not called by a sync command because the user has gotta be connected to the internet to make the account

    // this sends the data for the newly created mobilizr user and the server sends back a server-assigned userid
    // it happens when a valid sms code is sent back?

    //     var tempTestSingleUser = theUser;
    var tempTestSingleUser = temporaryUserObject;

    $.ajax({
        type: 'POST',
        url: 'http://www.mobilizrapp.com/app/mobilizrMainServerScript.php',
        data: {
            backendPostType: "SendNewUserToServer",
            data: tempTestSingleUser
        },
        success: function (response) {
            //            alert("Got info back from server");
        },
        error: function (response) {
            //            alert("Got error");
        }
    });

    // The app user is not listed in the mobilizr user parts, and instead is listed in its own object called theAppUser
    // which stores exactly the same info as a standard mobilizr user , it's just in a seperate place.

    ///// SERVER FUNCTION INVOLVED
}

function UpdateUserInfoToServer() {
    /* this takes goes through and checks all the "thisThingUpdated" timestamp for the user and if its newer than the "lastEdited" timestamp (set by the server previously) then it sends that data in an object , the object also has the phone users mobilizrId (so the server knows which one to edit). The server then looks through all the updated data in the sent through object and sets the corrosponding data for that mobilizr user in the servers Users database.

    */

    ///// SERVER FUNCTION INVOLVED
}

function DeleteUserFromServer() {
    // this sends over the mobilizrId to be deleted where the server will delete it from the users database and also delete all the connections with that user and all the activities created by that user, it will also delete the mobilizrId part of phone contacts that are linked to that mobilizrId, as well as setting the isOnMobilizr property for that phone contact to false.

    ///// SERVER FUNCTION INVOLVED
}

function SyncInAppContactsToServer(deletedContactIds, newContactIds, updatedContactIds) {
    //    alert(deletedContactIds[0] + "  " + newContactIds[0] + " " + updatedContactIds[0]);

    // creates a new object with the 3 parameters and also the users mobilizr id and also the array of phoneContacts that are new

    // sends an object with : the users mobilizr id, an array of all the new phoneContacts, an array of all the updated phoneContacts, an array of all the deleted phoneContact's phoneAssignedId's

    var syncingContactsObject = {};
    syncingContactsObject.usersId = theUser.id;
    syncingContactsObject.newContacts = inAppDatabases.phoneContacts({
        mobileAssignedContactId: newContactIds
    }).get();
    // This part is returning an empty array for some reason? (only now? woth the new placeholder data?
    syncingContactsObject.updatedContacts = inAppDatabases.phoneContacts({
        mobileAssignedContactId: updatedContactIds
    }).get();
    //    syncingContactsObject.deletedContacts = deletedContactIds;
    syncingContactsObject.deletedContacts = [1, 2];

    // instead of delete the properties directly, I think that deltes it from being able to be used from anywhere, instead mabye make a copy of the array first?

    for (var i = 0; i < syncingContactsObject.newContacts.length; i++) {
        delete syncingContactsObject.newContacts[i].___id;
        delete syncingContactsObject.newContacts[i].___s;
        //        delete syncingContactsObject.newContacts[i].isSelected;
        //        delete syncingContactsObject.newContacts[i].contactname;
    }

    for (var j = 0; j < syncingContactsObject.updatedContacts.length; j++) {
        delete syncingContactsObject.updatedContacts[j].___id;
        delete syncingContactsObject.updatedContacts[j].___s;
        //        delete syncingContactsObject.updatedContacts[j].isSelected;
        //        delete syncingContactsObject.updatedContacts[j].contactname;
    }


    $.ajax({
        type: 'POST',
        url: 'http://www.mobilizrapp.com/app/mobilizrMainServerScript.php',
        data: {
            backendPostType: "SyncInAppContactsToServer",
            data: syncingContactsObject
        },
        success: function (response) {
            //            alert("SyncInAppContactsToServerDATA was successfull");
            //            var tempResult = JSON.parse(response);
            //            var tempDateJoined = tempResult[0].dateJoined;
        },
        error: function (response) {
            //            alert("SyncInAppContactsToServerDATA wasn't successfull");
        }
    });


    // it sends an array of the mobileAssignedId's of the deleted contacts, and then sends the phoneContacts inAppDatabase exlcuding the deleted contacts as a json string
    // the server then finds all the newly sent in phonecontacts that have the temporary id (based on the sent in newContactIds array) and it gives them a new id? then it creates an object  which is an array of name:value pairs , the name is the phoneAssignedId and the value is the new serverAssignedId.

    // None of the checking if the user is on mobilizr part needs to happen here as it's all done in a seperate app-and-server function which finds which of the users phone contacts are also on mobilizr

    // the server will then send back an object which contains pairs of mobile-assigned contact id with the servers newly assigned contact id, this function will then use that returned data to assign the server assigned id to each of the phone contacts in the in-app database

    ///// SERVER FUNCTION INVOLVED
}


//endregion
//====================================================-


//====================================================-
//General Creating Functions region
//|30|5131|-1056|661.51|877.97|1----------------------------------------------------------------------------------------------~
function CreateSwipablePage(theObject, loopedKey) {
    // This makes one of the pages views in a horizontal draggable set of pages
    // this could potentially take info from the draggable it's attatched to (or even, the views could be stored in the draggable?

    // instead of just creating it with a modifier , it should create the view then add it to a view array in the correct order
    var tempPageId = theObject[loopedKey].id;
    var tempTitleBarHeight = draggables[getViewNameFromObject(theObject)].data.yOffset;
    var tempPageWidth = draggables[getViewNameFromObject(theObject)].data.pageWidth;
    var tempPageHeight = draggables[getViewNameFromObject(theObject)].data.pageHeight;
    var tempSurface = new ContainerSurface({
        size: [tempPageWidth, tempPageHeight],
        properties: {
            backgroundColor: "#fff",
            textAlign: 'center',
            cursor: 'pointer',
            color: themeColors.dark,
            overflow: 'hidden'
        }
    });

    theObject[loopedKey].view = tempSurface;
}

function CreateHorizontalDraggableOld(theObject, loopedKey) {
    // the bits saying "draggables" can be replaced with "theObject" no problem
    draggables[loopedKey].draggable = new Draggable({
        xRange: [-draggables[loopedKey].data.pageWidth * draggables[loopedKey].data.pageAmount, 0],
        yRange: [0, 0]
    });

    if (draggables[loopedKey] != draggables.SideBar) {
        draggables[loopedKey].container = new ContainerSurface({
            size: [draggables[loopedKey].data.pageWidth, draggables[loopedKey].data.pageHeight],
            properties: {
                backgroundColor: 'rgba(0, 0, 0, 0)',
                textAlign: 'center',
                cursor: 'pointer',
                color: themeColors.dark,
                overflow: 'hidden'
            }
        });

        draggables[loopedKey].containerModifier = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.5, 0.5],
            transform: Transform.translate(0, 0, 0)
        });
    }

    draggables[loopedKey].modifier = new StateModifier({
        origin: [0, 0],
        align: [0, 0],
        transform: Transform.translate(0, 0, draggables[loopedKey].data.zDepth)
    });
}

function CreateHorizontalDraggable(theObject, loopedKey) {
    // it's first gotta create an array out of all of its related views
    draggables[loopedKey].viewsArray = [];
    if (draggables[loopedKey].views) {
        for (var i = 0; i < Object.keys(draggables[loopedKey].views).length; i++) {
            for (var viewKey in draggables[loopedKey].views) {
                if (draggables[loopedKey].views.hasOwnProperty(viewKey)) {
                    if (draggables[loopedKey].views[viewKey].id === i) {
                        draggables[loopedKey].viewsArray.push(draggables[loopedKey].views[viewKey].view);
                    }
                }
            }
        }
    }

    // the bits saying "draggables" can be replaced with "theObject" no problem
    //    draggables[loopedKey].draggable = new Draggable({
    //        xRange: [-draggables[loopedKey].data.pageWidth * draggables[loopedKey].data.pageAmount, 0],
    //        yRange: [0, 0]
    //    });

    if (draggables[loopedKey] != draggables.SideBar) {
        draggables[loopedKey].container = new FlexScrollView({
            layout: ListLayout,
            layoutOptions: {
                margins: [0, 0, 0, 0],
                spacing: 0,
                isSectionCallback: function (renderNode) {
                    return renderNode.isSection;
                },
            },
            dataSource: draggables[loopedKey].viewsArray,
            overscroll: false,
            useContainer: true,
            autoPipeEvents: true,
            flow: true,
            mouseMove: shouldUseMouse,
            touchMoveDirectionThresshold: 0.3,
            container: { // options passed to the ContainerSurface
                size: [draggables[loopedKey].data.pageWidth, draggables[loopedKey].data.pageHeight],
                properties: {
                    textAlign: 'center',
                    cursor: 'pointer',
                    overflow: 'hidden'
                }
            },
            scrollSpring: {
                dampingRatio: 1.0,
                period: 150
            },
            direction: 0,
            paginated: true
        });

        draggables[loopedKey].containerModifier = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.5, 0.5],
            transform: Transform.translate(0, 0, 0)
        });
    }

    draggables[loopedKey].modifier = new StateModifier({
        origin: [0, 0],
        align: [0, 0],
        transform: Transform.translate(0, 0, draggables[loopedKey].data.zDepth)
    });
}

function CreateNewView(theObject, loopedKey) {
    theObject[loopedKey] = new View({
        size: [originalWidth, originalHeight]
    });
}

function CreateNewBasicViewAsChild(theObject, loopedKey) {
    theObject[loopedKey].view = new View();
}

function CreateNewViewAsChild(theObject, loopedKey) {
    var tempView = new View();
    tempView.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            //            backgroundColor: 'hsl(' + (1 * 360 / 30) + ',50%,90%)',
            backgroundColor: themeColors.viewColour,
            textAlign: 'center',
            cursor: 'pointer',
            color: themeColors.dark,
            overflow: 'hidden'
        }
    });

    tempView.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5]
    });

    //    tempView.add(tempView.mod).add(tempView.surface);
    tempView.add(tempView.mod).add(tempView.container);
    theObject[loopedKey].view = tempView;
}

function CreateNewScrollView(theObject, loopedKey) {
    var shouldAlwaysScroll = false;
    var doesItPullToRefresh = false;
    var thePullToRefreshPart = false;

    // Should Always Update
    if (theObject[loopedKey] === scrollers.ActivityUpdatesFriends || theObject[loopedKey] === scrollers.ActivityUpdatesEveryone || theObject[loopedKey] === scrollers.ActivityUpdatesChannels || theObject[loopedKey] === scrollers.ContactsPeople || theObject[loopedKey] === scrollers.ContactsGroups || theObject[loopedKey] === scrollers.Privacy || theObject[loopedKey] === scrollers.HelpFAQ) {
        shouldAlwaysScroll = true;
    }
    // Should have a "Pull To Refresh" part
    if (theObject[loopedKey] === scrollers.AllUpdates || theObject[loopedKey] === scrollers.Invitations || theObject[loopedKey] === scrollers.JoinedActivities || theObject[loopedKey] === scrollers.CreatedActivities || theObject[loopedKey] === scrollers.ActivityUpdatesFriends || theObject[loopedKey] === scrollers.ActivityUpdatesEveryone || theObject[loopedKey] === scrollers.ContactsPeople || theObject[loopedKey] === scrollers.ContactsGroups) {

        theObject[loopedKey].pullToRefreshHeader = new RefreshLoader({
            size: [undefined, 60],
            pullToRefresh: true,
            pullToRefreshBackgroundColor: 'white'
        });
        doesItPullToRefresh = true;
        thePullToRefreshPart = theObject[loopedKey].pullToRefreshHeader;
    }

    // Create pull to refresh header
    theObject[loopedKey].scrollView = new FlexScrollView({
        layout: ListLayout,
        layoutOptions: {
            margins: [0, 0, 0, 0],
            spacing: 0,
            isSectionCallback: function (renderNode) {
                return renderNode.isSection;
            },
        },
        dataSource: theObject[loopedKey].scrollElements,
        overscroll: doesItPullToRefresh,
        useContainer: true,
        autoPipeEvents: true,
        flow: true, // enable flow-mode (can only be enabled from the constructor)
        insertSpec: { // render-spec used when inserting renderables
            opacity: 0, // start opacity is 0, causing a fade-in effect,
            size: [0, 0] // uncommented to create a grow-effect
                //transform: Transform.translate(-300, 0, 0) // uncomment for slide-in effect
        },
        mouseMove: shouldUseMouse,
        touchMoveDirectionThresshold: 0.3,
        //        reflowOnResize: true,
        alwaysLayout: true, //shouldAlwaysScroll, // this is set when a person starts searching?
        //removeSpec: {...},    // render-spec used when removing renderables
        nodeSpring: { // spring-options used when transitioning between states
            dampingRatio: 0.8, // spring damping ratio
            period: 1000 // duration of the animation
        },
        container: { // options passed to the ContainerSurface
            size: [theObject[loopedKey].data.width, theObject[loopedKey].data.height],
            properties: {
                textAlign: 'center',
                overflow: 'hidden',
                borderTop: "0.1em solid " + themeColors.light,
            }
        },
        pullToRefreshHeader: thePullToRefreshPart
    });

    if (doesItPullToRefresh !== false) {
        theObject[loopedKey].scrollView.on('refresh', function (event) {
            UpdateActivityStatuses(1);

            // instead of the timeout hiding it, the callback from the syncing function can hide it (so the specific syncing function needs to know which scroller to hide the pull-to-refresh part of

            Timer.setTimeout(function () {
                theObject[loopedKey].scrollView.hidePullToRefresh(event.footer);
            }, 200);
        });
    }

    if (theObject[loopedKey] == scrollers.SideBar) {
        theObject[loopedKey].modifier = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.5, 0.5],
            transform: Transform.translate(0, standardScrollElementHeight, 502)
        });
    } else {
        theObject[loopedKey].modifier = new StateModifier({
            origin: [0.5, 0],
            align: [0.5, 0],
            transform: Transform.translate(0, theObject[loopedKey].data.heightOffset, theObject[loopedKey].data.zDepth)
        });
    }
    //    theObject[loopedKey].scrollView.sequenceFrom(theObject[loopedKey].scrollElements);
    theObject[loopedKey].scrollView.reflowLayout();
    // set it to always draw (then after a second set it to change the options to not automatically redraw
    if (!shouldAlwaysScroll) {
        Timer.setTimeout(function () {
            theObject[loopedKey].scrollView.setOptions({
                alwaysLayout: false
            });
            //
            //
        }, 1000);
    }
}

function setDefaultScrollerData(theObject, loopedKey) {
    if (theObject[loopedKey] != scrollers.SideBar && theObject[loopedKey] != scrollers.ActivityInfo && theObject[loopedKey] != scrollers.ActivityUpdatesFriends && theObject[loopedKey] != scrollers.ActivityUpdatesEveryone && theObject[loopedKey] != scrollers.ActivityUpdatesChannels && theObject[loopedKey] != scrollers.ContactsPeople && theObject[loopedKey] != scrollers.ContactsGroups && theObject[loopedKey] != scrollers.PremadeStatuses && theObject[loopedKey] != scrollers.PremadeStatusesMood) {
        theObject[loopedKey].data = defaultScrollerData;
    }
}

function CreatePictureButton(thePictureButton) {
    var tempView = new View();
    tempView.isEnabled = true;

    var tempIconSize = defaultIconSize;

    if (thePictureButton.data.customSize) {
        tempIconSize = thePictureButton.data.customSize;
    }

    tempView.mod = new StateModifier({
        origin: [0.5, 0.5],
        // not sure why the x value has to be offset to the left by 0.5, because the text button doesnt have the same issue and its created and added in a simmilar way, turns out this problem is brick specific (same with the title texts)
        align: [thePictureButton.data.alignX, thePictureButton.data.alignY],
        transform: Transform.translate(0,
            0,
            thePictureButton.data.zPos
        )
    });

    tempView.imageMod = new StateModifier({
        origin: [0.5, 0.5],
        // not sure why the x value has to be offset to the left by 0.5, because the text button doesnt have the same issue and its created and added in a simmilar way, turns out this problem is brick specific (same with the title texts)
        align: [0.5, 0.5],
        transform: Transform.translate(0,
            0,
            thePictureButton.data.zPos
        )
    });
    // the way it's done is a bit hacky ATM , it just sets the size of the container to 1x1 pixel so the image pretty much has to go where the container is?
    tempView.container = new ContainerSurface({
        size: [1, 1],
        properties: {
            cursor: 'crosshair'
        }
    });

    tempView.surface = new ImageSurface({
        size: [tempIconSize, tempIconSize],
        content: thePictureButton.data.imagePath,
        properties: {}
    });

    tempView.SetEnabled = function () {
        tempView.isEnabled = true;
        tempView.surface.setContent(thePictureButton.data.imagePath);
    };

    tempView.SetDisabled = function () {
        tempView.isEnabled = false;
        tempView.surface.setContent(thePictureButton.data.imageDisabledPath);
    };

    tempView.surface.on('click', thePictureButton.data.pressedFunction);


    tempView.container.add(tempView.imageMod).add(tempView.surface);
    tempView.add(tempView.mod).add(tempView.container);
    thePictureButton.view = tempView;
}

function CreateTextButton(theTextButton, isARequestStatus, userId) {
    var tempView = new View();
    var tempYPos = 0;
    var tempFontSize = fontSizes.tiny;
    var tempText = "Request<br>Status";
    var lineHeightInPixels;
    var tempDefaultColor = themeColors.main;

    var borderProperty = 'none';
    if (theTextButton === textButtons.Hide || theTextButton === textButtons.UpdatesHide) {
        borderProperty = '0.1em solid';
    }

    tempView.isEnabled = true;

    if (isARequestStatus) {
        lineHeightInPixels = ToPixelString((GetNumberFromString(tempFontSize)) * defaultFontHeight * 1.2);
        tempView.mod = new StateModifier({
            origin: [0.5, 0.5],
            align: [1, 0],
            transform: Transform.translate(-(defaultFontWidth * 3), defaultFontHeight * 1.5, 0)
        });
    } else {
        tempFontSize = theTextButton.data.fontSize;
        if (theTextButton.data.defaultColor) {
            tempDefaultColor = theTextButton.data.defaultColor;
        }
        lineHeightInPixels = ToPixelString((GetNumberFromString(tempFontSize)) * defaultFontHeight * 1.7);
        tempText = theTextButton.data.text;
        if (theTextButton.data.yPos) {
            tempYPos = theTextButton.data.yPos;
        }

        tempView.mod = new StateModifier({
            origin: [0.5, 0.5],
            align: [theTextButton.data.alignX, theTextButton.data.alignY],
            transform: Transform.translate(0,
                tempYPos,
                theTextButton.data.zPos
            )
        });
    }

    var paddingInPixels = ToPixelString((GetNumberFromString(tempFontSize)) * defaultFontHeight * 0.3);
    var paddingInPixelsHalved = ToPixelString(GetNumberFromString(tempFontSize) * 0.8);

    tempView.surface = new Surface({
        size: [true, true],
        content: tempText,
        properties: {
            lineHeight: lineHeightInPixels,
            textAlign: 'center',
            cursor: 'pointer',
            fontSize: tempFontSize,
            color: tempDefaultColor,
            border: borderProperty,
            borderRadius: '5px',
            paddingLeft: paddingInPixels /*'5px'*/ ,
            paddingRight: paddingInPixels /*'5px'*/ ,
            paddingTop: paddingInPixelsHalved /*'2px'*/ ,
            paddingBottom: paddingInPixelsHalved /*'2px'*/
        }
    });
    tempView.SetEnabled = function () {
        tempView.isEnabled = true;
        tempView.surface.setProperties({
            color: themeColors.main
        });
    };
    tempView.SetDisabled = function () {
        tempView.isEnabled = false;
        tempView.surface.setProperties({
            color: themeColors.grey
        });
    };

    tempView.add(tempView.mod).add(tempView.surface);

    if (isARequestStatus) {
        return tempView;
    } else {
        tempView.surface.on('mousedown', function(event){
            theTextButton.data.pressedFunction();
            preventFurtherClicks(event);
        });
        theTextButton.view = tempView;
    }

}

function CreateTitleText(titleGroupObject, loopedKey) {
    var tempTitleView = new View();

    // Text inside a surface
    tempTitleView.surface = new Surface({
        size: [true, true],
        content: titleGroupObject[loopedKey].text,
        properties: {
            textAlign: 'center',
            fontSize: fontSizes.small,
            color: themeColors.dark,
            //        fontWeight: "bold"
        }
    });

    // Modifier for the text
    tempTitleView.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.045],
        transform: Transform.translate(0, 0, 2)
    });
    tempTitleView.add(tempTitleView.mod).add(tempTitleView.surface);
    titleGroupObject[loopedKey].view = tempTitleView;
}

function CreateScreenDisabler() {
    screenDisabler.view = new View();

    screenDisabler.renderController = new RenderController();

    // a surface that's one colour
    screenDisabler.surface = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: themeColors.darkGlass,
            ////            lineHeight: '200px',
            textAlign: 'center',
            cursor: 'pointer'
                //                color: themeColors.light
        }
    });

    screenDisabler.modifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, 0, 1)
    });

    screenDisabler.view.add(screenDisabler.modifier).add(screenDisabler.surface);
    //a function to fade it in (and set it's depth to be higher?, and a function to fade it out (and also make it not interactable (could just make its z depth real low?)

}

function CreateBrick() {
    var brickZDepth = 9000;

    theBrick.isUp = false;

    // render controller? (set outside of this function?
    theBrick.renderController = new RenderController();

    // container surface
    theBrick.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: themeColors.light,
            //            lineHeight: '200px',
            textAlign: 'center',
            cursor: 'pointer'
        }
    });

    theBrick.shadow = new ImageSurface({
        size: [shadowSize, shadowSize],
        content: "./Images/brickShadow.svg",
        properties: {}
    });

    theBrick.shadowChain = new ModifierChain();

    theBrick.shadowScaleModifier = new StateModifier({
        transform: Transform.scale(originalWidth * 1.1, 1, 1)
    });

    theBrick.shadowPositionModifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0, 0],
        transform: Transform.translate(0, -(shadowSize * 0.5), 0)
    });
    // Functions 
    theBrick.SetBrickContent = function (brickContentView) {
        if (theBrick.isUp === true) {
            theBrick.HideBrick();
        }
        theBrick.renderController.show(brickContentView);
    };

    theBrick.MoveBrickUp = function (heightDistance) {
        theBrick.isUp = true;
        theBrick.modifier.setTransform(Transform.translate(0, originalHeight - heightDistance, brickZDepth), easeTransition);
        screenDisabler.renderController.show(screenDisabler.view);
    };

    theBrick.HideBrick = function () {
        theBrick.isUp = false;
        theBrick.modifier.setTransform(Transform.translate(0, originalHeight, brickZDepth), easeTransition);
        screenDisabler.renderController.hide();
        theBrick.renderController.hide();
    };

    theBrick.shadowChain.addModifier(theBrick.shadowScaleModifier);
    theBrick.shadowChain.addModifier(theBrick.shadowPositionModifier);

    theBrick.modifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, originalHeight, 1000)
    });

    theBrick.container.add(theBrick.renderController);
    //        theBrick.container.add(theBrick.shadowChain).add(theBrick.shadow);

}

function CreateActivityUpdatesBrick(theContainerParent) {
    var brickZDepth = 11;

    // no render controller because the Activity Updates Tab always needs to stay visible

    // container surface
    theContainerParent.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: themeColors.light,
            textAlign: 'center',
            cursor: 'pointer'
        }
    });

    theContainerParent.isUp = false;

    // Functions 
    theContainerParent.SetBrickContent = function (brickContentView) {
        activityContainers.UpdatesTabs.renderController.show(brickContentView);
    };

    theContainerParent.MoveBrickUp = function (heightDistance) {
        activityContainers.UpdatesTabs.renderController.show(activityContainers.UpdatesTabs.viewToShow);
        activityContainers.UpdatesBar.renderController.show(activityContainers.UpdatesBar.upContainer, easeTransitionQuick);
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight - heightDistance, brickZDepth), easeTransition);
        theContainerParent.isUp = true;
        activityContainers.UpdatesTabs.postStatusBrick.HideBrick();
    };

    theContainerParent.HideBrick = function () {
        activityContainers.UpdatesTabs.renderController.hide();
        activityContainers.UpdatesBar.renderController.show(activityContainers.UpdatesBar.downContainer, easeTransitionQuick);
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight - activityUpdatesBarHeight, brickZDepth), easeTransition);
        theContainerParent.isUp = false;
        activityContainers.UpdatesTabs.postStatusBrick.CompletelyHideBrick();
    };

    theContainerParent.modifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, originalHeight - activityUpdatesBarHeight, brickZDepth)
    });
}

function CreateDatePickerBrick(theContainerParent) {
    var brickZDepth = 9001;
    var brickHeight = originalWidth * 0.7;

    theContainerParent.isUp = false;
    // render controller? (set outside of this function?
    theContainerParent.renderController = new RenderController();

    theContainerParent.datePickerModifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0] //,
            //        transform: Transform.translate(0, -200, 1000)
    });

    theContainerParent.theActualPickerThing = new DateTimeExample();
    theContainerParent.pickerView = new View();
    theContainerParent.pickerView.add(theContainerParent.datePickerModifier).add(theContainerParent.theActualPickerThing);

    theContainerParent.container = new ContainerSurface({
        size: [originalWidth, brickHeight],
        properties: {
            //            backgroundColor: "red",
            //            lineHeight: '200px',
            //             background:'#ccc',
            //            opacity:0.99,
            //            overflow:'hidden',
            textAlign: 'center',
            cursor: 'pointer'
        }
    });

    theContainerParent.modifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, originalHeight, 0)
    });

    // Functions 
    theContainerParent.SetBrickContent = function (brickContentView) {
        if (theContainerParent.isUp === true) {
            theContainerParent.HideBrick();
        }
        theContainerParent.renderController.show(brickContentView);
    };

    theContainerParent.MoveBrickUp = function () {
        var tempTheDate = null;
        var tempStoredDate = null;
        var tempMomentTimeString = null;

        if (scrollers.When.scrollElements[0].isFocused === true) {
            // this checks if the temporary acitivty object has a date set in it? if it doesnt then nothing happens, if it does, then it sets the date on the date picker to the date in the startTime property of the temporary activity object
            if (temporaryActivityObject.timeStarting === null) {
                tempTheDate = datePickerBrick.theActualPickerThing.datePicker.getDate();
                temporaryActivityObject.timeStarting = Math.floor(tempTheDate.getTime() / 1000);
            } else {
                tempStoredDate = temporaryActivityObject.timeStarting * 1000;
                datePickerBrick.theActualPickerThing.datePicker.setDate(new Date(tempStoredDate));
            }

        } else if (scrollers.When.scrollElements[1].isFocused === true) {
            if (temporaryActivityObject.timeEnding === null) {
                tempTheDate = datePickerBrick.theActualPickerThing.datePicker.getDate();
                temporaryActivityObject.timeEnding = Math.floor(tempTheDate.getTime() / 1000);
            } else {
                tempStoredDate = temporaryActivityObject.timeEnding * 1000;
                datePickerBrick.theActualPickerThing.datePicker.setDate(new Date(tempStoredDate));
            }
        }

        datePickerBrick.theActualPickerThing.datePicker.on('scrollend', function () {
            if (scrollers.When.scrollElements[0].isFocused === true) {
                console.log(datePickerBrick.theActualPickerThing.datePicker);
                tempTheDate = datePickerBrick.theActualPickerThing.datePicker.getDate();
                // 				alert(tempTheDate);
                console.warn("tempTheDate");
                console.warn("tempTheDate");
                console.warn("tempTheDate");
                console.warn("tempTheDate");
                console.warn("tempTheDate");
                console.warn("tempTheDate");
                console.warn("tempTheDate");
                console.warn(tempTheDate);
                temporaryActivityObject.timeStarting = Math.floor(tempTheDate.getTime() / 1000);
                tempMomentTimeString = moment.unix(temporaryActivityObject.timeStarting).format('MMMM Do h:mm a, YYYY (dddd)');
                scrollers.When.scrollElements[0].textInput.setContent(tempMomentTimeString);
                scrollers.When.scrollElements[0].textInputHighlight.setContent(tempMomentTimeString);


                setNewLockedLimit(draggables.CreateActivity, 7);
                //                alert("The Starting date is " + temporaryActivityObject.timeStarting);
            } else if (scrollers.When.scrollElements[1].isFocused === true) {
                tempTheDate = datePickerBrick.theActualPickerThing.datePicker.getDate();
                temporaryActivityObject.timeEnding = Math.floor(tempTheDate.getTime() / 1000);
                tempMomentTimeString = moment.unix(temporaryActivityObject.timeEnding).format('MMMM Do h:mm a, YYYY (dddd)');
                scrollers.When.scrollElements[1].textInput.setContent(tempMomentTimeString);
                scrollers.When.scrollElements[1].textInputHighlight.setContent(tempMomentTimeString);
                //                alert("The Ending date is " + temporaryActivityObject.timeEnding);
            }
        });


        theContainerParent.SetBrickContent(datePickerBrick.pickerView);
        theContainerParent.isUp = true;
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight - brickHeight, brickZDepth), easeTransition);
        //        screenDisabler.renderController.show(screenDisabler.view);
    };

    theContainerParent.HideBrick = function () {
        theContainerParent.isUp = false;
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight, brickZDepth), easeTransition);
        //        screenDisabler.renderController.hide();
        //        theContainerParent.renderController.hide();
    };

    // A function that checks which of the two datePick boxes are focused, and then sets the date in the date thing to be that (this can happen on the show brick part

    // add an event listener in here for the scrollend and have it check which box is focused, and then set the temporary activity object's correct start and end time values, and then set the text content of that box to be the human readable (converted using moment.js) date and time text

    theContainerParent.container.add(theContainerParent.renderController);
    //        theContainerParent.container.add(theContainerParent.shadowChain).add(theContainerParent.shadow);
}

function CreatePostStatusBrick(theContainerParent) {
    var brickZDepth = 1110;

    // this actual brick thing doesnt have a render controller because the Activity Updates Tab always needs to stay visible
    theContainerParent.renderController = new RenderController();

    // container surface
    theContainerParent.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: themeColors.main,
            textAlign: 'center',
            cursor: 'pointer'
        }
    });
    theContainerParent.modifier = new StateModifier({
        origin: [0.5, 1],
        align: [0.5, 1],
        transform: Transform.translate(0, originalHeight - activityUpdatesBarHeight, brickZDepth)
    });

    theContainerParent.isUp = false;
    theContainerParent.isCompletelyHidden = true;
    // Functions
    theContainerParent.SetBrickContent = function (brickContentView) {
        theContainerParent.renderController.show(brickContentView);
    };
    theContainerParent.MoveBrickUp = function (heightDistance, showPresets) {
        if (showPresets) {
            theContainerParent.SetBrickContent(theContainerParent.presetsContainer);
        } else {
            theContainerParent.SetBrickContent(theContainerParent.largeContainer);
        }
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight - heightDistance, brickZDepth), easeTransition, function () {
            theContainerParent.largeContainer.textArea.focus();
        });
        theContainerParent.isUp = true;
    };
    theContainerParent.HideBrick = function () {

        theContainerParent.SetBrickContent(theContainerParent.smallContainer);
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight - activityUpdatesBarHeight, brickZDepth), easeTransition);
        theContainerParent.isUp = false;
        //        }
    };
    theContainerParent.CompletelyHideBrick = function () {
        theContainerParent.modifier.setTransform(Transform.translate(0, originalHeight + shadowSize + 10, brickZDepth), easeTransition);
        theContainerParent.renderController.hide();
    };
    theContainerParent.shadow = new ImageSurface({
        size: [shadowSize, shadowSize],
        content: "./Images/brickShadow.svg",
        properties: {}
    });
    theContainerParent.shadowChain = new ModifierChain();
    theContainerParent.shadowScaleModifier = new StateModifier({
        transform: Transform.scale(originalWidth / (shadowSize * 0.9), 1, 1)
    });
    theContainerParent.shadowPositionModifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0, 0],
        transform: Transform.translate(0, -(shadowSize * 0.5), 0)
    });
    theContainerParent.shadowChain.addModifier(theContainerParent.shadowScaleModifier);
    theContainerParent.shadowChain.addModifier(theContainerParent.shadowPositionModifier);

    theContainerParent.smallContainer = new ContainerSurface({
        size: [undefined, undefined],
        properties: {
            background: 'white',
            cursor: 'pointer',
        }
    });
    theContainerParent.smallContainer.textInput = new InputSurface({
        size: [originalWidth - defaultIconSize - (standardScrollElementHeight * 0.1), standardScrollElementHeight * 0.6],
        placeholder: "Where're you at?",
        type: 'text',
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight * 0.6),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.lightGrey,
            fontSize: fontSizes.medium
        }
    });
    theContainerParent.smallContainer.textInputMod = new StateModifier({
        origin: [0, 0],
        align: [-0.5, 0],
        transform: Transform.translate(defaultIconSize, (standardScrollElementHeight * 0.1), 0)
    });
    theContainerParent.smallContainer.textInput.on('focus', function () {
        theContainerParent.MoveBrickUp(sendStatusBrickHeight);
    });
    CreatePictureButton(pictureButtons.ShowPresets);

    theContainerParent.largeContainer = new ContainerSurface({
        size: [undefined, undefined],
        properties: {
            background: "white",
            cursor: 'pointer',
        }
    });
    theContainerParent.largeContainer.textArea = new TextareaSurface({
        content: "Surface for input",
        placeholder: "What's you status?",
        type: 'text',
        size: [originalWidth - 20, sendStatusBrickHeight * 0.5],
        properties: {
            textAlign: "center",
            fontSize: fontSizes.small

        }
    });
    theContainerParent.largeContainer.textAreaMod = new StateModifier({
        origin: [0.5, 0],
        align: [0, 0],
        transform: Transform.translate(0, (standardScrollElementHeight * 0.1), 0)
    });
    theContainerParent.largeContainer.refreshedText = new Surface({
        size: [true, defaultFontHeight],
        content: "Your Friends Channel Status is refreshed daily",
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.tiny
        }
    });
    theContainerParent.largeContainer.refreshedTextMod = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, (sendStatusBrickHeight * 0.84), 0)
    });
    theContainerParent.largeContainer.textArea.on('blur', function () {
        if (activityContainers.UpdatesBrickThing.isUp === true && activityContainers.Main.isMinimised === false) {
            theContainerParent.HideBrick();
        }
    });
    var lineHeightInPixels = ToPixelString((GetNumberFromString(fontSizes.small)) * defaultFontHeight * 2);
    var paddingInPixels = ToPixelString((GetNumberFromString(fontSizes.small)) * defaultFontHeight * 0.5);
    var paddingInPixelsHalved = ToPixelString(GetNumberFromString(fontSizes.small));
    theContainerParent.largeContainer.remainingLettersText = new Surface({
        size: [true, true],
        content: "64",
        properties: {
            border: '0.1em solid',
            borderRadius: '5px',
            lineHeight: lineHeightInPixels,
            textAlign: 'center',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.small,
            paddingLeft: paddingInPixels,
            paddingRight: paddingInPixels,
            paddingTop: paddingInPixelsHalved,
            paddingBottom: paddingInPixelsHalved,
        }
    });
    theContainerParent.largeContainer.remainingLettersMod = new StateModifier({
        origin: [0.5, 0],
        align: [0, 0],
        transform: Transform.translate(textButtonWidth * 0.5, (sendStatusBrickHeight * 0.5) + (textButtonHeight * 0.1) + (standardScrollElementHeight * 0.1), 0)
    });

    CreateTextButton(textButtons.Send);

    theContainerParent.presetsContainer = new ContainerSurface({
        size: [undefined, undefined],
        properties: {
            background: 'white',
            cursor: 'pointer',
        }
    });

    CreatePictureButton(pictureButtons.CloseCross);
    theContainerParent.presetsContainer.add(pictureButtons.CloseCross.view);

    CreateGenericScrollElement("scrollSpacer", "PremadeStatuses");
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });
    CreateGenericScrollElement("scrollButton", "PremadeStatuses", "Help/FAQ", null, null, function () {

    });

    theContainerParent.smallContainer.add(theContainerParent.smallContainer.textInputMod).add(theContainerParent.smallContainer.textInput);
    theContainerParent.smallContainer.add(pictureButtons.ShowPresets.view);

    theContainerParent.largeContainer.add(theContainerParent.largeContainer.textAreaMod).add(theContainerParent.largeContainer.textArea);
    theContainerParent.largeContainer.add(theContainerParent.largeContainer.refreshedTextMod).add(theContainerParent.largeContainer.refreshedText);
    theContainerParent.largeContainer.add(theContainerParent.largeContainer.remainingLettersMod).add(theContainerParent.largeContainer.remainingLettersText);

    theContainerParent.largeContainer.add(textButtons.Send.view);

    theContainerParent.container.add(theContainerParent.renderController);
    theContainerParent.container.add(theContainerParent.shadowChain).add(theContainerParent.shadow);
}

function CreateDotGroup(theDotGroup, theDraggable, isForTabs) {
    theDraggable.data.associatedDotGroup = theDotGroup;
    var tempPagesAmount = Object.keys(theDraggable.views).length;
    theDotGroup.dotsAmount = tempPagesAmount;
    var dotSize;
    var containerSize;
    var alignmentYOffset;
    var tranlsateYOffset;
    if (isForTabs === false) {
        dotSize = (15 / 480) * originalWidth;
        containerSize = originalWidth * (0.05 + (tempPagesAmount * 0.05));
        alignmentYOffset = 0.02;
        tranlsateYOffset = 0;
    } else {
        dotSize = 15;
        containerSize = originalWidth - (40 * 2);
        alignmentYOffset = 0.7;
        tranlsateYOffset = 0;
    }

    if (theDotGroup === dotGroups.IntroPanelsPanels) {
        tranlsateYOffset = originalHeight - 120;
    }

    theDotGroup.container = new ContainerSurface({
        size: [containerSize, dotSize],
        properties: {
            textAlign: 'center',
        }
    });

    theDotGroup.modifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, alignmentYOffset],
        transform: Transform.translate(0, tranlsateYOffset, 2000)
    });

    // loop through the amount of pages
    for (var i = 0; i < tempPagesAmount; i++) {

        theDotGroup.dots[i] = new ContainerSurface({
            size: [dotSize, dotSize],
            properties: {
                textAlign: 'center',
                cursor: 'all-scroll',
            }
        });

        var tempAlignmentDistance = (1 / tempPagesAmount);
        var tempAlignmentInterval = tempAlignmentDistance * 0.5;

        var tempXAlignment = tempAlignmentInterval + (i * tempAlignmentDistance);

        theDotGroup.dots[i].modifier = new StateModifier({
            origin: [0.5, 0.5],
            // this not doing anything atm
            align: [tempXAlignment, 0.5],
            transform: Transform.translate(0, 0, 20)
        });

        theDotGroup.dots[i].renderController = new RenderController();

        var centeredStateModifierProperties = {
            origin: [0.5, 0.5],
            // not exactly sure why this is like this (the mage Surfaces are slightly offset to the right of the dot containers by default? when their x alignment is set to 0.5
            align: [-0.01, 0.5],
            transform: Transform.translate(0, 0, 20)
        };

        theDotGroup.dots[i].inactiveSurface = new ImageSurface({
            size: [dotSize, dotSize],
            content: './Images/circleDotInactive.svg',
            properties: {}
        });
        theDotGroup.dots[i].inactiveSurface.modifier = new StateModifier(centeredStateModifierProperties);
        theDotGroup.dots[i].inactiveSurface.view = new View();

        theDotGroup.dots[i].activeSurface = new ImageSurface({
            size: [dotSize, dotSize],
            content: './Images/circleDotActive.svg',
            properties: {}
        });
        theDotGroup.dots[i].activeSurface.modifier = new StateModifier(centeredStateModifierProperties);
        theDotGroup.dots[i].activeSurface.view = new View();

        theDotGroup.dots[i].disabledSurface = new ImageSurface({
            size: [dotSize, dotSize],
            content: './Images/circleDotDisabled.svg',
            properties: {}
        });
        theDotGroup.dots[i].disabledSurface.modifier = new StateModifier(centeredStateModifierProperties);
        theDotGroup.dots[i].disabledSurface.view = new View();

        theDotGroup.dots[i].isDisabled = false;
        theDotGroup.dots[i].isActive = false;

        theDotGroup.dots[i].inactiveSurface.view.add(theDotGroup.dots[i].inactiveSurface.modifier).add(theDotGroup.dots[i].inactiveSurface);
        theDotGroup.dots[i].activeSurface.view.add(theDotGroup.dots[i].activeSurface.modifier).add(theDotGroup.dots[i].activeSurface);
        theDotGroup.dots[i].disabledSurface.view.add(theDotGroup.dots[i].disabledSurface.modifier).add(theDotGroup.dots[i].disabledSurface);

        theDotGroup.dots[i].renderController.show(theDotGroup.dots[i].inactiveSurface.view);
        theDotGroup.dots[i].add(theDotGroup.dots[i].renderController);
        theDotGroup.container.add(theDotGroup.dots[i].modifier).add(theDotGroup.dots[i]);
    }

}

function CreateTabsBar(theTabsBar, theDraggable) {
    var searchButtonWidth = 40;

    var tempPagesAmount = Object.keys(theDraggable.views).length;
    theTabsBar.data.tabsAmount = tempPagesAmount;
    theTabsBar.data.currentlyActiveTab = theDraggable.data.currentPage;
    theTabsBar.data.associatedDraggable = theDraggable;
    // this could be altered later so the tabs bar can be shorter than the screen width
    var tempTabsContainerWidth = originalWidth - (searchButtonWidth * 2);
    var tempTabWidth = tempTabsContainerWidth / tempPagesAmount;

    var tempYOffset = 0 + (tabsBarHeight * 0.2 /*0.2*/ );
    if (theTabsBar == tabsBars.Contacts) {
        tempYOffset = (defaultFontHeight * 4) + (tabsBarHeight * 0.2);
    }

    theTabsBar.container = new ContainerSurface({
        size: [originalWidth, tabsBarHeight],
        properties: {
            textAlign: 'center',
            cursor: 'all-scroll',
        }
    });
    //(this gets applied outside of this creation function but created in it)
    theTabsBar.modifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, tempYOffset, 1)
    });

    theTabsBar.container.mainContainer = new ContainerSurface({
        size: [originalWidth, tabsBarHeight],
        properties: {
            textAlign: 'center',
            cursor: 'all-scroll',
        }
    });
    theTabsBar.container.mainContainer.modifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 0)
    });

    // from http://stackoverflow.com/questions/24082190/rendercontroller-with-sliding-transitions-transform-translate by subblue

    theTabsBar.container.mainContainer.renderController = new RenderController({
        inTransition: {
            curve: Easing.inOutQuart,
            duration: 600
        },
        outTransition: {
            curve: Easing.inOutQuart,
            duration: 600
        },
        overlap: true
    });

    var tempRenderController = theTabsBar.container.mainContainer.renderController;

    tempRenderController.inTransformFrom(function (progress) {
        return Transform.translate(window.innerWidth * (1.0 - progress), 0, 0);
    });

    tempRenderController.outTransformFrom(function (progress) {
        return Transform.translate(window.innerWidth * progress - window.innerWidth, 0, 0);
    });

    // No opacity change
    tempRenderController.inOpacityFrom(function () {
        return 1;
    });
    tempRenderController.outOpacityFrom(function () {
        return 1;
    });
    /////////////////////////////

    theTabsBar.container.mainContainer.tabsContainer = new ContainerSurface({
        size: [tempTabsContainerWidth, tabsBarHeight],
        properties: {
            textAlign: 'center',
            cursor: 'all-scroll',
        }
    });

    theTabsBar.container.mainContainer.tabsContainer.modifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 0)
    });

    theTabsBar.container.mainContainer.tabsContainer.tabsIndicator = new Surface({
        content: "Surface" + (i + 1),
        size: [tempTabWidth, 5],
        properties: {
            //				backgroundColor: "hsl(" + (i * 360 / 20) + ", 100%, 50%)",
            backgroundColor: "hsl(" + (i * 360 / 20) + ", 300%, 80%)",
            //				backgroundColor: themeColors.lightGlass,
            lineHeight: ToPixelString(undefined),
            textAlign: "center",
        }
    });
    // (the logic for changing the modifier is handled in the draggable onUpdate function)
    theTabsBar.container.mainContainer.tabsContainer.tabsIndicator.modifier = new Modifier({
        opacity: 1
    });

    theTabsBar.container.mainContainer.searchBarContainer = new ContainerSurface({
        size: [tempTabsContainerWidth, tabsBarHeight],
        properties: {
            textAlign: 'center',
            cursor: 'all-scroll',
        }
    });
    theTabsBar.container.mainContainer.searchBarContainer.renderController = new RenderController();
    theTabsBar.container.mainContainer.searchBarContainer.inputSurface = new InputSurface({
        size: [tempTabsContainerWidth - (tempTabsContainerWidth * 0.02), tabsBarHeight],
        placeholder: "dangfyuiu",
        type: 'text'
    });

    theTabsBar.container.mainContainer.searchBarContainer.inputSurface.mod = new StateModifier({
        origin: [0, 0.5],
        align: [0.02, 0.5],
        transform: Transform.translate(-(tempTabsContainerWidth * 0.5), 0, 0)
    });
    theTabsBar.container.mainContainer.searchBarContainer.inputSurface.on('focus', function () {
        theTabsBar.data.searchIsFocused = true;
    });
    theTabsBar.container.mainContainer.searchBarContainer.inputSurface.on('blur', function () {
        theTabsBar.data.searchIsFocused = false;
    });
    // on blur it could remove all the search result? (or it could do that when then search bar is hidden? (it will blur automatically anyway)

    theTabsBar.container.searchButtonContainer = new ContainerSurface({
        size: [true, true],
        properties: {
            textAlign: 'center',
            cursor: 'all-scroll',
        }
    });
    theTabsBar.container.searchButtonContainer.modifier = new StateModifier({
        origin: [1, 0.5],
        align: [1, 0.5],
        transform: Transform.translate(-(defaultIconSize * 0.5) - (defaultIconSize * 0.2), 0, 0)
    });
    theTabsBar.container.searchButtonContainer.renderController = new RenderController();

    // these could potentially be picture buttons instead? (although it means the render controller won't be built in straight away

    theTabsBar.container.searchButtonContainer.searchIcon = new ImageSurface({
        size: [defaultIconSize, defaultIconSize],
        content: "./Images/searchIconNeedCredit.svg",
        properties: {}
    });
    theTabsBar.container.searchButtonContainer.cancelIcon = new ImageSurface({
        size: [defaultIconSize, defaultIconSize],
        content: "./Images/cross97.svg",
        properties: {}
    });

    theTabsBar.container.searchButtonContainer.searchIcon.modifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 0)
    });

    theTabsBar.container.searchButtonContainer.cancelIcon.modifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 0)
    });

    theTabsBar.container.searchButtonContainer.searchIcon.view = new View();
    theTabsBar.container.searchButtonContainer.cancelIcon.view = new View();

    theTabsBar.container.searchButtonContainer.searchIcon.on('click', function () {
        if (theTabsBar.data.searchBarIsShowing === false) {
            tabsBarLogic.ShowSearchBar(theTabsBar);
        } else {
            tabsBarLogic.HideSearchBar(theTabsBar);
        }
    });

    theTabsBar.container.searchButtonContainer.cancelIcon.view = new View();

    theTabsBar.container.mainContainer.tabsContainer.tabs = [];
    // loops through all the tabs
    var CreateGoToPageFunction = function (theDraggable, i) {
        return function () {
            if (i < theDraggable.data.lockedPageLimit) {
                theDraggable.data.currentPage = i;
                goToThisPage(theDraggable, theDraggable.data.currentPage, true);
            }
        };
    };
    for (var i = 0; i < tempPagesAmount; i++) {

        // it could be better to have a surface container for each of the tabs so the tappable area can be more easily set without interferring with the position of the text? mabye not
        var tempTabText = "Tab " + i;
        //(with the right width and height)
        if (theTabsBar === tabsBars.ActivityUpdates) {
            if (i === 0) {
                tempTabText = "Friends";
            } else if (i === 1) {
                tempTabText = "Everyone";
            } else if (i === 2) {
                tempTabText = "Custom";
            }
        } else if (theTabsBar === tabsBars.Contacts) {
            if (i === 0) {
                tempTabText = "People";
            } else if (i === 1) {
                tempTabText = "Groups";
            }
        } else if (theTabsBar === tabsBars.PresetStatusPages) {
            if (i === 0) {
                tempTabText = "Where";
            } else if (i === 1) {
                tempTabText = "Other";
            }
        }

        theTabsBar.container.mainContainer.tabsContainer.tabs[i] = new Surface({
            content: tempTabText,
            size: [tempTabWidth, tabsBarHeight],
            properties: {
                lineHeight: ToPixelString(tabsBarHeight),
                textAlign: 'center',
                cursor: 'crosshair',
                color: themeColors.main
            }
        });
        theTabsBar.container.mainContainer.tabsContainer.tabs[i].idNumber = i;
        theTabsBar.container.mainContainer.tabsContainer.tabs[i].on('click', CreateGoToPageFunction(theDraggable, i));

        theTabsBar.container.mainContainer.tabsContainer.tabs[i].isDisabled = false;
        theTabsBar.container.mainContainer.tabsContainer.tabs[i].isActive = false;

        // gotta be the center of a third if there's 3 tabs, or center of a half if theres 2, or the center if there's 1
        var tempAlignmentDistance = (1 / tempPagesAmount);
        var tempAlignmentInterval = tempAlignmentDistance * 0.5;

        var tempXAlignment = tempAlignmentInterval + (i * tempAlignmentDistance);
        theTabsBar.container.mainContainer.tabsContainer.tabs[i].modifier = new StateModifier({
            origin: [0.5, 0.5],
            align: [tempXAlignment, 0.3],
            transform: Transform.translate(0, 0, 7000)
        });

        theTabsBar.container.mainContainer.tabsContainer.add(theTabsBar.container.mainContainer.tabsContainer.tabs[i].modifier).add(theTabsBar.container.mainContainer.tabsContainer.tabs[i]);
    }

    theDraggable.data.associatedTabs = theTabsBar.container.mainContainer.tabsContainer.tabs;
    var tempTabsContainer = theTabsBar.container.mainContainer.tabsContainer;
    var tempSearchBarContainer = theTabsBar.container.mainContainer.searchBarContainer;
    var tempSearchButtonContainer = theTabsBar.container.searchButtonContainer;
    // Adding the stuff
    tempTabsContainer.view = new View();
    tempTabsContainer.view.add(tempTabsContainer.modifier).add(tempTabsContainer);

    tempSearchBarContainer.view = new View();
    tempSearchBarContainer.view.add(tempSearchBarContainer.modifier).add(tempSearchBarContainer);
    tempSearchBarContainer.add(theTabsBar.container.mainContainer.searchBarContainer.inputSurface.mod).add(theTabsBar.container.mainContainer.searchBarContainer.inputSurface);

    theTabsBar.container.mainContainer.add(theTabsBar.container.mainContainer.renderController);
    theTabsBar.container.mainContainer.add(tempTabsContainer.renderController);
    theTabsBar.container.add(theTabsBar.container.mainContainer);

    tempSearchButtonContainer.add(tempSearchButtonContainer.modifier).add(tempSearchButtonContainer.container);
    tempSearchButtonContainer.add(tempSearchButtonContainer.renderController);

    if (theTabsBar !== tabsBars.PresetStatusPages) {
        theTabsBar.container.add(tempSearchButtonContainer.modifier).add(tempSearchButtonContainer);
    }

    tempSearchButtonContainer.searchIcon.view.add(tempSearchButtonContainer.searchIcon.modifier).add(tempSearchButtonContainer.searchIcon);
    tempSearchButtonContainer.cancelIcon.view.add(tempSearchButtonContainer.cancelIcon.modifier).add(tempSearchButtonContainer.cancelIcon);

    tempSearchButtonContainer.renderController.show(tempSearchButtonContainer.searchIcon.view);
    theTabsBar.container.mainContainer.renderController.show(tempTabsContainer.view);
}

function CreateDeclinedView() {
    // set the view of the declined view to be new ? Also have this area appear on a brick? (easy enough to do and switch from)
    theViews.BrickContents.Declined = new View();

    theViews.BrickContents.Declined.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: 'rgba(106, 253, 71, 0.21)'
        }
    });

    // this creates a text surface for the contacts title (it can be changed with a function)
    theViews.BrickContents.Declined.title = new Surface({
        size: [true, true],
        content: "Declined",
        properties: {
            textAlign: 'center',
            fontSize: fontSizes.medium,
            color: themeColors.dark,
        }
    });

    // this creates a modifier for that text surface
    theViews.BrickContents.Declined.title.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.06],
        transform: Transform.translate(0, 0, 2)
    });

    CreateTextButton(textButtons.CloseDeclined);

    theViews.BrickContents.Declined.container.add(textButtons.CloseDeclined.view);

    theViews.BrickContents.Declined.container.add(theViews.BrickContents.Declined.title.mod).add(theViews.BrickContents.Declined.title);

    theViews.BrickContents.Declined.container.add(scrollers.Declined.node);

    theViews.BrickContents.Declined.add(theViews.BrickContents.Declined.container);
}



//endregion
//====================================================-


//====================================================-
//Signup Pages region
//|31|5991|-1756|756.68|433.49|1----------------------------------------------------------------------------------------------~
function CreateIntroPanel(picturePath) {
    //     console.log("here's the picture path");
    //     console.log('url(' + picturePath + ')');
    var extraWelcomeButton = "";
    if (picturePath === "./Images/IntroPage3.svg") {
        extraWelcomeButton = "<div style = 'top:" + ((originalHeight) - 140) + "px; position:relative ; font-size : 0.7em; color: " + themeColors.main + "; border-radius : 6px; border : 0.05em solid " + themeColors.main + "; ' onclick = ' (function(){mainRenderController.show(theViews.SignUpPages.VerifyPhone.view);})()'>Welcome</div>";
    }


    // make this a div with the image and text in it
    var tempPanelImage = new Surface({
        size: [originalWidth * 0.8, originalHeight],
        content: "<span style = 'top: 100px; position:relative; color: " + themeColors.main + "'>mobilizr</span>" + extraWelcomeButton,
        properties: {
            fontSize: fontSizes.mega,
            color: themeColors.dark,
            backgroundImage: 'url(' + picturePath + ')',
            backgroundSize: ToPixelString(originalWidth * 0.8) + ' ' + ToPixelString(200),
            backgroundRepeat: 'no-repeat',
            backgroundPosition: '50% 50%'
        }
    });

    // Create the modifier
    var tempPanelImageMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5] //,
            //transform: Transform.translate(-100, 0, 2)
    });

    // Create the node (just the image surface added to the modifier)
    var tempPanelImageNode = new RenderNode();
    tempPanelImageNode.add(tempPanelImageMod).add(tempPanelImage);
    return tempPanelImageNode;
}

function CreateSignupPages() {
    // Create the into panel images (and modifiers
    //    draggables.IntroPanelsPanels.container.add(dotGroups.IntroPanelsPanels.modifier).add(dotGroups.IntroPanelsPanels.container);

    //    theViews.IntroPanelsPanels.IntroPanel1.add(CreateIntroPanel("./Images/id_1113");

    var tempPanelImage = new ImageSurface({
        size: [100, 100],
        content: "./id_1113.png",
        properties: {}
    });
    var tempPanelImageMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 2)
    });

    theViews.IntroPanelsPanels.IntroPanel1.view.add(CreateIntroPanel("./Images/IntroPage1.svg"));
    theViews.IntroPanelsPanels.IntroPanel2.view.add(CreateIntroPanel("./Images/IntroPage2.svg"));
    theViews.IntroPanelsPanels.IntroPanel3.view.add(CreateIntroPanel("./Images/IntroPage3.svg"));
    theViews.SignUpPages.IntroPanels.view.add(dotGroups.IntroPanelsPanels.modifier).add(dotGroups.IntroPanelsPanels.container);
    dotLogic.CheckAndUpdateDots(draggables.IntroPanelsPanels);

    theViews.SignUpPages.IntroPanels.view.add(draggables.IntroPanelsPanels.containerModifier).add(draggables.IntroPanelsPanels.container);

    var mobilizrTitle = new Surface({
        size: [true, true],
        content: "mobilizr",
        properties: {
            fontSize: fontSizes.mega,
            color: themeColors.dark
        }
    });
    var mobilizrTitleMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.15]
    });

    //     theViews.SignUpPages.IntroPanels.view.add(mobilizrTitleMod).add(mobilizrTitle);

    //     CreateTextButton(textButtons.Welcome);
    //     theViews.SignUpPages.IntroPanels.view.add(textButtons.Welcome.view);

    CreateTextButton(textButtons.VerifyNext);
    theViews.SignUpPages.VerifyPhone.view.add(textButtons.VerifyNext.view);

    CreateTextButton(textButtons.BackToVerify);
    theViews.SignUpPages.EnterCode.view.add(textButtons.BackToVerify.view);

    CreateTextButton(textButtons.EnterCodeDone);
    theViews.SignUpPages.EnterCode.view.add(textButtons.EnterCodeDone.view);

    CreateTextButton(textButtons.WelcomePageEnter);
    theViews.SignUpPages.WelcomeEnter.view.add(textButtons.WelcomePageEnter.view);

    CreateGenericScrollElement("scrollDescription", "VerifyPhone", "Please verify your phone so we know you are a real person!");


    CreateLongNumberInputScrollElement(scrollers.VerifyPhone, "+61", "Your phone number");

    // TODO GETTING IF THE PHONE NUMBER BOX WAS FOCUS
    scrollers.VerifyPhone.scrollElements[1].numberInput.on("focus", function () {
        phoneNumberInputIsFocused = true;
        //     console.log("Focused!");
    });
    scrollers.VerifyPhone.scrollElements[1].numberInput.on("blur", function () {
        phoneNumberInputIsFocused = false;
        //     console.log("Blurred!");
    });

    CreateGenericScrollElement("accordionDescription", "EnterCode", "We sent you an SMS with a code to your number");

    CreateLongNumberInputScrollElement(scrollers.EnterCode, "SMS", "Enter Code");

    CreateValidNumberScrollElement();

    // this is a scroller, and the title up the top isn't part of the scroller
    //    theViews.SignUpPages.VerifyPhone.view.

    // The enter code view is the same scroller but with different text

    // the enter screen isn't a scroller, but there is still a line near the top (in the same place as on the previous view and a title ,

    //   theViews.SignUpPages.IntroPanels.view.container.add(tempPanelImageMod).add(tempPanelImage);

    //SignUpPages
    //        IntroPanels
    //        VerifyPhone
    //        YourCountry
    //        EnterCode

    /////////        WelcomeEnter
    // create surface with html text "Enjoy Mobilizr for 1 year free , and then it's only $1 a year
    var welcomeInfoSurfaceContent = "Enjoy Mobilizr<br>for 6 Months Free<div style = 'font-size: 0.5em;'>and then it's only $1 a year</div>";
    var welcomeInfoSurface = new Surface({
        size: [true, true],
        content: welcomeInfoSurfaceContent,
        properties: {
            fontSize: fontSizes.large,
            color: "green",
            textAlign: "center",
            border: "0.05em solid " + themeColors.light,
            paddingTop: ToPixelString(25),
            paddingBottom: ToPixelString(25)
        }
    });
    welcomeInfoSurface.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.35],
        transform: Transform.translate(0, 0, 2)
    });
    // create a "why we don't sell ads text surface
    var whyAdsSurfaceContent = "<u>Why we don't sell ads</u>";
    var whyAdsSurface = new Surface({
        size: [true, true],
        content: whyAdsSurfaceContent,
        properties: {
            fontSize: fontSizes.small,
            color: themeColors.dark,
            textAlign: "center"
        }
    });
    whyAdsSurface.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.65],
        transform: Transform.translate(0, 0, 2)
    });
    // Create text surface (out of html
    var welcomeDisclaimerSurfaceContent = "By using Mobilizr you agree to our<br><a href='http://www.google.com'>Terms of Service</a> and <a href='http://www.google.com'>Privacy Policy</a>";
    var welcomeDisclaimerSurface = new Surface({
        size: [true, true],
        content: welcomeDisclaimerSurfaceContent,
        properties: {
            fontSize: fontSizes.tiny,
            color: themeColors.dark,
            textAlign: "center"
        }
    });
    welcomeDisclaimerSurface.mod = new StateModifier({
        origin: [0.5, 1.0],
        align: [0.5, 0.98],
        transform: Transform.translate(0, 0, 2)
    });

    theViews.SignUpPages.WelcomeEnter.view.add(welcomeInfoSurface.mod).add(welcomeInfoSurface);
    theViews.SignUpPages.WelcomeEnter.view.add(whyAdsSurface.mod).add(whyAdsSurface);
    theViews.SignUpPages.WelcomeEnter.view.add(welcomeDisclaimerSurface.mod).add(welcomeDisclaimerSurface);


}

function CreateLongNumberInputScrollElement(theScroller, theText, placeHolderText) {
    var tempScrollElementView = new View();
    var tempLineColour = themeColors.light;

    tempScrollElementView.container = new ContainerSurface({
        size: [undefined, standardScrollElementHeight],
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            textAlign: 'center',
            cursor: 'pointer',
            borderBottom: "0.1em solid " + themeColors.light
        }
    });

    tempScrollElementView.textSurface = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: theText,
        placeholder: placeHolderText,
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight + 3),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.lightGrey,
            fontSize: fontSizes.medium
        }
    });

    var tempAttributes = {};

    var tempType = 'number';
    if (theScroller === scrollers.VerifyPhone) {
        tempType = 'tel';
    } else if (theScroller === scrollers.EnterCode) {
        tempType = 'text';
    }

    tempScrollElementView.numberInput = new InputSurface({
        size: [defaultFontHeight * 15, standardScrollElementHeight - (defaultPaddingVertical * 2)],
        type: tempType,
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight - (defaultPaddingVertical * 2)),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.lightGrey,
            fontSize: fontSizes.medium
        },
        attributes: tempAttributes
    });

    tempScrollElementView.textMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.55, 0.5]
    });

    tempScrollElementView.numberMod = new StateModifier({
        origin: [1, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(-defaultPaddingHorizontal, 0, 0)
    });

    if (theScroller === scrollers.Privacy) {
        tempScrollElementView.numberInput.on('blur', function () {
            temporaryActivityObject.deletionDays = tempScrollElementView.numberInput.getValue();
        });
    }

    if (theScroller === scrollers.Reach) {
        tempScrollElementView.numberInput.on('blur', function () {
            temporaryActivityObject.target = tempScrollElementView.numberInput.getValue();
        });
    }

    tempScrollElementView.container.add(tempScrollElementView.numberMod).add(tempScrollElementView.numberInput);
    tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);

    var tempStartOpacity = 1;

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    tempScrollElementView.container.pipe(theScroller.scrollView);
    theScroller.scrollElements.push(tempScrollElementView);
}

function CreateCountdownAndResendScrollElement() {

    // a number surface to the right and a text button or just a text surface that does something when it's clicked on the left

}

function CreateValidNumberScrollElement() {
    var theText = "please enter your number";

    var theScroller = scrollers.VerifyPhone;
    // this is jsut a text that says "valid" (in green) if the phone number is valid, or "invalid" (in red), if the phone number isn't a valid australian number
    var tempScrollElementView = new View();

    // FUTURE this could be an external function that can be reused around the app for calculating the needed height based on a width and a string (and the padding?
    //        var calculatedContainerHeight = (Math.ceil((theText.length / 62)) * defaultFontHeight) + (defaultFontHeight *2);
    var amountOfLettersOnALineTiny = calculateLetterAmountOnLine(theScroller.data.width - 2 * defaultFontHeight, GetNumberFromString(fontSizes.tiny));
    var calculatedContainerHeight = amountToIncreaseStatusHeightBy(theText.length, amountOfLettersOnALineTiny, defaultFontHeight * 1.15);
    tempScrollElementView.container = new ContainerSurface({
        size: [undefined, calculatedContainerHeight + (defaultFontHeight * 2)],
        properties: {
            backgroundColor: 'rgba(175, 175, 175, 0)',
            textAlign: 'center',
            cursor: 'pointer',
            borderBottom: "0.1em solid " + themeColors.light
        }
    });

    tempScrollElementView.textSurface = new Surface({
        size: [undefined, true],
        content: theText,
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'center',
            cursor: 'pointer',
            color: themeColors.grey,
            fontSize: fontSizes.smaller,
            padding: ToPixelString(defaultFontHeight)
        }
    });

    tempScrollElementView.textMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5]
    });

    tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
    var tempStartOpacity = 1;

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    theScroller.scrollElements.push(tempScrollElementView);
    //    theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
}


//endregion
//====================================================-


//====================================================-
//Contacts Pages region
//|32|6086|-1250|592.51|402.12|1----------------------------------------------------------------------------------------------~
// this creates the contacts view for the app
function CreateContactsView() {
    // create a blank new view for the contacts brick view? already done with the looping thing?
    theViews.BrickContents.Contacts = new View();

    // create a new container for the contacts brick container
    theViews.BrickContents.Contacts.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {}
    });

    // this creates a text surface for the contacts title (it can be changed with a function)

    theViews.BrickContents.Contacts.title = new Surface({
        size: [true, true],
        content: "Contacts",
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'center',
            fontSize: fontSizes.medium,
            color: themeColors.dark,
        }
    });

    // this creates a modifier for that text surface
    theViews.BrickContents.Contacts.title.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.06],
        transform: Transform.translate(0, 0, 2)
    });

    CreateTextButton(textButtons.CloseContacts);

    theViews.BrickContents.Contacts.container.add(textButtons.CloseContacts.view);

    theViews.BrickContents.Contacts.container.add(theViews.BrickContents.Contacts.title.mod).add(theViews.BrickContents.Contacts.title);

    theViews.ContactsPages.People.view.add(scrollers.ContactsPeople.modifier).add(scrollers.ContactsPeople.scrollView);
    theViews.ContactsPages.Groups.view.add(scrollers.ContactsGroups.modifier).add(scrollers.ContactsGroups.scrollView);

    theViews.BrickContents.Contacts.add(theViews.BrickContents.Contacts.container);

    // create the Done button Mini Brick
    theViews.BrickContents.Contacts.ButtonBrick = {};
    CreateDoneButtonMiniBrick(theViews.BrickContents.Contacts.ButtonBrick);

    // create the textInput Mini Brick
    theViews.BrickContents.Contacts.TextBrick = {};
    CreateTextInputMiniBrick(theViews.BrickContents.Contacts.TextBrick);

    //     theViews.BrickContents.Contacts.add(theViews.BrickContents.Contacts.ButtonBrick.modifier).add(theViews.BrickContents.Contacts.ButtonBrick.container);

    //  theViews.BrickContents.Contacts.add(theViews.BrickContents.Contacts.TextBrick.modifier).add(theViews.BrickContents.Contacts.TextBrick.container);

    // all the tabs and dots and pages and scrollers are made in the main.js file and added there too instead of here

}

function CreateDoneButtonMiniBrick(thingToSet) {
    var brickZDepth = 10100; //900;

    thingToSet.renderController = new RenderController();

    thingToSet.isUp = false;

    // container surface
    thingToSet.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: 'white',
            textAlign: 'center',
            cursor: 'pointer',
            borderTop: "0.1em solid " + themeColors.light
        }
    });

    thingToSet.container.on('click', function () {
        alert("The okay button was clicked");
    });

    thingToSet.textSurface = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: "Ok",
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            textAlign: 'center',
            cursor: 'pointer',
            color: themeColors.main,
            fontSize: fontSizes.small
        }
    });

    thingToSet.textMod = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0],
        transform: Transform.translate(0, 0, 0)
    });

    thingToSet.container.add(thingToSet.textMod).add(thingToSet.textSurface);

    // Functions 
    thingToSet.SetBrickContent = function (brickContentView) {
        thingToSet.renderController.show(brickContentView);
    };

    thingToSet.MoveBrickUp = function () {
        thingToSet.isUp = true;
        var heightDistance = standardScrollElementHeight;
        thingToSet.modifier.setTransform(Transform.translate(0, originalHeight - heightDistance, brickZDepth), easeTransition);
        screenDisabler.renderController.show(screenDisabler.view);
    };

    // NOTE Currently HideBrick and CompletelyHideBrick do (pretty much) the same thing, but CompletelyHideBrick can be used later for hiding the rendercontroller on the mini bricks
    thingToSet.HideBrick = function () {
        thingToSet.isUp = false;
        thingToSet.modifier.setTransform(Transform.translate(0, originalHeight, brickZDepth), easeTransition);
        //         screenDisabler.renderController.hide();
        //         thingToSet.renderController.hide();
    };

    thingToSet.CompletelyHideBrick = function () {
        thingToSet.modifier.setTransform(Transform.translate(0, originalHeight + shadowSize + 10, brickZDepth), easeTransition);
        // NOTE FIXME ideally everything inside these mini bricks is hidden so it doesn't slow down the rest of the app (like with the post status brick)
        //         thingToSet.renderController.hide();
    };

    thingToSet.modifier = new StateModifier({
        origin: [0.5, 1],
        align: [0.5, 1],
        transform: Transform.translate(0, originalHeight, 1000)
    });

    thingToSet.container.add(thingToSet.renderController);


}

function CreateTextInputMiniBrick(thingToSet) {
    var brickZDepth = 10098; //800

    thingToSet.renderController = new RenderController();

    thingToSet.isUp = false;

    // container surface
    thingToSet.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: 'white',
            textAlign: 'center',
            cursor: 'pointer',
            borderTop: "0.1em solid " + themeColors.light
        }
    });

    // Create the text input part and the text next to it here
    thingToSet.textSurface = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: "Add To",
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.small
        }
    });

    thingToSet.textInput = new InputSurface({
        size: [originalWidth - 65, standardScrollElementHeight - (defaultPaddingVertical * 2)],
        placeholder: "new group",
        type: 'text',
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight - (defaultPaddingVertical * 2)),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.grey,
            fontSize: fontSizes.small
        }
    });

    // on focus (set the contactsLogic.textIsFocused to true)
    thingToSet.textInput.on('focus', function () {
        contactsLogic.textIsFocused = true;
    });
    //on blur (set the contactsLogic.textIsFocused to false)
    thingToSet.textInput.on('blur', function () {
        contactsLogic.textIsFocused = false;
    });

    thingToSet.textInputMod = new StateModifier({
        origin: [0.5, 0],
        align: [0, 0],
        transform: Transform.translate((defaultPaddingHorizontal * 4) + (25), defaultPaddingVertical * 2, 0)
    });
    thingToSet.textMod = new StateModifier({
        origin: [0, 0],
        align: [0, 0],
        transform: Transform.translate(defaultPaddingHorizontal * 2, defaultPaddingVertical, 0)
    });
    thingToSet.container.add(thingToSet.textInputMod).add(thingToSet.textInput);
    thingToSet.container.add(thingToSet.textMod).add(thingToSet.textSurface);

    // The onclick for the text input checks if the contacts is in "Edit group mode" and if it is it sets the value of the text input to the placeholder text of the text input

    // Functions

    thingToSet.MoveBrickUp = function () {
        var heightDistance = standardScrollElementHeight + (2 * defaultPaddingVertical) + standardScrollElementHeight;
        thingToSet.isUp = true;
        thingToSet.modifier.setTransform(Transform.translate(0, originalHeight - heightDistance, brickZDepth), easeTransition);
        screenDisabler.renderController.show(screenDisabler.view);
    };

    thingToSet.HideBrick = function () {
        thingToSet.isUp = false;
        thingToSet.modifier.setTransform(Transform.translate(0, originalHeight, brickZDepth), easeTransition);
        //         screenDisabler.renderController.hide();
    };

    thingToSet.CompletelyHideBrick = function () {
        thingToSet.modifier.setTransform(Transform.translate(0, originalHeight + shadowSize + 10, brickZDepth), easeTransition);
        thingToSet.renderController.hide();
    };

    thingToSet.modifier = new StateModifier({
        origin: [0.5, 1],
        align: [0.5, 1],
        transform: Transform.translate(0, originalHeight, 1000)
    });

    thingToSet.container.add(thingToSet.renderController);

}

function editThisGroup(groupId) {
    // get the group object from the in app databse based on its id

    // get an array of the contact id's from that group

    // select all the contacts from that group , it loops through all the contacts related to that group and does TickAcontactCheckBox    

    // move the button brick up and the text brick (for the contacts views) 
    // set the title for the contacts views
    // set the placeholder text for the text brixk

    theViews.BrickContents.Contacts.ButtonBrick.MoveBrickUp(80);
    theViews.BrickContents.Contacts.title.setContent("New Title");
    theViews.BrickContents.Contacts.TextBrick.MoveBrickUp(160);
}


//endregion
//====================================================-


//====================================================-
//Activity Pages region
//|33|6000|-677|955.65|689.26|1----------------------------------------------------------------------------------------------~
function updateActivityInfoPage(theActivityId, shouldOpenActivity) {
    // shouldOpenActivity defaults to true
    if (shouldOpenActivity !== false) {
        shouldOpenActivity = true;
    }

    // this updaetes all the info in the activity page
    var theActivityObject = inAppDatabases.activities({
        id: theActivityId
    }).first();

    // The picture
    var tempPicturePath = theActivityObject.picture;
    theViews.ActivityPages.Picture.image.setContent(tempPicturePath);
    activityContainers.Info.picturePart.image.setContent(tempPicturePath);

    var privacyTypeString = getPrivacyStringFromNumber(theActivityObject.privacy);
    activityContainers.Info.titleBar.setContent(privacyTypeString + " - " + theActivityObject.category);

    var timeEndingText = "";
    if (theActivityObject.timeEnding && theActivityObject.timeEnding !== theActivityObject.timeStarting) {
        timeEndingText = " - " + theActivityObject.timeEnding;
    }

    var theCreatorObject = inAppDatabases.mobilizrUsers({
        id: theActivityObject.createdBy
    }).first();

    var tempPercentageFactor = 0 + ((originalWidth / 320)) - 1;

    var tempImageIconSize = 10 + (10 * tempPercentageFactor);

    var tempInfoBarElement = "<div class = 'activityInfoBarTitle' style='margin-top:" + (tempPercentageFactor * 1.4) + "% ;'>" + "Black Rock Beach Party This Weekend " + "</div>" + "<div class = 'infoBarOrganiser'style='margin-top:" + (tempPercentageFactor * 1.4) + "% ;'>By " + theCreatorObject.name + "</div>" + "<div class = 'infoBarWhenWhere'style='margin-top:" + (tempPercentageFactor * 1.4) + "% ; line-height:" + (100) + "% ;'><div><img class = 'alignSmallImage' src='./Images/pin56.svg' width='" + tempImageIconSize + "' height='" + tempImageIconSize + "'>" + theActivityObject.location + "</div><div><img class = 'alignSmallImage' src='./Images/clock96.svg' width='" + tempImageIconSize + "' height='" + tempImageIconSize + "'>" + theActivityObject.timeStarting + timeEndingText + "</div></div>";

    activityContainers.Info.infoBar.setContent(tempInfoBarElement);

    // The description
    var tempAttendingAmount = theActivityObject.peopleJoined;
    var tempTargetAmount = theActivityObject.target;
    var tempAreInElement = "<span class='mainText'>" + tempAttendingAmount + "</span> are IN";

    activityContainers.Info.descriptionPart.container.setContent(theActivityObject.description);

    console.log("theActivityObject");
    console.log(theActivityObject);

    // target bar or how many are in part?
    if (tempTargetAmount !== null) {
        activityContainers.Info.sharingPart.leftContainer.renderController.show(activityContainers.Info.sharingPart.leftContainer.targetContainer);
        activityContainers.Info.sharingPart.targetBar.mod.setSize([tempAttendingAmount / tempTargetAmount * targetBarSize, defaultFontHeight]);
        if (originalWidth < 400) {
            tempAreInElement = "<span class='mainText'>" + tempAttendingAmount + "</span> IN";
        }
        activityContainers.Info.sharingPart.INTextSmall.setContent(tempAreInElement);
        activityContainers.Info.sharingPart.TargetTextSmall.setContent("Target " + tempTargetAmount);
    } else {
        activityContainers.Info.sharingPart.leftContainer.renderController.show(activityContainers.Info.sharingPart.leftContainer.inContainer);
        activityContainers.Info.sharingPart.INTextLarge.setContent(tempAreInElement);
    }

    if (shouldOpenActivity) {
        goToThisPageOld(draggables.ActivitySmallBox, 1);
        activityContainers.Main.MaximiseActivity();
    }
}

function CreateActivityContainers() {
    // This creates the main container region
    activityContainers.Main.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {}
    });
    activityContainers.Main.isMinimised = false;
    activityContainers.Main.container.chain = new ModifierChain();
    activityContainers.Main.container.state = new StateModifier({
        origin: [1, 1],
        align: [1, 1]
    });
    activityContainers.Main.container.sizeState = new StateModifier({
        origin: [1, 1],
        align: [1, 1]
    });
    activityContainers.Main.container.chain.addModifier(activityContainers.Main.container.sizeState);
    activityContainers.Main.container.chain.addModifier(activityContainers.Main.container.state);

    // endregion

    // The Activity Info container region
    activityContainers.Info.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        content: "Hello",
        properties: {}
    });
    activityContainers.Info.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5]
    });

    activityContainers.Info.picturePart = {};
    activityContainers.Info.picturePart.view = new View();
    activityContainers.Info.picturePart.container = new ContainerSurface({
        size: [originalWidth, originalWidth * heightToWidthRatio],
        properties: {
            backgroundColor: themeColors.main,
        }
    });
    activityContainers.Info.picturePart.image = new ImageSurface({
        size: [originalWidth, originalWidth * heightToWidthRatio],
        content: /*"./id_1112.png"*/ "./id_1112.png",
        properties: {}
    });
    activityContainers.Info.titleBar = new Surface({
        size: [originalWidth, titleBarHeight],
        content: "Private - Party",
        properties: {
            textAlign: "center",
            lineHeight: ToPixelString(titleBarHeight),
            color: themeColors.light
//            backgroundColor: themeColors.darkGlass,
        }
    });
    activityContainers.Info.titleBar.mod = new StateModifier({
        origin: [0, 0],
        align: [0, 0],
        transform: Transform.translate(0, 0, 10)
    });
    activityContainers.Info.picturePart.image.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0, 0.5]
    });

    activityContainers.Info.infoBar = new Surface({
        size: [originalWidth, (originalWidth * heightToWidthRatio)],
        content: "The bottom info will go here",
        properties: {
            textAlign: "center",
            lineHeight: "1.3em",
            color: themeColors.light,
            backgroundColor: themeColors.darkGlass,
            fontSize: 70 + ((originalWidth / 320) * 40) + "%"
        }
    });
    activityContainers.Info.infoBar.mod = new StateModifier({
        origin: [0, 1],
        align: [0, 1],
        transform: Transform.translate(0, 0, 10)
    });

    activityContainers.Info.picturePart.container.add(activityContainers.Info.picturePart.image.mod).add(activityContainers.Info.picturePart.image);

    CreatePictureButton(pictureButtons.LightMenu);
    pictureButtons.LightMenu.view.mod.setAlign([pictureButtons.LightMenu.data.alignX, ((titleBarHeight) * 0.5) / (originalWidth * heightToWidthRatio)]);

    CreatePictureButton(pictureButtons.ThreeDots);
    pictureButtons.ThreeDots.view.mod.setAlign([pictureButtons.ThreeDots.data.alignX, ((titleBarHeight) * 0.5) / (originalWidth * heightToWidthRatio)]);

    CreateTextButton(textButtons.Hide);
    textButtons.Hide.view.mod.setAlign([textButtons.Hide.data.alignX, ((titleBarHeight) * 0.5) / (originalWidth * heightToWidthRatio)]);
    setTextButtonColor(textButtons.Hide, themeColors.light);

    activityContainers.Info.picturePart.container.add(activityContainers.Info.titleBar.mod).add(activityContainers.Info.titleBar);
    activityContainers.Info.picturePart.container.add(activityContainers.Info.infoBar.mod).add(activityContainers.Info.infoBar);
    activityContainers.Info.picturePart.container.add(pictureButtons.LightMenu.view);
    activityContainers.Info.picturePart.container.add(pictureButtons.ThreeDots.view);
    activityContainers.Info.picturePart.container.add(textButtons.Hide.view);
    activityContainers.Info.picturePart.view.add(activityContainers.Info.picturePart.container);

    activityContainers.Info.sharingPart = {};
    activityContainers.Info.sharingPart.view = new View();
    activityContainers.Info.sharingPart.container = new ContainerSurface({
        size: [originalWidth, standardScrollElementHeight],
        properties: {
            backgroundColor: themeColors.lightGlass,
        }
    });
    activityContainers.Info.sharingPart.leftContainer = new ContainerSurface({
        size: [(originalWidth * 0.5), standardScrollElementHeight],
        properties: {
            backgroundColor: themeColors.main,
        }
    });
    activityContainers.Info.sharingPart.leftContainer.mod = new StateModifier({
        origin: [0, 0.5],
        align: [0, 0.5]
    });
    activityContainers.Info.sharingPart.rightContainer = new ContainerSurface({
        size: [(originalWidth * 0.5), standardScrollElementHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.Info.sharingPart.rightContainer.mod = new StateModifier({
        origin: [1, 0.5],
        align: [1, 0.5],
        transform: Transform.translate(0, 0, -10)
    });
    activityContainers.Info.sharingPart.leftContainer.targetContainer = new ContainerSurface({
        size: [(originalWidth * 0.5), standardScrollElementHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.Info.sharingPart.leftContainer.inContainer = new ContainerSurface({
        size: [(originalWidth * 0.5), standardScrollElementHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.Info.sharingPart.lineSurface = new Surface({
        size: [originalWidth, 1],
        properties: {
            backgroundColor: themeColors.lightGrey,
        }
    });
    activityContainers.Info.sharingPart.lineMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.97],
        transform: Transform.translate(0, 0, 1000000)
    });
    activityContainers.Info.sharingPart.leftContainer.renderController = new RenderController();
    activityContainers.Info.sharingPart.leftContainer.add(activityContainers.Info.sharingPart.leftContainer.renderController);
    activityContainers.Info.sharingPart.leftContainer.renderController.show(activityContainers.Info.sharingPart.leftContainer.targetContainer);
    activityContainers.Info.sharingPart.container.add(activityContainers.Info.sharingPart.lineMod).add(activityContainers.Info.sharingPart.lineSurface);

    CreateTextButton(textButtons.PassItOn);
    CreatePictureButton(pictureButtons.Share);
    activityContainers.Info.sharingPart.rightContainer.add(textButtons.PassItOn.view);
    activityContainers.Info.sharingPart.rightContainer.add(pictureButtons.Share.view);
    activityContainers.Info.sharingPart.rightContainer.add(textButtons.PassItOn.view);

    // each of the surfaces also have a modifier
    // Target bar

    // One Surface, the bar size, but with borderRadius
    // size targetBarSize
    var tempAttendingAmount = 120;
    var tempTargetAmount = 300;
    // One surface main colour , with its width set to a percentage of the bar size
    // attendingAmount/targetAmount * targetBarSize
    activityContainers.Info.sharingPart.targetBar = new Surface({
        size: [undefined, undefined],
        //        content: theText,
        properties: {
            backgroundColor: themeColors.main,
            lineHeight: ToPixelString(standardScrollElementHeight + 3),
            textAlign: 'left',
            cursor: 'pointer',
            fontSize: fontSizes.small,
            borderTopLeftRadius: '5px',
            borderBottomLeftRadius: '5px'
        }
    });
    activityContainers.Info.sharingPart.targetBar.mod = new StateModifier({
        size: [tempAttendingAmount / tempTargetAmount * targetBarSize, defaultFontHeight],
        origin: [0, 0.5],
        align: [0, 0.3],
        transform: Transform.translate(((originalWidth * 0.5) - targetBarSize) * 0.5, 0, 0)
    });

    activityContainers.Info.sharingPart.targetBarBorder = new Surface({
        size: [targetBarSize, defaultFontHeight],
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight + 3),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.grey,
            fontSize: fontSizes.small,
            border: '0.1em solid',
            borderRadius: '5px'
        }
    });
    activityContainers.Info.sharingPart.targetBarBorder.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.3],
        transform: Transform.translate(0, 0, 10)
    });

    var tempAreInElement = "<span class='mainText'>248</span> are IN";
    if (originalWidth < 400) {
        tempAreInElement = "<span class='mainText'>248</span> IN";
    }

    activityContainers.Info.sharingPart.INTextSmall = new Surface({
        size: [true, true],
        content: tempAreInElement,
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'left',
            cursor: 'pointer',
            fontSize: fontSizes.smaller,
            borderTopLeftRadius: '5px',
            borderBottomLeftRadius: '5px'
        }
    });
    activityContainers.Info.sharingPart.INTextSmall.mod = new StateModifier({
        origin: [0, 0.5],
        align: [0, 0.7],
        transform: Transform.translate(((originalWidth * 0.5) - targetBarSize) * 0.5, 0, 0)
    });

    activityContainers.Info.sharingPart.TargetTextSmall = new Surface({
        size: [true, true],
        content: "Target 400",
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'left',
            cursor: 'pointer',
            fontSize: fontSizes.smaller,
            borderTopLeftRadius: '5px',
            borderBottomLeftRadius: '5px'
        }
    });
    activityContainers.Info.sharingPart.TargetTextSmall.mod = new StateModifier({
        origin: [1, 0.5],
        align: [1, 0.7],
        transform: Transform.translate(-((originalWidth * 0.5) - targetBarSize) * 0.5, 0, 0)
    });

    activityContainers.Info.sharingPart.INTextLarge = new Surface({
        size: [true, true],
        content: "<span class='mainText'>248</span> are IN",
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'left',
            cursor: 'pointer',
            fontSize: fontSizes.medium,
            borderTopLeftRadius: '5px',
            borderBottomLeftRadius: '5px'
        }
    });
    activityContainers.Info.sharingPart.INTextLarge.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(((originalWidth * 0.5) - targetBarSize) * 0.5, 0, 0)
    });

    // one surface that says "<number of people. are IN" (smaller font)
    // true sized (with a line height that matches the smaller font's height)
    // One surface that says "Target <target number>"
    // The same sizing as the other text surface

    // In Container
    // One surface that says "<number of people. are IN" (small font)
    // the same sizing as the other text surface but the line height's based on the small font instead

    //    activityContainers.Info.sharingPart.leftContainer.targetContainer;
    //    activityContainers.Info.sharingPart.leftContainer.inContainer;
    activityContainers.Info.sharingPart.leftContainer.targetContainer.add(activityContainers.Info.sharingPart.targetBar.mod).add(activityContainers.Info.sharingPart.targetBar);
    activityContainers.Info.sharingPart.leftContainer.targetContainer.add(activityContainers.Info.sharingPart.targetBarBorder.mod).add(activityContainers.Info.sharingPart.targetBarBorder);
    activityContainers.Info.sharingPart.leftContainer.targetContainer.add(activityContainers.Info.sharingPart.INTextSmall.mod).add(activityContainers.Info.sharingPart.INTextSmall);
    activityContainers.Info.sharingPart.leftContainer.targetContainer.add(activityContainers.Info.sharingPart.TargetTextSmall.mod).add(activityContainers.Info.sharingPart.TargetTextSmall);
    activityContainers.Info.sharingPart.leftContainer.inContainer.add(activityContainers.Info.sharingPart.INTextLarge.mod).add(activityContainers.Info.sharingPart.INTextLarge);

    activityContainers.Info.sharingPart.container.add(activityContainers.Info.sharingPart.rightContainer.mod).add(activityContainers.Info.sharingPart.rightContainer);
    activityContainers.Info.sharingPart.container.add(activityContainers.Info.sharingPart.leftContainer.mod).add(activityContainers.Info.sharingPart.leftContainer);
    activityContainers.Info.sharingPart.view.add(activityContainers.Info.sharingPart.container);

    activityContainers.Info.descriptionPart = {};
    activityContainers.Info.descriptionPart.view = new View();
    activityContainers.Info.descriptionPart.container = new Surface({
        size: [originalWidth, true],
        content: "Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description Description",
        properties: {
            lineHeight: '20px',
            backgroundColor: "white",
            padding: ToPixelString(defaultFontHeight)
        }
    });
    activityContainers.Info.descriptionPart.view.add(activityContainers.Info.descriptionPart.container);
    activityContainers.Info.updatesBarExtraSpacing = {};
    activityContainers.Info.updatesBarExtraSpacing.view = new View();
    activityContainers.Info.updatesBarExtraSpacing.container = new Surface({
        size: [originalWidth, activityUpdatesBarHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.Info.updatesBarExtraSpacing.view.add(activityContainers.Info.updatesBarExtraSpacing.container);
    activityContainers.Info.picturePart.container.pipe(scrollers.ActivityInfo.scrollView);
    activityContainers.Info.sharingPart.container.pipe(scrollers.ActivityInfo.scrollView);
    activityContainers.Info.descriptionPart.container.pipe(scrollers.ActivityInfo.scrollView);

    scrollers.ActivityInfo.scrollElements.push(activityContainers.Info.picturePart.view);
    scrollers.ActivityInfo.scrollElements.push(activityContainers.Info.sharingPart.view);
    scrollers.ActivityInfo.scrollElements.push(activityContainers.Info.descriptionPart.view);
    scrollers.ActivityInfo.scrollElements.push(activityContainers.Info.updatesBarExtraSpacing.view);

    theViews.ActivityPages.Info.view.add(scrollers.ActivityInfo.modifier).add(scrollers.ActivityInfo.scrollView);
    // endregion

    //  The Activity Updates container (this is actually made with the activity updates brick creator function?
    CreateActivityUpdatesBrick(activityContainers.UpdatesBrickThing);

    // The Updates Tabs region
    activityContainers.UpdatesTabs.container = new ContainerSurface({
        size: [originalWidth, originalHeight - activityUpdatesBarHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.UpdatesTabs.modifier = new StateModifier({
        origin: [0.5, 1],
        align: [0.5, 1]
    });
    activityContainers.UpdatesTabs.renderController = new RenderController();
    // This is the view that gets shown by the Updates Tabs renderController (it's never switched, just hidden and shown)
    activityContainers.UpdatesTabs.viewToShow = new View();

    // Post a Status Brick
    activityContainers.UpdatesTabs.postStatusBrick = new View();
    CreatePostStatusBrick(activityContainers.UpdatesTabs.postStatusBrick);
    activityContainers.UpdatesTabs.postStatusBrick.CompletelyHideBrick();

    activityContainers.UpdatesTabs.container.add(activityContainers.UpdatesTabs.renderController);

    // endregion

    // The Updates Bar region
    activityContainers.UpdatesBar.container = new ContainerSurface({
        size: [originalWidth, activityUpdatesBarHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.UpdatesBar.upContainer = new ContainerSurface({
        size: [originalWidth, activityUpdatesBarHeight],
        properties: {
            backgroundColor: "white",
            borderBottom: "0.1em solid " + themeColors.greyHighlight,
        }
    });
    activityContainers.UpdatesBar.downContainer = new ContainerSurface({
        size: [originalWidth, activityUpdatesBarHeight],
        properties: {
            backgroundColor: "white",
        }
    });
    activityContainers.UpdatesBar.modifier = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0]
    });

    // all the things that change are added into two seperate containers that get switched between (this means the arrow can go from pointing up to down when the rendercontroller switches
    activityContainers.UpdatesBar.renderController = new RenderController();
    activityContainers.UpdatesBar.container.add(activityContainers.UpdatesBar.renderController);
    activityContainers.UpdatesBar.shadow = new ImageSurface({
        size: [shadowSize, shadowSize],
        content: "./Images/brickShadow.svg",
        properties: {}
    });
    activityContainers.UpdatesBar.shadowChain = new ModifierChain();
    activityContainers.UpdatesBar.shadowScaleModifier = new StateModifier({
        transform: Transform.scale(originalWidth / (shadowSize * 0.9), 1, 1)
    });
    activityContainers.UpdatesBar.shadowPositionModifier = new StateModifier({
        origin: [0.5, 0.5],
        align: [0, 0],
        transform: Transform.translate(0, -(shadowSize * 0.5), 0)
    });
    activityContainers.UpdatesBar.shadowChain.addModifier(activityContainers.UpdatesBar.shadowScaleModifier);
    activityContainers.UpdatesBar.shadowChain.addModifier(activityContainers.UpdatesBar.shadowPositionModifier);
    activityContainers.UpdatesBar.downContainer.add(activityContainers.UpdatesBar.shadowChain).add(activityContainers.UpdatesBar.shadow);

    activityContainers.UpdatesBar.downContainer.text = new Surface({
        size: [originalWidth - (defaultIconSize * 2.5), activityUpdatesBarHeight],
        content: "Sync",
        properties: {
            textAlign: 'center',
            fontSize: fontSizes.small,
            lineHeight: ToPixelString(activityUpdatesBarHeight),
            color: themeColors.lightGrey
        }
    });
    activityContainers.UpdatesBar.downContainer.text.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 10)
    });
    activityContainers.UpdatesBar.upContainer.text = new Surface({
        size: [originalWidth - (defaultIconSize * 2.5), activityUpdatesBarHeight],
        content: "Sync",
        properties: {
            textAlign: 'center',
            fontSize: fontSizes.small,
            lineHeight: ToPixelString(activityUpdatesBarHeight),
            color: themeColors.lightGrey
        }
    });
    activityContainers.UpdatesBar.upContainer.text.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5],
        transform: Transform.translate(0, 0, 10)
    });
    activityContainers.UpdatesBar.upContainer.arrow = new ImageSurface({
        size: [activityUpdatesBarHeight, activityUpdatesBarHeight],
        content: "./Images/downArrow.svg",
        properties: {}
    });
    activityContainers.UpdatesBar.upContainer.arrow.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0, 0.5],
        transform: Transform.translate(0, 0, 5)
    });
    activityContainers.UpdatesBar.downContainer.arrow = new ImageSurface({
        size: [activityUpdatesBarHeight, activityUpdatesBarHeight],
        content: "./Images/upArrow.svg",
        properties: {}
    });
    activityContainers.UpdatesBar.downContainer.arrow.mod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0, 0.5],
        transform: Transform.translate(0, 0, 5)
    });

    CreatePictureButton(pictureButtons.UpdatesMenu);
    activityContainers.UpdatesBar.upContainer.add(pictureButtons.UpdatesMenu.view);

    CreateTextButton(textButtons.UpdatesHide);
    activityContainers.UpdatesBar.upContainer.add(textButtons.UpdatesHide.view);
    activityContainers.UpdatesBar.upContainer.add(activityContainers.UpdatesBar.upContainer.arrow.mod).add(activityContainers.UpdatesBar.upContainer.arrow);
    activityContainers.UpdatesBar.downContainer.add(activityContainers.UpdatesBar.downContainer.arrow.mod).add(activityContainers.UpdatesBar.downContainer.arrow);
    activityContainers.UpdatesBar.downContainer.add(activityContainers.UpdatesBar.downContainer.text.mod).add(activityContainers.UpdatesBar.downContainer.text);
    activityContainers.UpdatesBar.upContainer.add(activityContainers.UpdatesBar.upContainer.text.mod).add(activityContainers.UpdatesBar.upContainer.text);

    activityContainers.UpdatesBar.renderController.show(activityContainers.UpdatesBar.downContainer);
    activityContainers.UpdatesBar.downContainer.text.on('click', function () {
        activityContainers.UpdatesBrickThing.MoveBrickUp(originalHeight);
    });
    activityContainers.UpdatesBar.upContainer.text.on('click', activityContainers.UpdatesBrickThing.HideBrick);
    activityContainers.UpdatesBrickThing.container.add(activityContainers.UpdatesBar.modifier).add(activityContainers.UpdatesBar.container);
    activityContainers.UpdatesBrickThing.container.add(activityContainers.UpdatesTabs.modifier).add(activityContainers.UpdatesTabs.container);
    // endregion

    // This creates an adds the renderControllers to the containers that have one
    activityContainers.Main.renderController = new RenderController();
    activityContainers.Info.renderController = new RenderController();

    theViews.ActivityPages.Picture.image = new ImageSurface({
        size: [undefined, undefined],
        content: "./id_1112.png",
        properties: {}
    });
    theViews.ActivityPages.Picture.mod = new Modifier({
        opacity: 0.5
    });
    theViews.ActivityPages.Picture.view.add(theViews.ActivityPages.Picture.mod).add(theViews.ActivityPages.Picture.image);
    theViews.ActivityPages.Main.container = new ContainerSurface({
        size: [originalWidth, originalHeight],
        properties: {
            backgroundColor: themeColors.main,
            //            lineHeight: '200px',
            textAlign: 'center',
            cursor: 'pointer'
        }
    });
    theViews.ActivityPages.Main.view.add(activityContainers.Info.mod).add(activityContainers.Info.container);
    theViews.ActivityPages.Main.view.add(activityContainers.UpdatesBrickThing.modifier).add(activityContainers.UpdatesBrickThing.container);

    activityContainers.Main.MinimiseActivity = function () {
        activityContainers.UpdatesTabs.postStatusBrick.CompletelyHideBrick();
        mainRenderController.show(appSections.MainPages);
        activityContainers.Main.renderController.show(theViews.ActivityPages.Picture.view);
        draggables.ActivitySmallBox.draggable.activate();
        activityContainers.Main.isMinimised = true;
        activityContainers.Main.container.sizeState.setOrigin([1, 1]);
        activityContainers.Main.container.sizeState.setAlign([1, 1]);
        activityContainers.Main.container.state.setOrigin([1, 1]);
        activityContainers.Main.container.state.setAlign([1, 1]);

        activityContainers.Main.container.sizeState.setTransform(
            Transform.scale((activityBoxWidth) / originalWidth, (activityBoxWidth) / originalHeight, 1), {
                duration: 400,
                curve: 'easeInOut'
            }
        );

        activityContainers.Main.container.state.setTransform(
            Transform.translate(activityBoxWidth, 0, 900), {
                duration: 400,
                curve: 'easeInOut'
            }
        );

    };
    activityContainers.Main.MinimiseActivity();
    activityContainers.Main.MaximiseActivity = function () {
        mainRenderController.hide();
        activityContainers.Main.renderController.show(theViews.ActivityPages.Main.view);
        draggables.ActivitySmallBox.draggable.deactivate();
        activityContainers.Main.isMinimised = false;

        activityContainers.Main.container.sizeState.setTransform(
            Transform.scale(1, 1, 1), {
                duration: 400,
                curve: 'easeInOut'
            }
        );
        activityContainers.Main.container.state.setTransform(
            Transform.translate(activityBoxWidth, 0, 900), {
                duration: 400,
                curve: 'easeInOut'
            }
        );
        Timer.setTimeout(function () {

            activityContainers.Main.container.sizeState.setOrigin([0.5, 0]);
            activityContainers.Main.container.sizeState.setAlign([0.5, 0]);
            activityContainers.Main.container.state.setOrigin([0.5, 0]);
            activityContainers.Main.container.state.setAlign([0.5, 0]);
        }, 400);

        if (activityContainers.UpdatesBrickThing.isUp === true) {
            activityContainers.UpdatesTabs.postStatusBrick.HideBrick();
        }
    };
    activityContainers.Main.SwapFromMinimisedOrMaximised = function () {

        if (scrollviewHasBeenTurnedOff === false && draggables.ActivitySmallBox.data.currentPage == 1) {
            if (activityContainers.Main.isMinimised === true) {
                activityContainers.Main.MaximiseActivity();
            } else {
                activityContainers.Main.MinimiseActivity();
            }
        }
    };

    activityContainers.Main.container.add(activityContainers.Main.renderController);
    activityContainers.Main.container.pipe(draggables.ActivitySmallBox.draggable);
}

function createStatusScrollElement(theStatusId, theScroller, theIndexPosition, startHidden, isInAllUpdates /*, isSearched*/ ) {
    var tempScrollElementView = new View();

    var theStatusObject = inAppDatabases.statuses({
        statusId: theStatusId
    }).first();
    var theActivityObject = inAppDatabases.activities({
        id: theStatusObject.activityId
    }).first();
    var fromUserObject = inAppDatabases.mobilizrUsers({
        id: theStatusObject.fromUserId
    }).first();

    var amountOfLettersOnALineSmall = calculateLetterAmountOnLine(originalWidth - 65, GetNumberFromString(fontSizes.small));
    var amountOfLettersOnALineSmallWithIcons = calculateLetterAmountOnLine(originalWidth - (originalWidth * 0.25), GetNumberFromString(fontSizes.small));
    var amountOfLettersOnALineMedium = calculateLetterAmountOnLine(originalWidth - 65, GetNumberFromString(fontSizes.medium));
    var amountOfLettersOnALineSmaller = calculateLetterAmountOnLine(originalWidth - 65, GetNumberFromString(fontSizes.smaller));
    // this is a variable used for when the statuses are searched 
    tempScrollElementView.isSafe = false;
    tempScrollElementView.isHidden = false;
    if (startHidden) {
        tempScrollElementView.isHidden = true;
    }
    tempScrollElementView.searchId = theStatusId;
    // TODO may be a good idea to include the name variable as a property of the view so it cn be searched for
    var tempName = fromUserObject.name; //"Haley";
    tempScrollElementView.name = tempName.toLowerCase();
    var tempActivityName = theActivityObject.name; //"RSU: March In August For A Better Government";
    var tempStatusText = theStatusObject.statusText; //"I'm at the State Library entrance";
    var tempPicturePath = fromUserObject.picture; //'./Images/displayPicTest.png';
    var tempTimeSent = theStatusObject.timeUpdated; //1422330140; // about 2:42pm Tuesday
    tempScrollElementView.timeUpdated = tempTimeSent;
    var tempTimeSentAgo = Math.round((GetCurrentTime() - tempTimeSent) / 60); // this is the current time minues the time the status was sent , and it's in seconds because of the timestamp fromat, so it's divided by 60 to get the amount of minutes that've passed since the status was updated
    var containerHeight = standardScrollElementHeight * 1.1 /*0.75*/ ;
    var bottomLine = '<div style="width: ' + (originalWidth - 65) + 'px; height: 1.4px; background: ' + themeColors.light + '; overflow: hidden; float:right;"></div><br>';

    if (isInAllUpdates === true) {
        containerHeight += (defaultFontHeight * GetNumberFromString(fontSizes.smaller) * 2);

        if (tempActivityName.length > amountOfLettersOnALineSmaller) {
            containerHeight += (defaultFontHeight * GetNumberFromString(fontSizes.smaller) * 1);

        } else {

        }
    }

    var tempNameElement = "";

    var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempPicturePath + "); background-size: 46px 46px;' onclick=\"this.className+=' transparentClass'\"></div></div></div></div>";

    var firstLetterOfActivity = tempActivityName.charAt(0).toUpperCase();
    var letterPicElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-color: #9db9e0;'><div class ='letterDisplayPic'>" + firstLetterOfActivity + "</div></div></div></div></div>";

    var tempAllUpdatesActivityNameElement = "";
    if (isInAllUpdates) {
        tempAllUpdatesActivityNameElement = "<div class = 'statusSmallActivity'>" + tempActivityName + "</div>";
    }

    var tempTimeSentElement = "<div class = 'statusTime'>" + tempTimeSentAgo + " mins ago</div>";
    if (tempTimeSentAgo >= 60) {
        var tempRemainderMins = tempTimeSentAgo % 60;
        tempTimeSentElement = "<div class = 'statusTime'>" + Math.ceil(tempTimeSentAgo / 60) + " hours and " + tempRemainderMins + " mins ago</div>";
    }
    var tempStatusTextElement = "<div class = 'statusText'>" + tempStatusText + "</div>";

    var tempStringTest = "";

    if (theStatusObject.statusType === 0) {
        if (theStatusObject.amountOfTimesUpdated == "0") {
            var tempFullStatusText = tempName + " is coming to " + tempActivityName;
            //            containerHeight += amountToIncreaseStatusHeightBy(tempFullStatusText.length, amountOfLettersOnALineSmallWithIcons, (defaultFontHeight * GetNumberFromString(fontSizes.small) * 1.1));
            tempNameElement = "<span class = 'statusText darkColor' >" + tempFullStatusText + "</span>";
            tempStringTest = tempPictureElement + "<div class = 'shiftStatusText'style='padding-right:" + (originalWidth * 0.25) + "px; '>" + tempNameElement + tempTimeSentElement + "</div>" + bottomLine;
        } else {
            tempNameElement = "<div class = 'statusName'>" + tempName + "</div>";
            tempStringTest = tempPictureElement + "<div class = 'shiftStatusText'>" + tempNameElement + tempStatusTextElement + tempAllUpdatesActivityNameElement + tempTimeSentElement + "</div>" + bottomLine;
        }
    } else if (theStatusObject.statusType == 1) {
        tempScrollElementView.name = tempActivityName.toLowerCase();
        tempNameElement = "<div class = 'statusName'>" + tempActivityName + "</div>";
        tempStringTest = letterPicElement + "<div class = 'shiftStatusText'>" + tempNameElement + tempStatusTextElement + tempAllUpdatesActivityNameElement + tempTimeSentElement + "</div>" + bottomLine;

    } else if (theStatusObject.statusType == 2) {
        var tempRequestText = fromUserObject.name + " wants to know where you're at";
        containerHeight += amountToIncreaseStatusHeightBy(tempRequestText.length, amountOfLettersOnALineSmall, (defaultFontHeight * GetNumberFromString(fontSizes.small) * 2.0));
        var statusRequestElement = "<div class = 'statusText'>" + fromUserObject.name + "</span> wants to know where you're at </div>";
        tempStringTest = tempPictureElement + "<div class = 'shiftStatusText'>" + statusRequestElement + tempAllUpdatesActivityNameElement + tempTimeSentElement + "</div>" + bottomLine;
    } else if (theStatusObject.statusType == 3) {
        tempScrollElementView.name = tempActivityName.toLowerCase();
        var updatedInfoText = "" + theActivityObject.name + theStatusObject.statusText;
        var updatedInfoTextElement = " <span class = 'darkColor'>" + theActivityObject.name + "</span> " + theStatusObject.statusText;
        containerHeight += amountToIncreaseStatusHeightBy(updatedInfoText.length, amountOfLettersOnALineSmall, (defaultFontHeight * GetNumberFromString(fontSizes.small) * 1.5));
        var updatedInfoElement = "<div class = 'statusText'>" + updatedInfoTextElement + " </div>";
        tempStringTest = letterPicElement + "<div class = 'shiftStatusText'>" + updatedInfoElement + tempAllUpdatesActivityNameElement + tempTimeSentElement + "</div>" + bottomLine;
    }

    if (theStatusObject.statusType === 0 || theStatusObject.statusType == 1) {
        containerHeight += amountToIncreaseStatusHeightBy(tempStatusText.length, amountOfLettersOnALineSmall, (defaultFontHeight * GetNumberFromString(fontSizes.small) * 1.5));
        containerHeight += amountToIncreaseStatusHeightBy(tempName.length, amountOfLettersOnALineMedium, (defaultFontHeight * GetNumberFromString(fontSizes.medium) * 1));
    }

    tempScrollElementView.textSurface = new Surface({
        size: [undefined, true],
        content: tempStringTest,
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            cursor: 'pointer',
            color: themeColors.light,
            fontSize: fontSizes.small,
        }
    });

    tempScrollElementView.textMod = new StateModifier({
        origin: [0.5, 0],
        align: [0.5, 0]
    });

    var tempContainerBackground = "white";

    tempScrollElementView.container = new Surface({
        size: [undefined, true],
        content: tempStringTest,
        properties: {}
    });

    tempScrollElementView.originalHeight = containerHeight;
    // (if it's statusType 1 or 3)
    // activityLetterSurface

    // whichever display pic (or letter box) it generated, they will be put inside a div that makes the hexagon thing happen  

    // (if it's any of them)
    // timeSentSurface


    if (theStatusObject.statusType === 0) {
        tempScrollElementView.requestButton = CreateTextButton(null, true, "3456");
        tempScrollElementView.requestButton.surface.on('click', function () {
            tempScrollElementView.requestButton.surface.setContent('<span>   <img src="./Images/star138.svg" alt="Smiley face" height="22" width="22">   </span>');
        });
        //        tempScrollElementView.container.add(tempScrollElementView.requestButton);

        tempScrollElementView.adminButton = new ImageSurface({
            size: [30, 30],
            content: './Images/adminButton.svg'
        });
        tempScrollElementView.adminButton.mod = new StateModifier({
            origin: [1, 0.5],
            align: [0.5, 0],
            transform: Transform.translate(-(defaultFontWidth * 3) - (30 /*+ 5 + 30*/ ), defaultFontHeight * 1.5, 0)
        });
        tempScrollElementView.adminButton.on('click', function () {
            alert("Admin Button Clicked");
        });
        //        tempScrollElementView.container.add(tempScrollElementView.adminButton.mod).add(tempScrollElementView.adminButton);
    }
    // The line width will always be the shorter one to the right
    //    tempScrollElementView.lineSurface = new Surface({
    //        size: [theScroller.data.width - 65, 1],
    //        properties: {
    //            backgroundColor: themeColors.lightGrey,
    //        }
    //    });
    //    tempScrollElementView.lineMod = new StateModifier({
    //        origin: [1, 0.5],
    //        align: [1, 1]
    //    });

    //    tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);

    if (!theIndexPosition) {
        theIndexPosition = 0;
    }

    var tempStartOpacity = 1;
    if (startHidden) {
        tempStartOpacity = 0;
        tempScrollElementView.container.setSize([undefined, 0]);
    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });
    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    // Hopefully the thing is autmatically added to the scroll elements array when it's added to the scrollView because that's the scrollViews dataSource thing
    //    theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);

    theScroller.scrollView.insert(theIndexPosition, tempScrollElementView);
}

function CreateActivityScrollElement(theActivityId, theScroller, theIndexPosition, startHidden, theInvitationId) {
    var tempScrollElementView = new View();
    var theInvitationObject;
    var theActivityObject;
    var invitedFromElement = "";
    var invitedFromLineElement = "";
    // this is the temp Bottom Bar Text
    var tempBottomBarText = "";

    var tempImageHeight = originalWidth * 0.65;
    var tempImagePartWidth = theScroller.data.width;
//    var picturePaddingSide = 4 * defaultPaddingHorizontal;
    var picturePaddingSide = 0;
    var tempHeightBasedOnWidth = heightToWidthRatio * tempImagePartWidth;
    var tempImageYOffset = 0;

    if (theInvitationId) {
        theInvitationObject = inAppDatabases.invitations({
            invitationId: theInvitationId
        }).first();
        theActivityObject = inAppDatabases.activities({
            id: theInvitationObject.connectedActivity
        }).first();
        var theInviterObject = inAppDatabases.mobilizrUsers({
            id: theInvitationObject.sentFromUser
        }).first();

        theActivityId = theActivityObject.id;
        var lettersInALineInvitedText = calculateLetterAmountOnLine(tempImagePartWidth, GetNumberFromString(fontSizes.small));
        var tempInvitedFromText = theInviterObject.name + " invited you to"; // this is used to check if it's on one or two lines
        tempImageYOffset += amountToIncreaseStatusHeightBy(tempInvitedFromText.length, lettersInALineInvitedText, defaultFontHeight * GetNumberFromString(fontSizes.small) * 2);
        
        var shortenedInviterName = theInviterObject.name.split(' ')[0];

        invitedFromElement = "<div class = 'invitedByElementFont' style = 'position:absolute; top: "+(tempImageHeight + 7)+"px; text-align: center; width: 100%;'><span class = 'lightGreyColor'> invited by: " + shortenedInviterName + "</span></div>";
        
//        invitedFromLineElement = '<div style="width: ' + (tempImagePartWidth) + 'px; height: 1.4px; background: ' + themeColors.light + '; overflow: hidden; float:right; padding-right:' + picturePaddingSide + 'px;"></div>';
        tempBottomBarText = "or";
    } else {
        theActivityObject = inAppDatabases.activities({
            id: theActivityId
        }).first();
        tempImageYOffset += defaultFontHeight * 0.75;
    }

    var tempActivityName = theActivityObject.name;
    var tempActivityPicturePath = theActivityObject.picture;
    //NOTE the organisation text part can go here too when it's needed

    var lettersInALineActivityName = calculateLetterAmountOnLine(tempImagePartWidth, GetNumberFromString(fontSizes.small));
    tempImageYOffset += amountToIncreaseStatusHeightBy(tempActivityName.length, lettersInALineActivityName, defaultFontHeight * GetNumberFromString(fontSizes.medium) * 2);

    var tempOnClickFunction = "console.log(\"The Activity Container was clicked!\"); console.log(Math.abs(draggables.MainPages.container.getOffset() % (originalWidth - 2))); if (Math.abs(draggables.MainPages.container.getOffset() % (originalWidth - 2)) < ( 10)) { updateActivityInfoPage(" + theActivityObject.id + "); "+ stopFurtherClicks+ "}";

    var tempPassItOnFunction = "alert(event + \"The Pass it On button was clicked!\");" + stopFurtherClicks;

    var tempINFunction = "event.bubbles = false; console.log(event);" + stopFurtherClicks;
    var tempOUTFunction = "event.bubbles = false; console.log(event);" + stopFurtherClicks;

//    var tempINFunction = "alert(event + \"The IN button was clicked!\");" + stopFurtherClicks;
//    var tempOUTFunction = "alert(event + \"The Out button was clicked!\");" + stopFurtherClicks;


    // at the moment it uses a global event variable handed in?
    // this may need to instead run a function
    // have to preent the background
    //  potentially in this function, set a variable for this activity scroller that prevents
    // DONE Event prevent default http://stackoverflow.com/questions/985389/how-can-i-stop-an-onclick-event-from-firing-for-parent-element-when-child-is-cli

    var lowerInnerButtonDiv = "";
    if (theInvitationId) {
        
        //style='position:absolute; left:0px ; top: "+(tempImageHeight * 0.5)+"px;'
        
        var inButtonElement = "<img src = './Images/inButton.svg' width = '70px' height = '60px'  onmousedown='" + tempINFunction + "' >";        
        var outButtonElement = "<img src = './Images/outButton.svg' width = '70px' height = '60px' onmousedown='" + tempOUTFunction + "'>";

        
//        lowerInnerButtonDiv = "<div style = 'width:80px ; height: 30px; line-height : 30px; border: 1px solid white; border-radius: 5px ; display:inline-block;'onclick='" + tempINFunction + "' >In</div> or <div style = 'width:80px ; height: 30px; line-height : 30px; border: 1px solid white; border-radius: 5px ; display:inline-block;'  onclick='" + tempOUTFunction + "' >Out</div>";
        lowerInnerButtonDiv = inButtonElement + "<span style='position: relative; top: 90%; transform: translateY(-90%); display: inline-block;'> &nbsp; or &nbsp; </span>" + outButtonElement;
    } else {
        lowerInnerButtonDiv = "<div style = 'width:80px ; height: 30px; line-height : 30px; border: 1px solid white; border-radius: 5px ; ' onmousedown ='" + tempPassItOnFunction + "'>Pass It On</div>";
    }

    var pictureDiv = "<div onmousedown='" + tempOnClickFunction + "' style = 'background-image: url(" + tempActivityPicturePath + ") ; background-position: center; overflow:hidden; width:" + tempImagePartWidth + "px;height:" + tempImageHeight + "px; background-size:cover; position: relative;width: display: block; margin-left: auto; margin-right: auto;'><p> <div  style = 'background: " + "rgba(0, 0, 0, 0.24)" + "; bottom: 0; position: absolute;  width:" + tempImagePartWidth + "px; height: " + tempImageHeight + "px; '><span style='position: relative; top: 40%; transform: translateY(-50%); display: inline-block; color: white'>" + lowerInnerButtonDiv + "</span></div></div>";

    // the function will be inline cause the div can dissapear if its scrolled out of view, so it wont be able to find the id

    var tempPrivacyAndCategoryElement = "<div style = 'position:absolute; top: "+(5)+"px; text-align: center; width: 100%; color:white;'><div class = 'fontSizeSmaller'>" + getPrivacyStringFromNumber(theActivityObject.privacy) + " - " + theActivityObject.category +"</div></div>";

    
//    var tempActivtyNameElement = "<div class = 'activityNameMargin' style='position:absolute; top: 30px; left : 30px; color: white;'><div class = 'fontSizeMedium'>" + tempActivityName + "</div></div>";
    var tempActivtyNameElement = "<div style='position:absolute; top: " + (tempImageHeight - 50) + "px; left : 70px; color: white;'><div class = 'fontSizeSmall'>" + tempActivityName + "</div></div>";
    
    //style = 'position: absolute; left: 30px'

//    var activityStartDateText = moment.unix(theActivityObject.timeStarting).format('h:mm a, MMMM Do, YYYY');
    var activityStartDateText = moment.unix(theActivityObject.timeStarting).format('MMM Do');
    activityStartDateText = activityStartDateText.split(' ');
    var ActivityStartDateElement = "<div class = 'activityDate'> <div style = 'position:absolute; top: "+(tempImageHeight - 48)+"px; left: 10px; border: 1.5px solid white; width: 30px; border-radius : 5px; text-align:center; padding-left: 5px; padding-right: 5px; padding-top: 3px; padding-bottom: 3px; color:white;'>" + activityStartDateText[0].toUpperCase() + "<br><b><span style = 'font-family : opensansBold; color: white;'>" + activityStartDateText[1] + "</span></b></div></div></div>";

    //  get an array of all the people going to the activity
    var tempjoinedFriendsArray = GetUsersAtActivity(theActivityId);
    var allTheFaceBoxesElement = "";
//    for (var i = 0; i < tempjoinedFriendsArray.length; i++) {
//        var tempLoopedUser = inAppDatabases.mobilizrUsers({
//            id: tempjoinedFriendsArray[i]
//        }).first();
//
//        var tempNewFaceBox = "<div style = 'position:absolute; right: " + ((i * 35) - 10 + picturePaddingSide) + "px; float:right;'><div class='hexagon hexagon2'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempLoopedUser.picture + "); background-size: 34px 34px;'></div></div></div></div>";
//
//        allTheFaceBoxesElement += tempNewFaceBox;
//    }

//    var tempBottomSurfaceElement = "<div class = floatingLeft style='padding-left: " + picturePaddingSide + "px; position:absolute;'><img src = './Images/smallInButton.svg' width = '30px' height = '25px' style='position:absolute; left:0px ; top: "+(tempImageHeight + 60)+"px;' > <span class = 'lightGreyColor' style='position:absolute; left:32px ;'>" + theActivityObject.peopleJoined + "</span></div> <br><br>" + invitedFromElement;
    var tempPeopleGoingElement = "<img src = './Images/smallInButton.svg' width = '30px' height = '25px' style='position:absolute; left:0px ; top: "+(tempImageHeight + 2)+"px;' > <span class = 'lightGreyColor' style='position:absolute; left:32px ;top: "+(tempImageHeight + 2)+"px;'>" + theActivityObject.peopleJoined + "</span>" + invitedFromElement;
    var tempFriendsGoingElement = "<img src = './Images/noAvatarUser.svg' width = '30px' height = '25px' style='position:absolute; right:26px ; top: "+(tempImageHeight + 2)+"px;' > <span class = 'lightGreyColor' style='position:absolute; right:6px ;top: "+(tempImageHeight + 2)+"px;'>" + theActivityObject.peopleJoined + "</span>" + invitedFromElement;
    
    var tempBottomSpacer = "<br><br>";
    
    var tempBottomSurfaceElement = tempPeopleGoingElement + invitedFromElement + tempFriendsGoingElement + tempBottomSpacer;

//    var tempFriendsGoingElement = "<img src = './Images/noAvatarUser.svg' width = '30px' height = '25px' style='position:absolute; right:0px ; top: "+(tempImageHeight - 30)+";' > <div class = 'lightGreyColor' style='position:absolute; right:32px ;'>" + theActivityObject.peopleJoined + "</div>";

//    var tempTopSurfaceElement = pictureDiv + tempBottomSurfaceElement + ActivityStartDateElement + tempActivtyNameElement;
    var tempTopSurfaceElement = pictureDiv + tempPrivacyAndCategoryElement + ActivityStartDateElement + tempActivtyNameElement + tempBottomSurfaceElement;

    var containerHeight = (standardScrollElementHeight * 1.2) + tempImageHeight + tempImageYOffset;

    if (theScroller == scrollers.Invitations || theScroller == scrollers.Declined) {
        //        tempInvitedFromText = ""; // this is used to check if it's on one or two lines
        //        invitedFromElement = "";
    } else if (theScroller == scrollers.JoinedActivities) {

    } else if (theScroller == scrollers.CreatedActivities) {

    } else {
        console.error("Accidently Tried to add an activity scrollElement to an incorrect scroller");
    }

    tempScrollElementView.container = new Surface({
//        size: [undefined, tempImageHeight + 45],
        size: [undefined, true],
        content: tempTopSurfaceElement,
        properties: {
            overflow: 'hidden',
            lineHeight: ToPixelString(defaultFontHeight * 1.5)
        }
    });

    if (!theIndexPosition) {
        theIndexPosition = 0;
    }

    var tempStartOpacity = 1;
    if (startHidden) {
        tempStartOpacity = 0;
        tempScrollElementView.container.setSize([undefined, 0]);
    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });
    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    tempScrollElementView.container.pipe(theScroller.scrollView);
    //    theScroller.scrollElements.push(tempScrollElementView);
    theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
}

function CreateContactScrollElement(contactId, theScroller, theIndexPosition, startHidden) {
    //    console.log("All the phone contacts!");
    //    console.log( inAppDatabases.phoneContacts().first());
    var tempScrollElementView = new View();
    var theContactObject = inAppDatabases.phoneContacts({
        phoneContactId: contactId
    }).first();
    var relatedMobilizrUser = null;

    if (theContactObject.mobilizrId !== null) {
        relatedMobilizrUser = inAppDatabases.mobilizrUsers({
            id: theContactObject.mobilizrId
        }).first();
    }

    var tempNameText = theContactObject.contactname;
    var tempPicturePath = './Images/user109.svg'; //'./Images/displayPicTest.png';
    if (relatedMobilizrUser) {
        tempNameText = relatedMobilizrUser.name;


        tempPicturePath = relatedMobilizrUser.picture; //'./Images/displayPicTest.png';
    }

    tempScrollElementView.contactId = contactId;
    tempScrollElementView.name = tempNameText.toLowerCase();
    tempScrollElementView.searchId = contactId;

    var tempTextOffset = 0;

    if (!theIndexPosition) {
        theIndexPosition = 0;
    }

    // the onclick can be a closure that takes in the object in the brackets?

    //    var tempCheckboxElement = "<span style='background-color:black' onclick=' 

    // TODO Stop click through clicks here?
    var stopFurtherClicks = ""; //"if (!e) var e = window.event; e.cancelBubble = true;if (e.stopPropagation) e.stopPropagation(); console.log('the click should have been stopped');";

    var onclickPart = "onmousedown='pressACheckBox(" + contactId + "); " + stopFurtherClicks + "'";

    var tempCheckboxElement = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxEmpty.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    var tempCheckboxElementTicked = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxTicked.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    var tempNameElement = "<div class = 'shiftContactText' style = 'width:100%' >" + tempNameText + tempCheckboxElement + "</div>";

    var tempNameElementTicked = "<div class = 'shiftContactText' style = 'width:100%; color : " + themeColors.main + ";' >" + tempNameText + tempCheckboxElementTicked + "</div>";

    var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempPicturePath + "); background-size: 46px 46px;'></div></div></div></div>";


    //style = ' right: 0px;'><img src = './Images/checkBoxTicked.svg' style = 'position:absolute; right: 0px; display:inline;'

    // check if it is selected if it is then, show it, if it isn't selected then hide it (switch to the appropriate class)

    var bottomLine = '<div style="width: ' + (originalWidth - 65) + 'px; height: 1.4px; background: ' + themeColors.light + '; overflow: hidden; float:right;"></div>';

    tempScrollElementView.tempContentElement = tempPictureElement + tempNameElement + bottomLine;

    tempScrollElementView.tempContentElementTicked = tempPictureElement + tempNameElementTicked + bottomLine;

    tempScrollElementView.isTicked = false;

    // this will edit the inAppDataBase contact directly
    //        if ( linkedVariable) {
    //            tempScrollElementView.linkedVariable = linkedVariable;
    //        }

    tempScrollElementView.container = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: tempScrollElementView.tempContentElement,
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            cursor: 'pointer',
        }
    });

    // open the profile for the selected contact
    //tempScrollElementView.container.on('click', functionToRun);
    //        }
    var tempStartOpacity = 1;
    if (startHidden) {
        tempStartOpacity = 0;
        tempScrollElementView.container.setSize([undefined, 0]);
    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    //    theScroller.scrollElements.push(tempScrollElementView);
    if (theIndexPosition !== 0) {
        theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
    } else {
        theScroller.scrollElements.push(tempScrollElementView);
    }
}


//endregion
//====================================================-


//====================================================-
//Settings Pages region
//|34|5987|312|946.01|616.37|1----------------------------------------------------------------------------------------------~
function AddButtonsToSettingsPages() {
    // This adds the top right hand corner back and done buttons to the settings pages
    // the BackToSettings textButton should work for all of the pages, it could either be cloned or added everytime someone goes to a new settings page?
    CreateTextButton(textButtons.BackToSettings);
    theViews.SettingsPages.Interests.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.HelpFAQ.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.ContactUs.view.add(textButtons.BackToSettings.view);
    //    theViews.SettingsPages.GettingStarted.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.Suggestion.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.Notifications.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.Notifications.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.Privacy.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.PaymentInfo.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.ChangeNumber.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.OldNumber.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.NewNumber.view.add(textButtons.BackToSettings.view);
    theViews.SettingsPages.DeleteAccount.view.add(textButtons.BackToSettings.view);

    /*
    Settings
    Profile
    Interests
    HelpFAQ
    ContactUs
    GettingStarted
    Suggestion
    Notifications
    Privacy
    PaymentInfo
    ChangeNumber
    OldNumber
    NewNumber
    DeleteAccount
    
    */


    console.log("This should mean the button is added to the HelpFAQ settings page");
    //    theViews.SettingsPages.GettingStarted.view.add(textButtons.BackToSettings);
}

function CreateProfilePageElements() {
    // this creates a scroll element with a large hexagon profile picture (in a hexagon) on the left and adds it to the "Customize Profile

    // Create the html element for the picture hexagon (its initial picture is that saying "Add Photo"
    //FUTURE in the future it could be initially text in the profile picture hexagon (so the language can be easily changed)

    var tempProfilePhotoContentString = "";

    var tempNameElement = "";

    theViews.SettingsPages.Profile.updateImage = function () {
        console.log("The viewable profile pic is about to be updated");
        var tempPicturePath = temporaryUserObject.pictureDataUrl; //theUser.picturePath;//'./Images/user109.svg';//'./Images/displayPicTest.png';
        console.log(tempPicturePath);
        var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon3'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempPicturePath + "); background-size: 100px 100px;' onclick=\"this.className+=' transparentClass'\"></div></div></div></div>";

        var tempNameText = " Name Here";
        var tempSideBarPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempPicturePath + "); background-size: 100px 100px;' onclick=\"this.className+=' transparentClass'\"></div></div></div></div><div style ='display: block; position: absolute; left : " + (70) + "px; color:" + themeColors.light + " '>" + tempNameText + "</div>";
        profilePicScrollElementView.container.setContent(tempPictureElement);
        // update the profile image on the side bar (Note this is a different html string to the create profile page)
        scrollers.SideBar.scrollElements[0].container.setContent(tempSideBarPictureElement);
        //         profilePicScrollElementView.container.setContent(tempPictureElement);

    };

    // This should reference the app users' (theUser) picture path, (it could be an imgCache.js path)
    var tempPicturePath = theUser.pictureDataUrl; //theUser.picturePath;//'./Images/user109.svg';//'./Images/displayPicTest.png';

    //TODO the onclick part for the profile picture should prompt the user to choose a new image
    // However the image is only updated (and the name isit's changed) to the server when someone presses "Done" on the profile page

    var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon3'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempPicturePath + "); background-size: 100px 100px;' onclick=\"this.className+=' transparentClass'\"></div></div></div></div>";
    //$('#activityPictureFileInput').click();
    var addPhotoText = "Add<br>Photo";
    var letterPicElement = "<div class = topLeftPosition><div class='hexagon hexagon3'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-color: #9db9e0; '><div class ='addPhotoText' style = 'font-size: 1.0em; line-height: 20px; padding-top: 17px;' onclick=\"$('#userPictureFileInput').click();\">" + addPhotoText + "</div></div></div></div></div>";

    tempProfilePhotoContentString = letterPicElement;



    var profilePicScrollElementView = new View();

    profilePicScrollElementView.container = new Surface({
        size: [undefined, 120 /*standardScrollElementHeight*/ ],
        content: tempProfilePhotoContentString,
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight + 3),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.small
        }
    });

    profilePicScrollElementView.mod = new Modifier({
        opacity: 1 //tempStartOpacity
    });

    profilePicScrollElementView.add(profilePicScrollElementView.mod).add(profilePicScrollElementView.container);
    profilePicScrollElementView.container.pipe(scrollers.Profile.scrollView);

    scrollers.Profile.scrollElements.push(profilePicScrollElementView);

    CreateGenericScrollElement("textInput", "Profile", "Full Name", null, null, null, null);

    CreateTextButton(textButtons.ProfileDone);
    theViews.SettingsPages.Profile.view.add(textButtons.ProfileDone.view);

}

function FillOutProfilePageForUser() {
    // This sets the content of the scrollElements in the "Profile" view to be for the apps user, it also means they can edit parts of it (lik etheir name and picture)
}

function FillOutProfilePageForOtherUser() {
    // This fills out the scrollElements in the "Profile" view to be that of another user, with their info (and uneditable)
}



//endregion
//====================================================-


//====================================================-
//Scroll Elements region
//|35|7241|-1253|500|500|1----------------------------------------------------------------------------------------------~

function CreateGenericScrollElement(scrollElementType, theScroller, theText, thePicture, linkedVariable, functionToRun, placeholderText, theIndexPosition, startHidden, cancelFunction, isForDark) {
    // to be able to set the scroll element type by name
    if (typeof scrollElementType === 'string') {
        scrollElementType = genericScrollElements[scrollElementType];
    }
    // to be able to set the scroller by name
    if (typeof theScroller === 'string') {
        theScroller = scrollers[theScroller];
    }
    /* this creates and returns one of the views that can be added to a scroller
 may need to store the data for each of the things somewhere? like what the text will be inside them?
if the picture part is set to 0 then it means instead of a picture there's just empty space */

    // instead of parameters it could take in an optins object which has each of those parameters as potential properties
    // then this function reads in the options object instead of th eparameters directly, so instead of theText thePicture etc it could be options.theText and options.thePicture (only for optional providable info)

    var tempScrollElementView = new View();
    var tempLineColour = themeColors.light;
    if (!theIndexPosition) {
        theIndexPosition = 0;
    }
    if (scrollElementType == genericScrollElements.scrollButton) {
        var tempFontColor = themeColors.dark;
        var tempImagePath = "Images/next15.svg";
        tempLineColour = themeColors.light;
        if (isForDark) {
            tempFontColor = themeColors.light;
            tempLineColour = themeColors.lightGrey;
        }

        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + tempLineColour
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'left',
                cursor: 'pointer',
                color: tempFontColor,
                fontSize: fontSizes.small
            }
        });

        tempScrollElementView.iconSurface = new ImageSurface({
            size: [20, 20],
            content: tempImagePath,
            properties: {}
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5]
        });

        tempScrollElementView.iconMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.4, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);

        tempScrollElementView.container.add(tempScrollElementView.iconMod).add(tempScrollElementView.iconSurface);
        if (functionToRun) {
            tempScrollElementView.container.on('click', functionToRun);
        }

    } else if (scrollElementType == genericScrollElements.scrollSpacer) {
        if (isForDark) {
            tempLineColour = themeColors.lightGrey;
        }
        var tempDividerHeight = standardScrollElementHeight - (standardScrollElementHeight * 0.3);
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, tempDividerHeight],
            //        content : "<div>Surface</div>",
            properties: {
                backgroundColor: 'rgba(175, 175, 175, 0.17)',
                lineHeight: ToPixelString(standardScrollElementHeight),
                borderBottom: "0.1em solid " + tempLineColour
            }
        });


        if (theText) {
            tempScrollElementView.textSurface = new Surface({
                size: [undefined, tempDividerHeight],
                content: theText,
                properties: {
                    lineHeight: ToPixelString(standardScrollElementHeight),
                    textAlign: 'left',
                    cursor: 'pointer',
                    color: themeColors.dark,
                }
            });

            tempScrollElementView.textMod = new StateModifier({
                origin: [0.5, 0.5],
                align: [0.5, 0.5]
            });
            tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);

        }


    } else if (scrollElementType == genericScrollElements.scrollDescription) {
        // FUTURE this could be an external function that can be reused around the app for calculating the needed height based on a width and a string (and the padding?
        //        var calculatedContainerHeight = (Math.ceil((theText.length / 62)) * defaultFontHeight) + (defaultFontHeight *2);
        var amountOfLettersOnALineTiny = calculateLetterAmountOnLine(theScroller.data.width - 2 * defaultFontHeight, GetNumberFromString(fontSizes.tiny));
        var calculatedContainerHeight = amountToIncreaseStatusHeightBy(theText.length, amountOfLettersOnALineTiny, defaultFontHeight * 1.15);
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, calculatedContainerHeight + (defaultFontHeight * 2)],
            properties: {
                backgroundColor: 'rgba(175, 175, 175, 0)',
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, true],
            content: theText,
            properties: {
                lineHeight: ToPixelString(defaultFontHeight),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.dark,
                fontSize: fontSizes.smaller,
                padding: ToPixelString(defaultFontHeight)
            }
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.5, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
    } else if (scrollElementType == genericScrollElements.accordionDescription) {
        // FUTURE this could be an external function that can be reused around the app for calculating the needed height based on a width and a string (and the padding?
        var amountOfLettersOnALineTinyLess = calculateLetterAmountOnLine(theScroller.data.width - 5 * defaultFontHeight, GetNumberFromString(fontSizes.tiny));
        var calculatedContainerHeightLessWidth = amountToIncreaseStatusHeightBy(theText.length, amountOfLettersOnALineTinyLess, defaultFontHeight * 1.15);
        tempScrollElementView.originalHeight = calculatedContainerHeightLessWidth + (2 * defaultFontHeight);
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, calculatedContainerHeightLessWidth + (2 * defaultFontHeight)],
            properties: {
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, true],
            content: theText,
            properties: {
                lineHeight: ToPixelString(defaultFontHeight),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.dark,
                fontSize: fontSizes.smaller,
                paddingTop: ToPixelString(defaultFontHeight),
                paddingBottom: ToPixelString(defaultFontHeight),
                paddingLeft: ToPixelString(defaultFontHeight * 4),
                paddingRight: ToPixelString(defaultFontHeight)
            }
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.5, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
    } else if (scrollElementType == genericScrollElements.textInput) {
        var tempTexInputOffset = 0.0;
        var textInputSize = originalWidth * 0.6;
        var textInputHeight = standardScrollElementHeight;
        var textInputXAlign = 0.2;
        var xOffset = -defaultPaddingHorizontal;
        var shouldCreateText = true;
        var tempBorderBottom = "0.1em solid " + themeColors.light;

        if (placeholderText !== null) {
            textInputSize = originalWidth - (defaultPaddingHorizontal * 2);
            textInputHeight = standardScrollElementHeight - (defaultPaddingVertical * 2);
            textInputXAlign = 0;
            shouldCreateText = false;
            tempBorderBottom = "none";
            xOffset = 0;
        }

        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: tempBorderBottom
            }
        });

        if (shouldCreateText === true) {
            tempScrollElementView.textSurface = new Surface({
                size: [undefined, standardScrollElementHeight],
                content: theText,
                properties: {
                    lineHeight: ToPixelString(textInputHeight),
                    textAlign: 'left',
                    cursor: 'pointer',
                    color: themeColors.lightGrey,
                    fontSize: fontSizes.small
                }
            });
        }
        tempScrollElementView.textInput = new InputSurface({
            size: [textInputSize, standardScrollElementHeight - (defaultPaddingVertical * 2)],
            placeholder: placeholderText,
            type: 'text',
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight - (defaultPaddingVertical * 2)),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.grey,
                fontSize: fontSizes.small
            }
        });
        // FUTURE these parts are reliant on having the same text enetered here and in the creator for the actual text input, a better way to do it could be to link it up without require the exact text to be here?
        if (placeholderText === "Event Name") {
            // on focus (set the contactsLogic.textIsFocused to true)
            tempScrollElementView.textInput.on('focus', function () {
                activityTitleIsFocused = true;
            });
            //on blur (set the contactsLogic.textIsFocused to false)
            tempScrollElementView.textInput.on('blur', function () {
                activityTitleIsFocused = false;
            });
        } else if (placeholderText === "Add a location...") {
            // on focus (set the contactsLogic.textIsFocused to true)
            tempScrollElementView.textInput.on('focus', function () {
                wherePartIsFocused = true;
            });
            //on blur (set the contactsLogic.textIsFocused to false)
            tempScrollElementView.textInput.on('blur', function () {
                wherePartIsFocused = false;
            });
        }

        tempScrollElementView.textInputMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [textInputXAlign, 0.5],
            transform: Transform.translate(xOffset, 0, 0)
        });
        tempScrollElementView.container.add(tempScrollElementView.textInputMod).add(tempScrollElementView.textInput);
        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5]
        });
        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);

        if (functionToRun) {
            tempScrollElementView.container.on('click', functionToRun);
        }

    } else if (scrollElementType == genericScrollElements.checkbox) {
        var tempTextOffset = 0;
        var hasGotPicture = false;
        if (thePicture !== null) {
            tempTextOffset = defaultIconSize;
        }

        tempScrollElementView.isTicked = false;

        if (linkedVariable) {
            tempScrollElementView.linkedVariable = linkedVariable;
        }

        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.dark,
                fontSize: fontSizes.small
            }
        });

        tempScrollElementView.iconSurface = new ImageSurface({
            size: [defaultIconSize, defaultIconSize],
            content: "./Images/checkBoxEmpty.svg",
            properties: {}
        });
        tempScrollElementView.iconSurface.on('click', function () {
            tempScrollElementView.isTicked = !tempScrollElementView.isTicked;
            linkedVariable.isSelected = tempScrollElementView.isTicked;

            // FIXME this has still gotta set the variable it's linked to to true
            tickACheckBox(theScroller, tempScrollElementView.isTicked, tempScrollElementView.linkedVariable);
            //            linkedVariable.isSelected = true;
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5],
            transform: Transform.translate(tempTextOffset, 0, 0)
        });

        tempScrollElementView.iconMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.4, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
        if (hasGotPicture === true) {
            tempScrollElementView.container.add(tempScrollElementView.imageMod).add(tempScrollElementView.imageSurface);
        }
        tempScrollElementView.container.add(tempScrollElementView.iconMod).add(tempScrollElementView.iconSurface);
        if (functionToRun) {
            tempScrollElementView.container.on('click', functionToRun);
        }


    } else if (scrollElementType == genericScrollElements.pictureCheckbox) {

    } else if (scrollElementType == genericScrollElements.scrollTitle) {
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.lightGrey,
                fontSize: fontSizes.small
            }
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
        if (functionToRun) {
            tempScrollElementView.container.on('click', functionToRun);
        }


    } else if (scrollElementType == genericScrollElements.numberInput) {
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.lightGrey,
                fontSize: fontSizes.small
            }
        });

        var tempAttributes = {};

        if (theScroller === scrollers.Privacy) {
            tempAttributes = {
                min: 0,
                max: 7
            };

        }

        tempScrollElementView.numberInput = new InputSurface({
            size: [defaultFontHeight * 5, standardScrollElementHeight - (defaultPaddingVertical * 2)],
            type: 'number',
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight - (defaultPaddingVertical * 2)),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.lightGrey,
                fontSize: fontSizes.medium
            },
            attributes: tempAttributes
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5]
        });

        tempScrollElementView.numberMod = new StateModifier({
            origin: [1, 0.5],
            align: [0.5, 0.5],
            transform: Transform.translate(-defaultPaddingHorizontal, 0, 0)
        });

        if (theScroller === scrollers.Privacy) {
            tempScrollElementView.numberInput.on('blur', function () {
                temporaryActivityObject.deletionDays = tempScrollElementView.numberInput.getValue();
            });
        }

        if (theScroller === scrollers.Reach) {
            tempScrollElementView.numberInput.on('blur', function () {
                temporaryActivityObject.target = tempScrollElementView.numberInput.getValue();
            });
        }

        tempScrollElementView.container.add(tempScrollElementView.numberMod).add(tempScrollElementView.numberInput);

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
        if (functionToRun) {
            tempScrollElementView.container.on('click', functionToRun);
        }
    } else if (scrollElementType == genericScrollElements.bullet) {

    } else if (scrollElementType == genericScrollElements.textArea) {

        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight * 7],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                color: 'black'
            }
        });

        var tempPlaceholder2 = "Enter the text here";
        if (placeholderText) {
            tempPlaceholder2 = placeholderText;
        }

        tempScrollElementView.textArea = new TextareaSurface({
            content: "Surface for input",
            placeholder: tempPlaceholder2,
            type: 'text',
            size: [originalWidth - (defaultPaddingHorizontal * 2), undefined],
            properties: {
                textAlign: "left",
                fontSize: fontSizes.small

            }
        });

        tempScrollElementView.textAreaMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.textAreaMod).add(tempScrollElementView.textArea);

    } else if (scrollElementType == genericScrollElements.addAPicture) {

    } else if (scrollElementType == genericScrollElements.textAccordion) {
        // This whole thing isn't used anymore since it's easier to just have it be made in its own function
        tempScrollElementView.childrenHidden = false;

        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.dark,
                fontSize: fontSizes.small
            }
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5],
            transform: Transform.translate(defaultIconSize, 0, 0)
        });

        tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
        if (functionToRun) {
            tempScrollElementView.container.on('click', functionToRun);
        }

    } else if (scrollElementType == genericScrollElements.groupAccordion) {
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.dark,
                fontSize: fontSizes.small
            }
        });

        tempScrollElementView.textMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.55, 0.5],
            transform: Transform.translate(defaultIconSize, 0, 0)
        });
    } else if (scrollElementType == genericScrollElements.cancelOk) {
        tempScrollElementView.container = new ContainerSurface({
            size: [undefined, standardScrollElementHeight],
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight),
                textAlign: 'center',
                cursor: 'pointer',
                borderBottom: "0.1em solid " + themeColors.light
            }
        });

        tempScrollElementView.cancelSurface = new Surface({
            size: [theScroller.data.width * 0.5, standardScrollElementHeight],
            content: "Cancel",
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'center',
                cursor: 'pointer',
                color: themeColors.main,
                fontSize: fontSizes.small
            }
        });

        tempScrollElementView.okSurface = new Surface({
            size: [theScroller.data.width * 0.5, standardScrollElementHeight],
            content: "Ok",
            properties: {
                lineHeight: ToPixelString(standardScrollElementHeight + 3),
                textAlign: 'center',
                cursor: 'pointer',
                color: themeColors.main,
                fontSize: fontSizes.small,
                borderLeft: "1px solid " + themeColors.greyHighlight
            }
        });

        tempScrollElementView.iconSurface = new ImageSurface({
            size: [20, 20],
            content: "Images/next15.svg",
            properties: {}
        });

        tempScrollElementView.cancelMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.25, 0.5]
        });

        tempScrollElementView.okMod = new StateModifier({
            origin: [0.5, 0.5],
            align: [0.75, 0.5]
        });

        tempScrollElementView.container.add(tempScrollElementView.cancelMod).add(tempScrollElementView.cancelSurface);
        tempScrollElementView.container.add(tempScrollElementView.okMod).add(tempScrollElementView.okSurface);

        if (functionToRun) {
            tempScrollElementView.okSurface.on('click', functionToRun);
        }
        if (cancelFunction) {
            tempScrollElementView.cancelSurface.on('click', cancelFunction);
        }
    }

    var tempStartOpacity = 1;
    if (startHidden) {
        tempStartOpacity = 0;
        tempScrollElementView.container.setSize([undefined, 0]);
    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    theScroller.scrollElements.push(tempScrollElementView);
    //    theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
}

// A loop will create these and put the looped index number as the textAccordionId (so each acordion has its own id
function CreateTextAccordion(textAccordionId, theScroller, theText, theIndexPosition) {
    // to be able to set the scroller by name
    if (typeof theScroller === 'string') {
        theScroller = scrollers[theScroller];
    }
    
    var tempScrollElementView = new View();
    var tempLineColour = themeColors.light;

    tempScrollElementView.childrenHidden = false;
    // all the text accordions only hide one thing under them
    //        tempScrollElementView

    tempScrollElementView.id = textAccordionId;

    tempScrollElementView.container = new ContainerSurface({
        size: [undefined, standardScrollElementHeight],
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            textAlign: 'center',
            cursor: 'pointer',
            borderBottom: "0.1em solid " + themeColors.light
        }
    });

    tempScrollElementView.textSurface = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: theText,
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight + 3),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.small
        }
    });

    tempScrollElementView.textMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.55, 0.5],
        transform: Transform.translate(defaultIconSize, 0, 0)
    });

    tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);
    tempScrollElementView.container.on('click', function () {
        if (tempScrollElementView.childrenHidden === false) {
            hideAnAccordion(textAccordionId, theScroller);
        } else {
            showAnAccordion(textAccordionId, theScroller);
        }
    });

    var tempStartOpacity = 1;
    //    if (startHidden) {
    //        tempStartOpacity = 0;
    //        tempScrollElementView.container.setSize([undefined, 0]);
    //    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    //    theScroller.scrollElements.push(tempScrollElementView);
    if (theIndexPosition !== undefined) {
        theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
    } else {
        theScroller.scrollElements.push(tempScrollElementView);
    }

}
// these are for the contacts groups
function CreateGroupAccordionScrollElement(groupId, theScroller, theIndexPosition) {
    // to be able to set the scroller by name
    if (typeof theScroller === 'string') {
        theScroller = scrollers[theScroller];
    }
    
    var tempScrollElementView = new View();

    var theGroupObject = inAppDatabases.groups({
        groupId: groupId
    }).first();
    var groupsContacts = inAppDatabases.groupConnections({
        groupId: groupId
    }).get();

    tempScrollElementView.groupsContacts = groupsContacts;
    //


    var tempNameText = theGroupObject.groupName;
    var tempPicturePath = './Images/user109.svg'; //'./Images/displayPicTest.png';
    //        if (relatedMobilizrUser) {
    //            tempNameText = relatedMobilizrUser.groupName;
    //            tempPicturePath = relatedMobilizrUser.picture;//'./Images/displayPicTest.png';
    //        }

    tempScrollElementView.groupId = groupId;
    tempScrollElementView.childrenHidden = false;
    tempScrollElementView.name = tempNameText.toLowerCase();
    tempScrollElementView.searchId = groupId;

    var tempTextOffset = 0;

    if (!theIndexPosition) {
        theIndexPosition = 0;
    }

    ///////////////////////////////////////////

    var onclickPart = "onmousedown='pressAGroupCheckBox(" + groupId + ");'";
    //        var onclickPart = "onclick='

    var tempEditButtonElement = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxEmpty.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    // If the edit button needs to change when it's pressed, then instead of setting the whole html content in one go, the tickAGroupCheckBox could run a function inside the group scroll element that returns the correct html string based on which images it should show (based on the edit button's state and if the group is ticked)

    var tempCheckboxElement = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxEmpty.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    var tempCheckboxElementTicked = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxTicked.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    var tempNameElement = "<div class = 'shiftContactText' style = 'width:100% ; ' >" + tempNameText + tempCheckboxElement + "</div>";

    var tempNameElementTicked = "<div class = 'shiftContactText' style = 'width:100%; color : " + themeColors.main + ";' >" + tempNameText + tempCheckboxElementTicked + "</div>";
    //        var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url("+ tempPicturePath +"); background-size: 46px 46px;'></div></div></div></div>";


    //style = ' right: 0px;'><img src = './Images/checkBoxTicked.svg' style = 'position:absolute; right: 0px; display:inline;'

    // check if it is selected if it is then, show it, if it isn't selected then hide it (switch to the appropriate class)

    var bottomLine = '<div style="width: ' + (originalWidth - 65) + 'px; height: 1.4px; background: ' + themeColors.light + '; overflow: hidden; float:right;"></div>';

    tempScrollElementView.tempContentElement = tempNameElement + bottomLine;

    tempScrollElementView.tempContentElementTicked = tempNameElementTicked + bottomLine;

    tempScrollElementView.isTicked = false;

    /////////////////////////////

    //        var tempNameElement = "<div class = 'shiftContactText'>" + tempNameText + "</div>";

    //        var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url("+ tempPicturePath +"); background-size: 46px 46px;'></div></div></div></div>";

    // this will edit the inAppDataBase contact directly
    //        if ( linkedVariable) {
    //            tempScrollElementView.linkedVariable = linkedVariable;
    //        }


    tempScrollElementView.container = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: tempScrollElementView.tempContentElement,
        properties: {
            //            backgroundColor: themeColors.lightGlass,
            lineHeight: ToPixelString(standardScrollElementHeight),
            //                textAlign: 'center',
            cursor: 'pointer',
            //            color: 'black'
            //                color: themeColors.light
        }
    });

    tempScrollElementView.container.on('click', function () {

        if (tempScrollElementView.childrenHidden === false) {
            hideAGroup(groupId, scrollers.ContactsGroups);
        } else {
            showAGroup(groupId, scrollers.ContactsGroups);
        }
    });

    //        if (functionToRun) {
    // open the profile for the selected contact
    //tempScrollElementView.container.on('click', functionToRun);
    //        }
    var tempStartOpacity = 1;
    //        if (startHidden) {
    //            tempStartOpacity = 0;
    //            tempScrollElementView.container.setSize([undefined, 0]);
    //        }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    theScroller.scrollElements.push(tempScrollElementView);
    //        theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);

    for (var i = 0; i < groupsContacts.length; i++) {
        CreateContactScrollElement(groupsContacts[i].includesContact, theScroller, null, false);
    }
}

function CreateGenericCheckboxDescription(theText, theScroller, theName) {
    // to be able to set the scroller by name
    if (typeof theScroller === 'string') {
        theScroller = scrollers[theScroller];
    }
    
    var tempScrollElementView = new View();
    //    var tempLineColour = themeColors.light;
    //    if (!theIndexPosition) {
    //        theIndexPosition = 0;
    //    }

    tempScrollElementView.type = "genericCheckboxDescription";
    tempScrollElementView.name = theName.toLowerCase();

    //        var calculatedContainerHeight = (Math.ceil((theText.length / 62)) * defaultFontHeight) + (defaultFontHeight *2);
    var amountOfLettersOnALineTiny = calculateLetterAmountOnLine(theScroller.data.width - 2 * defaultFontHeight, GetNumberFromString(fontSizes.tiny));
    var calculatedContainerHeight = amountToIncreaseStatusHeightBy(theText.length, amountOfLettersOnALineTiny, defaultFontHeight * 1.15);
    tempScrollElementView.container = new ContainerSurface({
        size: [undefined, calculatedContainerHeight + (defaultFontHeight * 2)],
        properties: {
            backgroundColor: 'rgba(175, 175, 175, 0.17)',
            textAlign: 'center',
            cursor: 'pointer',
            borderBottom: "0.1em solid " + themeColors.light
        }
    });

    tempScrollElementView.textSurface = new Surface({
        size: [undefined, true],
        content: theText,
        properties: {
            lineHeight: ToPixelString(defaultFontHeight),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.smaller,
            padding: ToPixelString(defaultFontHeight)
        }
    });

    tempScrollElementView.textMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.5, 0.5]
    });

    tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);



    var tempStartOpacity = 1;
    //    if (startHidden) {
    //        tempStartOpacity = 0;
    //        tempScrollElementView.container.setSize([undefined, 0]);
    //    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    theScroller.scrollElements.push(tempScrollElementView);
    //    theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
}

function CreateGenericCheckBox(theText, theScroller, theIndexPosition, isNotABullet, startHidden) {
    // this creates a a tickable checkbox scrollElement with some text, each checkbox scrollElement has an "isTicked?" part, a scrollElement type part and also a scrollElement value part, these are all used when checking through a scrollView for which of the checkboxes are ticked, and when it finds one, it can take noe of the value of that checkbox
    
    // to be able to set the scroller by name
    if (typeof theScroller === 'string') {
        theScroller = scrollers[theScroller];
    }
    
    var tempScrollElementView = new View();

    var tempNameText = theText;

    var tempIsNotABulletPart = false;

    tempScrollElementView.isTicked = false;
    tempScrollElementView.name = tempNameText.toLowerCase();
    tempScrollElementView.type = "genericCheckbox";
    if (isNotABullet && isNotABullet === true) {
        tempIsNotABulletPart = isNotABullet;
        tempScrollElementView.type = "genericCheckboxMultiple";
    }

    var tempTextOffset = 0;

    if (!theIndexPosition) {
        theIndexPosition = 0;
    }

    // the onclick can be a closure that takes in the object in the brackets?
    var onclickPart = "onmousedown='pressAGenericCheckBox(\"" + tempScrollElementView.name + "\",\"" + getScrollerNameFromObject(theScroller) + "\", " + tempIsNotABulletPart + ");'";
    //
    //

    var tempCheckboxElement = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxEmpty.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    var tempCheckboxElementTicked = "<span style='background-color:black' " + onclickPart + "><img src = './Images/checkBoxTicked.svg' style='position:absolute; right:0px ; top:20%;' ></span>";

    var tempNameElement = "<div style = 'width:100%; text-align: left; margin-left: 15px;' >" + tempNameText + tempCheckboxElement + "</div>";

    var tempNameElementTicked = "<div style = 'width:100%; text-align: left; margin-left: 15px; color : " + themeColors.main + ";' >" + tempNameText + tempCheckboxElementTicked + "</div>";

    //        var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon1'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url("+ tempPicturePath +"); background-size: 46px 46px;'></div></div></div></div>";


    //style = ' right: 0px;'><img src = './Images/checkBoxTicked.svg' style = 'position:absolute; right: 0px; display:inline;'

    // check if it is selected if it is then, show it, if it isn't selected then hide it (switch to the appropriate class)

    var bottomLine = '<div style="width: ' + (originalWidth - 15) + 'px; height: 1.4px; background: ' + themeColors.light + '; overflow: hidden; float:right;"></div>';

    tempScrollElementView.tempContentElement = /*tempPictureElement +*/ tempNameElement + bottomLine;

    tempScrollElementView.tempContentElementTicked = /*tempPictureElement +*/ tempNameElementTicked + bottomLine;

    tempScrollElementView.isTicked = false;

    // this will edit the inAppDataBase contact directly
    //        if ( linkedVariable) {
    //            tempScrollElementView.linkedVariable = linkedVariable;
    //        }

    tempScrollElementView.container = new Surface({
        size: [undefined, standardScrollElementHeight],
        content: tempScrollElementView.tempContentElement,
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            cursor: 'pointer',
        }
    });

    // open the profile for the selected contact
    //tempScrollElementView.container.on('click', functionToRun);
    //        }
    var tempStartOpacity = 1;
    if (startHidden) {
        tempStartOpacity = 0;
        tempScrollElementView.container.setSize([undefined, 0]);
    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    //    theScroller.scrollElements.push(tempScrollElementView);
    if (theIndexPosition !== 0) {
        theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);
    } else {
        theScroller.scrollElements.push(tempScrollElementView);
    }
}

function CreateDateInputScrollElement(theText, theScroller) {
    // to be able to set the scroller by name
    if (typeof theScroller === 'string') {
        theScroller = scrollers[theScroller];
    }
    
    var tempScrollElementView = new View();

    var tempTexInputOffset = 0.0;
    var textInputSize = originalWidth * 0.75;
    var textInputHeight = standardScrollElementHeight;
    var textInputXAlign = 0.2;
    var xOffset = -defaultPaddingHorizontal;
    var shouldCreateText = true;
    var tempBorderBottom = "0.1em solid " + themeColors.light;
    /*
        if (placeholderText !== null) {
            textInputSize = originalWidth - (defaultPaddingHorizontal * 2);
            textInputHeight = standardScrollElementHeight - (defaultPaddingVertical * 2);
            textInputXAlign = 0;
            shouldCreateText = false;
            tempBorderBottom = "none";
            xOffset = 0;
        }
    */
    tempScrollElementView.container = new ContainerSurface({
        size: [undefined, standardScrollElementHeight],
        properties: {
            lineHeight: ToPixelString(standardScrollElementHeight),
            textAlign: 'center',
            cursor: 'pointer',
            borderBottom: tempBorderBottom
        }
    });

    if (shouldCreateText === true) {
        tempScrollElementView.textSurface = new Surface({
            size: [undefined, standardScrollElementHeight],
            content: theText,
            properties: {
                lineHeight: ToPixelString(textInputHeight),
                textAlign: 'left',
                cursor: 'pointer',
                color: themeColors.lightGrey,
                fontSize: fontSizes.small
            }
        });
    }
    tempScrollElementView.textInput = new Surface({
        size: [textInputSize, standardScrollElementHeight - (defaultPaddingVertical * 2)],
        //        placeholder: "Select a date",
        content: "Select a date",
        //        type: 'text',
        //        attributes: {
        //            readOnly: true
        //        },
        properties: {
            overflow: 'hidden',
            lineHeight: ToPixelString(standardScrollElementHeight - (defaultPaddingVertical * 2)),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.grey,
            fontSize: fontSizes.small,
            backgroundColor: '#f1f1f1'
        }
    });

    tempScrollElementView.textInputHighlight = new Surface({
        size: [textInputSize, standardScrollElementHeight - (defaultPaddingVertical * 2)],
        //        placeholder: "Select a date",
        content: "Select a date",
        //        type: 'text',
        //        attributes: {
        //            readOnly: true
        //        },
        properties: {
            overflow: 'hidden',
            lineHeight: ToPixelString(standardScrollElementHeight - (defaultPaddingVertical * 2)),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.grey,
            fontSize: fontSizes.small,
            backgroundColor: '#e7ffe5'
        }
    });

    // FUTURE these parts are reliant on hacing the same text enetered here and in the creator for the actual text input, a better way to do it could be to link it up without require the exact text to be here?

    // a fake focus thing, when the surface is clicked use the render controller to change it, and also set it as being in focus (stored within the actual scrollElement
    // The check for which of the two are in focus can be done outside here, so the comparsion of the "theText" string doesn't need to happen

    tempScrollElementView.textInputMod = new StateModifier({
        origin: [1, 0.5],
        align: [1, 0.5],
        transform: Transform.translate(xOffset, 0, 0)
    });


    tempScrollElementView.textInputModHighlight = new StateModifier({
        origin: [1, 0.5],
        align: [1, 0.5],
        opacity: 0,
        transform: Transform.translate(xOffset, 0, 0)
    });

    tempScrollElementView.container.add(tempScrollElementView.textInputMod).add(tempScrollElementView.textInput);

    tempScrollElementView.container.add(tempScrollElementView.textInputModHighlight).add(tempScrollElementView.textInputHighlight);

    tempScrollElementView.isFocused = false;

    tempScrollElementView.setFocused = function () {
        tempScrollElementView.isFocused = true;
        tempScrollElementView.textInputModHighlight.setOpacity(1, easeTransitionSlow /*, callback*/ );
        datePickerBrick.MoveBrickUp();

    };

    tempScrollElementView.setBlurred = function () {
        datePickerBrick.HideBrick();
        tempScrollElementView.textInputModHighlight.setOpacity(0, easeTransitionSlow /*, callback*/ );
        tempScrollElementView.isFocused = false;
    };

    tempScrollElementView.textInputHighlight.on('click', function () {
        if (tempScrollElementView.isFocused === false) {

            //FUTURE this is a bit hacky, it can't be reused however in the app it's only used here, but in the future it could be better to make it a more generic thing (perhaps with an external function that loops through focuses, or blurs the correct "date picking" surfaces
            // also gotta check the theText of this one
            if (theText === "Starts") {
                if (scrollers.When.scrollElements[1].isFocused === true) {
                    scrollers.When.scrollElements[1].setBlurred();
                }
            } else { // theText == "Ends"
                if (scrollers.When.scrollElements[0].isFocused === true) {
                    scrollers.When.scrollElements[0].setBlurred();
                }
            }
            // this happens after the other one is blurred so the date picker dissapear then reapper, instead of appearing then dissappearing
            tempScrollElementView.setFocused();

        } else {
            tempScrollElementView.setBlurred();
        }
    });

    tempScrollElementView.textMod = new StateModifier({
        origin: [0.5, 0.5],
        align: [0.55, 0.5]
    });
    tempScrollElementView.container.add(tempScrollElementView.textMod).add(tempScrollElementView.textSurface);

    //    if (functionToRun) {
    //        tempScrollElementView.container.on('click', functionToRun);
    //    }

    var tempStartOpacity = 1;
    //    if (startHidden) {
    //        tempStartOpacity = 0;
    //        tempScrollElementView.container.setSize([undefined, 0]);
    //    }

    tempScrollElementView.mod = new Modifier({
        opacity: tempStartOpacity
    });

    tempScrollElementView.add(tempScrollElementView.mod).add(tempScrollElementView.container);

    // this part could possibly be seperated into a different function (so the main function just returns a view)
    // it could mean that the scrollElements could be created through a different function that doesnt neccesarily want the scrollElement element to be added to a scroller directly straight away (like the sections that list contacts)
    tempScrollElementView.container.pipe(theScroller.scrollView);
    theScroller.scrollElements.push(tempScrollElementView);
    //    theScroller.scrollElements.splice(theIndexPosition, 0, tempScrollElementView);


}

function CreateScrollbarProfileScrollElement() {
    var tempPicturePath = temporaryUserObject.pictureDataUrl;
    //     var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon3'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-image: url(" + tempPicturePath + "); background-size: 100px 100px;' onclick=\"this.className+=' transparentClass'\"></div></div></div></div>";

    var addPhotoText = "Add<br>Photo";
    var tempPictureElement = "<div class = topLeftPosition><div class='hexagon hexagon3'><div class='hexagon-in1'><div class='hexagon-in2' style = 'background-color: #9db9e0; '><div class ='addPhotoText' style = 'font-size: 1.0em; line-height: 20px; padding-top: 17px;' onclick=\"$('#userPictureFileInput').click();\">" + addPhotoText + "</div></div></div></div></div>";



    var profilePicScrollElementView = new View();

    profilePicScrollElementView.container = new Surface({
        size: [undefined, 120 /*standardScrollElementHeight*/ ],
        content: tempPictureElement,
        properties: {
            //             lineHeight: ToPixelString(standardScrollElementHeight + 3),
            textAlign: 'left',
            cursor: 'pointer',
            color: themeColors.dark,
            fontSize: fontSizes.small
        }
    });

    profilePicScrollElementView.mod = new Modifier({
        opacity: 1 //tempStartOpacity
    });

    //   profilePicScrollElementView.container.setContent(tempPictureElement);

    profilePicScrollElementView.add(profilePicScrollElementView.mod).add(profilePicScrollElementView.container);
    profilePicScrollElementView.container.pipe(scrollers.SideBar.scrollView);

    scrollers.SideBar.scrollElements.push(profilePicScrollElementView);
}




//endregion
//====================================================-

/////////////////////////////////////////////////////////////////////////////////////////////////////////
// Starting the app
/////////////////////////////////////////////////////////////////////////////////////////////////////////

//====================================================-
//Creating region
//|36|9606|728|827.67|513.48|1----------------------------------------------------------------------------------------------~

// create the main context
var mainContext = Engine.createContext();
var mainRenderController = new RenderController();

var settingsRenderController = new RenderController();

CreateScreenDisabler();
CreateBrick();



doThisForAll(scrollers, setDefaultScrollerData);

doThisForAll(theViews.SignUpPages, CreateNewViewAsChild);
doThisForAll(theViews.SettingsPages, CreateNewViewAsChild);
doThisForAll(scrollers, CreateNewScrollView);
doThisForAll(appSections, CreateNewView);
doThisForAll(theViews.ActivityPages, CreateNewBasicViewAsChild);
doThisForAll(theViews.IntroPanelsPanels, CreateNewBasicViewAsChild);

doThisForAll(draggables.MainPages.views, CreateSwipablePage);
doThisForAll(draggables.CreateActivity.views, CreateSwipablePage);
doThisForAll(draggables.ActivityUpdates.views, CreateSwipablePage);
doThisForAll(draggables.ActivitySmallBox.views, CreateSwipablePage);
doThisForAll(draggables.ContactsPages.views, CreateSwipablePage);
doThisForAll(draggables.PresetStatusPages.views, CreateSwipablePage);
doThisForAll(draggables.IntroPanelsPanels.views, CreateSwipablePage);

doThisForAll(draggables, CreateHorizontalDraggable);
CreateHorizontalDraggableOld(draggables, "ActivitySmallBox");
CreateHorizontalDraggableOld(draggables, "SideBar");

CreatePictureButton(pictureButtons.AddActivity);
CreateTextButton(textButtons.Cancel);

CreateTabsBar(tabsBars.ActivityUpdates, draggables.ActivityUpdates);
CreateTabsBar(tabsBars.Contacts, draggables.ContactsPages);
CreateTabsBar(tabsBars.PresetStatusPages, draggables.PresetStatusPages);

CreateDotGroup(dotGroups.MainPages, draggables.MainPages, false);
CreateDotGroup(dotGroups.CreateActivity, draggables.CreateActivity, false);
CreateDotGroup(dotGroups.ActivityUpdates, draggables.ActivityUpdates, true);
CreateDotGroup(dotGroups.Contacts, draggables.ContactsPages, true);
CreateDotGroup(dotGroups.PresetStatusPages, draggables.PresetStatusPages, true);
CreateDotGroup(dotGroups.IntroPanelsPanels, draggables.IntroPanelsPanels, false);

// this function is now required otherwise the draggables pages don't have an isDisabled value set for them
setNewLockedLimit(draggables.CreateActivity, draggables.CreateActivity.data.lockedPageLimit);
setNewLockedLimit(draggables.ActivityUpdates, draggables.ActivityUpdates.data.lockedPageLimit);
setNewLockedLimit(draggables.ContactsPages, draggables.ContactsPages.data.lockedPageLimit);
setNewLockedLimit(draggables.PresetStatusPages, draggables.PresetStatusPages.data.lockedPageLimit);
setNewLockedLimit(draggables.IntroPanelsPanels, draggables.IntroPanelsPanels.data.lockedPageLimit);

// The create text will be a return thing instead, so potentially all of the titles can be set declaratively? instead of a loop? or not, or it could use the object loop and set the title text for the currently looped view based on the views name? or something not sure no
doThisForAll(titleTexts.MainPages, CreateTitleText);
doThisForAll(titleTexts.CreateActivity, CreateTitleText);
doThisForAll(titleTexts.SettingsPages, CreateTitleText);
doThisForAll(titleTexts.SignUpPages, CreateTitleText);

CreatePictureButton(pictureButtons.Menu);
CreatePictureButton(pictureButtons.Submit);
pictureButtons.Submit.view.SetDisabled();
CreatePictureButton(pictureButtons.ThreeDots);

// All of these can probably be created in one go and added to the one view
var sideMenuContainer = new ContainerSurface({
    size: [sideBarWidth, originalHeight],
    properties: {
        backgroundColor: 'rgba(100, 100, 100, 0.95)',
        textAlign: 'center',
        cursor: 'all-scroll',
    }
});
sideMenuContainer.pipe(draggables.SideBar.draggable);

CreateActivityContainers();
CreateContactsView();
CreateDeclinedView();
CreateSignupPages();
AddButtonsToSettingsPages();
CreateProfilePageElements();
CreateDatePickerBrick(datePickerBrick);
CreateScrollbarProfileScrollElement();






//endregion
//====================================================-


//====================================================-
//Adding region
//|44|9628|1427|946.99|534.38|1----------------------------------------------------------------------------------------------~
var appViewModifier = new StateModifier({
    size: [originalWidth, originalHeight],
    origin: [0.5, 0],
    align: [0.5, 0]
});

var mainNode = new RenderNode(appViewModifier);

mainNode.add(mainRenderController);

mainContext.add(mainNode);
mainContext.add(screenDisabler.renderController);
mainContext.add(theBrick.modifier).add(theBrick.container);
screenDisabler.renderController.show(screenDisabler.view);
mainRenderController.show(appSections.MainPages);

//mainRenderController.show(theViews.SignUpPages.IntroPanels.view);

doThisForAll(theViews.MainPages, addTitleTexts);
doThisForAll(theViews.CreateActivity, addTitleTexts);
doThisForAll(theViews.SettingsPages, addTitleTexts);
doThisForAll(theViews.SignUpPages, addTitleTexts);

appSections.CreateActivity.add(textButtons.Cancel.view);

appSections.CreateActivity.add(datePickerBrick.modifier).add(datePickerBrick.container);

CreateTextButton(textButtons.createSubmit);
setTextButtonColor(textButtons.createSubmit, themeColors.lightGrey);
appSections.CreateActivity.add(textButtons.createSubmit.view);

appSections.MainPages.add(pictureButtons.AddActivity.view);
appSections.MainPages.add(pictureButtons.Menu.view);

appSections.Settings.add(settingsRenderController);
appSections.Settings.add(pictureButtons.Menu.view);
settingsRenderController.show(theViews.SettingsPages.Settings.view);

activitySmallBoxMovementLogic([draggables.SideBar, draggables.ActivitySmallBox]);
draggablePagesMovementLogic([draggables.MainPages, draggables.CreateActivity, draggables.ActivityUpdates, draggables.ContactsPages, draggables.PresetStatusPages, draggables.IntroPanelsPanels]);

//PreventDraggableWhileScrolling(draggables.MainPages);
//PreventDraggableWhileScrolling(draggables.SideBar);
//PreventDraggableWhileScrolling(draggables.ActivityUpdates);
//PreventDraggableWhileScrolling(draggables.ContactsPages);
//PreventDraggableWhileScrolling(draggables.PresetStatusPages);



// Adding static things to the scrollers

//     SideBar region
CreateGenericScrollElement("scrollButton", "SideBar", "Give Feedback", null, null, pictureButtons.AddActivity.data.pressedFunction, null, null, null, null, true);

CreateGenericScrollElement("scrollButton", "SideBar", "Settings", null, null, function () {
    draggables.SideBar.data.currentPage = 1;
    goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
    settingsRenderController.show(theViews.SettingsPages.Settings.view);
    mainRenderController.show(appSections.Settings);
}, null, null, null, null, true);

CreateGenericScrollElement("scrollSpacer", "SideBar", null, null, null, null, null, null, null, null, true);
CreateGenericScrollElement("scrollButton", "SideBar", "Declined", null, null, function () {
    draggables.SideBar.data.currentPage = 1;
    goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
    mainRenderController.hide();
    theBrick.SetBrickContent(theViews.BrickContents.Declined);
    theBrick.MoveBrickUp(originalHeight);
}, null, null, null, null, true);
CreateGenericScrollElement("scrollButton", "SideBar", "Contacts", null, null, function () {
    draggables.SideBar.data.currentPage = 1;
    goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
    mainRenderController.hide();
    theBrick.SetBrickContent(theViews.BrickContents.Contacts);
    theBrick.MoveBrickUp(originalHeight);
    contactsLogic.isLookingAtContacts = true;
    // Check if the mini bricks should be shown again (since they're not attached to the contacts brick container
    if (contactsLogic.aContactIsSelected === true) {
        contactsLogic.TurnToEditAGroupMode();
    }

}, null, null, null, null, true);
CreateGenericScrollElement("scrollSpacer", "SideBar", null, null, null, null, null, null, null, null, true);

CreateGenericScrollElement("scrollButton", "SideBar", "Created", null, null, function () {
    // this could have a check added to see if the brick is up first? it could even be put into the theBrick.HideBrick function
    mainRenderController.show(appSections.MainPages);
    theBrick.HideBrick();
    activityContainers.UpdatesBrickThing.HideBrick();
    if (draggables.SideBar.data.currentPage === 0) {
        draggables.SideBar.data.currentPage = 1;
        goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
        draggables.MainPages.data.currentPage = 3;
        goToThisPage(draggables.MainPages, draggables.MainPages.data.currentPage, true);
    }

}, null, null, null, null, true);
CreateGenericScrollElement("scrollButton", "SideBar", "Joined", null, null, function () {
    // this could have a check added to see if the brick is up first? it could even be put into the theBrick.HideBrick function
    mainRenderController.show(appSections.MainPages);
    theBrick.HideBrick();
    activityContainers.UpdatesBrickThing.HideBrick();
    if (draggables.SideBar.data.currentPage === 0) {
        draggables.SideBar.data.currentPage = 1;
        goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
        draggables.MainPages.data.currentPage = 2;
        goToThisPage(draggables.MainPages, draggables.MainPages.data.currentPage, true);
    }

}, null, null, null, null, true);
CreateGenericScrollElement("scrollButton", "SideBar", "Invites", null, null, function () {
    // this could have a check added to see if the brick is up first? it could even be put into the theBrick.HideBrick function
    mainRenderController.show(appSections.MainPages);
    theBrick.HideBrick();
    activityContainers.UpdatesBrickThing.HideBrick();
    if (draggables.SideBar.data.currentPage === 0) {
        draggables.SideBar.data.currentPage = 1;
        goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
        draggables.MainPages.data.currentPage = 1;
        goToThisPage(draggables.MainPages, draggables.MainPages.data.currentPage, true);
    }

}, null, null, null, null, true);
CreateGenericScrollElement("scrollButton", "SideBar", "Updates", null, null, function () {
    // this could have a check added to see if the brick is up first? it could even be put into the theBrick.HideBrick function
    mainRenderController.show(appSections.MainPages);
    theBrick.HideBrick();
    activityContainers.UpdatesBrickThing.HideBrick();
    if (draggables.SideBar.data.currentPage === 0) {
        draggables.SideBar.data.currentPage = 1;
        goToThisPageOld(draggables.SideBar, draggables.SideBar.data.currentPage);
        draggables.MainPages.data.currentPage = 0;
        goToThisPage(draggables.MainPages, draggables.MainPages.data.currentPage, true);
    }

}, null, null, true, null, true);
CreateGenericScrollElement("scrollSpacer", "SideBar", null, null, null, null, null, null, null, null, true);
CreateGenericScrollElement("scrollButton", "SideBar", "Create Activity", null, null, pictureButtons.AddActivity.data.pressedFunction, null, null, false, null, true);
// endregion
//     Invitations region
// get an array of invitations and then loop through them and make the invitation scrollElements


// this part is for testing getting all the invitations from the taffy db
var tempAllInvitationIds = inAppDatabases.invitations().order("activityStartDate").select("invitationId");

function TestAddStuffToInvitations(theInvitationIdArray) {
    for (var i = 0; i < theInvitationIdArray.length; i++) {
        CreateActivityScrollElement(null, scrollers.Invitations, null, null, theInvitationIdArray[i]);
    }
}

TestAddStuffToInvitations(tempAllInvitationIds);
// endregion
//     AllUpdates region

// endregion
//     JoinedActivities region

// get an array of joinedActivies and then loop through them and make the invitation scrollElements

// this part is for testing getting all the invitations from the taffy db
var tempAllJoinedActivityIds = inAppDatabases.activities({
    isAttending: {
        is: true
    }
}).order("timeStarting").select("id");

function TestAddStuffToJoined(theActivityIdArray) {
    for (var i = 0; i < theActivityIdArray.length; i++) {
        CreateActivityScrollElement(theActivityIdArray[i], scrollers.JoinedActivities, null, null);
    }
}

TestAddStuffToJoined(tempAllJoinedActivityIds);
// endregion
//     CreatedActivities region
// get an array of Created activities and then loop through them and make the invitation scrollElements

var tempAllCreatedActivityIds = inAppDatabases.activities({
    createdBy: {
        is: theUser.id
    }
}).order("timeStarting").select("id");

function TestAddStuffToCreated(theActivityIdArray) {
    for (var i = 0; i < theActivityIdArray.length; i++) {
        CreateActivityScrollElement(theActivityIdArray[i], scrollers.CreatedActivities, null, null);
    }
}

TestAddStuffToCreated(tempAllCreatedActivityIds);

// endregion
//    CreateActivity
//     Info region

// endregion
//     Category region
for (var l = 0; l < categoryNames.length; l++) {
    CreateGenericCheckBox(categoryNames[l], scrollers.Category);
}
tickAGenericCheckBox(scrollers.Category, "other", false);



CreateGenericScrollElement("textInput", "Info", null, null, null, null, "Event Name");
CreateGenericScrollElement("textArea", "Info", null, null, null, null, "Description");

// endregion
//     Privacy region

CreateGenericCheckBox("Private", "Privacy");
CreateGenericCheckboxDescription("Only your friends can be invited", "Privacy", "Private");
CreateGenericCheckBox("Sharable", scrollers.Privacy);
CreateGenericCheckboxDescription("Friends of friends can invite other people", "Privacy", "Sharable");
CreateGenericCheckBox("Public", scrollers.Privacy);
CreateGenericCheckboxDescription("Anyone can come", "Privacy", "Public");

CreateGenericScrollElement("numberInput", "Privacy", "Days until data deletion");
CreateGenericCheckBox("Allow anonymous attendees", scrollers.Privacy, null, true);

tickAGenericCheckBox(scrollers.Privacy, "private", false);

CreateGenericScrollElement("numberInput", "Reach", "Attendee Target");
CreateGenericScrollElement("scrollDescription", "Reach", "The target amount of people");

// endregion
//     Where region
CreateGenericScrollElement("textInput", "Where", null, null, null, null, "Add a location...");

// endregion
//     When region
CreateDateInputScrollElement("Starts", scrollers.When);
CreateDateInputScrollElement("Ends", scrollers.When);

// endregion
//     Visuals region
CreateGenericScrollElement("scrollButton", "Visuals", "Choose a Picture", null, null, function () {
    $('#activityPictureFileInput').click();
});

// endregion
//     Reach region

// endregion
//     Settings
//     Settings region
CreateGenericScrollElement("scrollSpacer", "Settings");
CreateGenericScrollElement("scrollButton", "Settings", "Help/FAQ", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.HelpFAQ.view);
});
CreateGenericScrollElement("scrollButton", "Settings", "Contact Us", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.ContactUs.view);
});
CreateGenericScrollElement("scrollSpacer", "Settings");
CreateGenericScrollElement("scrollButton", "Settings", "Notifications", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.Notifications.view);
});
CreateGenericScrollElement("scrollSpacer", "Settings");
CreateGenericScrollElement("scrollButton", "Settings", "Privacy", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.Privacy.view);
});
CreateGenericScrollElement("scrollButton", "Settings", "Payment Info", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.PaymentInfo.view);
});
CreateGenericScrollElement("scrollSpacer", "Settings");
CreateGenericScrollElement("scrollButton", "Settings", "Change Number", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.ChangeNumber.view);
});
CreateGenericScrollElement("scrollButton", "Settings", "Delete my account", null, null, function () {
    settingsRenderController.show(theViews.SettingsPages.DeleteAccount.view);
});
CreateGenericScrollElement("scrollSpacer", "Settings");


// endregion
//     Profile region
//CreateGenericScrollElement("scrollSpacer,scrollers.Profile);
//CreateGenericScrollElement("scrollButton,scrollers.Profile, "Add Photo",null,null,function(){
//    settingsRenderController.show(theViews.SettingsPages.HelpFAQ.view);
//});
//CreateGenericScrollElement("textInput,scrollers.Profile, "Full Name", null, null, null, null);
// endregion
//     Interests region

// endregion
//     HelpFAQ region
CreateGenericScrollElement("scrollSpacer", "HelpFAQ", "TOP QUESTIONS");
CreateTextAccordion(("Accordion" + 0), scrollers.HelpFAQ, "An Accordion 0");
CreateGenericScrollElement("accordionDescription", "HelpFAQ", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
CreateTextAccordion(("Accordion" + 1), scrollers.HelpFAQ, "An Accordion 1");
CreateGenericScrollElement("accordionDescription", "HelpFAQ", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");
CreateTextAccordion(("Accordion" + 2), scrollers.HelpFAQ, "An Accordion 2");
CreateGenericScrollElement("accordionDescription", "HelpFAQ", "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.");

// endregion
//     ContactUs region

// endregion
//     GettingStarted region

// endregion
//     Suggestion region

// endregion
//     Notifications region

// endregion
//     SetPrivacy region

// endregion
//     PaymentInfo region

// endregion
//     ChangeNumber region

// endregion
//     OldNumber region

// endregion
//     NewNumber region

// endregion
//     DeleteAccount region

// endregion
//    Activity Pages
//     ActivityInfo region

// endregion
//     ActivityUpdatesEveryone region

// endregion
//     ActivityUpdatesFriends region

// endregion
//     ActivityUpdatesChannels region

// endregion
//    Brick Pages
//     Declined region

// endregion
//     ContactsPeople region

// Loop through all the contacts and add them to the contacts scrollers

CreateContactScrollElement(1, scrollers.ContactsPeople);
CreateContactScrollElement(2, scrollers.ContactsPeople);
CreateContactScrollElement(3, scrollers.ContactsPeople);
CreateContactScrollElement(4, scrollers.ContactsPeople);
CreateContactScrollElement(5, scrollers.ContactsPeople);

// endregion
//     ContactsGroups region
// Loop through all groups and add them to 

var tempAllContactIds = inAppDatabases.phoneContacts({
    createdBy: {
        is: theUser.id
    }
}).order("timeStarting").select("id");

function TestAddStuffToCreated(theActivityIdArray) {
    for (var i = 0; i < theActivityIdArray.length; i++) {
        CreateActivityScrollElement(theActivityIdArray[i], scrollers.CreatedActivities, null, null);
    }
}
TestAddStuffToCreated(tempAllCreatedActivityIds);

CreateGroupAccordionScrollElement(1, scrollers.ContactsGroups);
CreateGroupAccordionScrollElement(2, scrollers.ContactsGroups);

// endregion
//     PremadeStatuses region

// endregion

//  Adding Main Interface Parts ( scrollers, draggables, containers etc) region

// Adding ScrollViews
sideMenuContainer.add(scrollers.SideBar.modifier).add(scrollers.SideBar.scrollView);
theViews.MainPages.Updates.view.add(scrollers.AllUpdates.modifier).add(scrollers.AllUpdates.scrollView);
theViews.MainPages.Invites.view.add(scrollers.Invitations.modifier).add(scrollers.Invitations.scrollView);
theViews.MainPages.Joined.view.add(scrollers.JoinedActivities.modifier).add(scrollers.JoinedActivities.scrollView);
theViews.MainPages.Created.view.add(scrollers.CreatedActivities.modifier).add(scrollers.CreatedActivities.scrollView);

theViews.ActivityUpdates.Friends.view.add(scrollers.ActivityUpdatesFriends.modifier).add(scrollers.ActivityUpdatesFriends.scrollView);
theViews.ActivityUpdates.Everyone.view.add(scrollers.ActivityUpdatesEveryone.modifier).add(scrollers.ActivityUpdatesEveryone.scrollView);
theViews.ActivityUpdates.Custom.view.add(scrollers.ActivityUpdatesChannels.modifier).add(scrollers.ActivityUpdatesChannels.scrollView);

theViews.SettingsPages.Settings.view.add(scrollers.Settings.modifier).add(scrollers.Settings.scrollView);
theViews.SettingsPages.Profile.view.add(scrollers.Profile.modifier).add(scrollers.Profile.scrollView);
theViews.SettingsPages.Interests.view.add(scrollers.Interests.modifier).add(scrollers.Interests.scrollView);
theViews.SettingsPages.HelpFAQ.view.add(scrollers.HelpFAQ.modifier).add(scrollers.HelpFAQ.scrollView);
theViews.SettingsPages.ContactUs.view.add(scrollers.ContactUs.modifier).add(scrollers.ContactUs.scrollView);
theViews.SettingsPages.GettingStarted.view.add(scrollers.GettingStarted.modifier).add(scrollers.GettingStarted.scrollView);
theViews.SettingsPages.Suggestion.view.add(scrollers.Suggestion.modifier).add(scrollers.Suggestion.scrollView);
theViews.SettingsPages.Notifications.view.add(scrollers.Notifications.modifier).add(scrollers.Notifications.scrollView);
theViews.SettingsPages.Privacy.view.add(scrollers.SetPrivacy.modifier).add(scrollers.SetPrivacy.scrollView);
theViews.SettingsPages.PaymentInfo.view.add(scrollers.PaymentInfo.modifier).add(scrollers.PaymentInfo.scrollView);
theViews.SettingsPages.ChangeNumber.view.add(scrollers.ChangeNumber.modifier).add(scrollers.ChangeNumber.scrollView);
theViews.SettingsPages.OldNumber.view.add(scrollers.OldNumber.modifier).add(scrollers.OldNumber.scrollView);
theViews.SettingsPages.NewNumber.view.add(scrollers.NewNumber.modifier).add(scrollers.NewNumber.scrollView);
theViews.SettingsPages.DeleteAccount.view.add(scrollers.DeleteAccount.modifier).add(scrollers.DeleteAccount.scrollView);

theViews.CreateActivity.Info.view.add(scrollers.Info.modifier).add(scrollers.Info.scrollView);
theViews.CreateActivity.Category.view.add(scrollers.Category.modifier).add(scrollers.Category.scrollView);
theViews.CreateActivity.Privacy.view.add(scrollers.Privacy.modifier).add(scrollers.Privacy.scrollView);
theViews.CreateActivity.Where.view.add(scrollers.Where.modifier).add(scrollers.Where.scrollView);
theViews.CreateActivity.When.view.add(scrollers.When.modifier).add(scrollers.When.scrollView);
theViews.CreateActivity.Visuals.view.add(scrollers.Visuals.modifier).add(scrollers.Visuals.scrollView);
theViews.CreateActivity.Reach.view.add(scrollers.Reach.modifier).add(scrollers.Reach.scrollView);

theViews.PresetStatusPages.Time.view.add(scrollers.PremadeStatuses.modifier).add(scrollers.PremadeStatuses.scrollView);
theViews.PresetStatusPages.Mood.view.add(scrollers.PremadeStatusesMood.modifier).add(scrollers.PremadeStatusesMood.scrollView);

theViews.SignUpPages.VerifyPhone.view.add(scrollers.VerifyPhone.modifier).add(scrollers.VerifyPhone.scrollView);
theViews.SignUpPages.EnterCode.view.add(scrollers.EnterCode.modifier).add(scrollers.EnterCode.scrollView);

// Adding Other Parts Like Containers
appSections.MainPages.add(draggables.MainPages.containerModifier).add(draggables.MainPages.container);
appSections.CreateActivity.add(draggables.CreateActivity.containerModifier).add(draggables.CreateActivity.container);
activityContainers.UpdatesTabs.viewToShow.add(draggables.ActivityUpdates.containerModifier).add(draggables.ActivityUpdates.container);
theViews.BrickContents.Contacts.container.add(draggables.ContactsPages.containerModifier).add(draggables.ContactsPages.container);
activityContainers.UpdatesTabs.postStatusBrick.presetsContainer.add(draggables.PresetStatusPages.containerModifier).add(draggables.PresetStatusPages.container);

activityContainers.UpdatesTabs.renderController.show(activityContainers.UpdatesTabs.viewToShow);
goToThisPage(draggables.ActivityUpdates, draggables.ActivityUpdates.data.currentPage);
activityContainers.UpdatesTabs.viewToShow.add(tabsBars.ActivityUpdates.modifier).add(tabsBars.ActivityUpdates.container);
tabsBarLogic.CheckAndUpdateTabs(draggables.ActivityUpdates);

tabsBars.ActivityUpdates.container.mainContainer.tabsContainer.add(dotGroups.ActivityUpdates.modifier).add(dotGroups.ActivityUpdates.container);
dotLogic.CheckAndUpdateDots(draggables.ActivityUpdates);

theViews.BrickContents.Contacts.add(tabsBars.Contacts.modifier).add(tabsBars.Contacts.container);

tabsBars.Contacts.container.mainContainer.tabsContainer.add(dotGroups.Contacts.modifier).add(dotGroups.Contacts.container);
dotLogic.CheckAndUpdateDots(draggables.ContactsPages);

theViews.BrickContents.Contacts.add(tabsBars.Contacts.modifier).add(tabsBars.Contacts.container);

activityContainers.UpdatesTabs.postStatusBrick.presetsContainer.add(tabsBars.PresetStatusPages.modifier).add(tabsBars.PresetStatusPages.container);
tabsBars.PresetStatusPages.container.mainContainer.tabsContainer.add(dotGroups.PresetStatusPages.modifier).add(dotGroups.PresetStatusPages.container);
dotLogic.CheckAndUpdateDots(draggables.PresetStatusPages);
tabsBarLogic.CheckAndUpdateTabs(draggables.PresetStatusPages);

mainContext.add(draggables.SideBar.modifier).add(draggables.SideBar.draggable).add(sideMenuContainer);

appSections.CreateActivity.add(dotGroups.CreateActivity.modifier).add(dotGroups.CreateActivity.container);
dotLogic.CheckAndUpdateDots(draggables.CreateActivity);

appSections.MainPages.add(dotGroups.MainPages.modifier).add(dotGroups.MainPages.container);
dotLogic.CheckAndUpdateDots(draggables.MainPages);

mainContext /*.add(draggables.ActivitySmallBox.modifier)*/ .add(draggables.ActivitySmallBox.draggable).add(activityContainers.Main.container.chain) /*.add(activityContainers.Main.container.mod)*/ .add(activityContainers.Main.container);

mainContext.add(activityContainers.UpdatesTabs.postStatusBrick.modifier).add(activityContainers.UpdatesTabs.postStatusBrick.container);
// TODO This is around about where the Contacts mini bricks need to be added aswell!
mainContext.add(theViews.BrickContents.Contacts.ButtonBrick.modifier).add(theViews.BrickContents.Contacts.ButtonBrick.container);
mainContext.add(theViews.BrickContents.Contacts.TextBrick.modifier).add(theViews.BrickContents.Contacts.TextBrick.container);
//theViews.BrickContents.Contacts.ButtonBrick
// theViews.BrickContents.Contacts.TextBrick

draggables.ActivitySmallBox.data.currentPage = 0;
goToThisPageOld(draggables.ActivitySmallBox, draggables.ActivitySmallBox.data.currentPage);
activityContainers.Main.container.add(activityContainers.Main.container.renderController);
activityContainers.Main.renderController.show(theViews.ActivityPages.Picture.view);
activityContainers.Info.container.add(activityContainers.Info.renderController);
activityContainers.Info.renderController.show(theViews.ActivityPages.Info.view);

//endregion




//endregion
//====================================================-


//====================================================-
//When Inputs are Blurred region
//|55|8560|1085|711.8|398.06|1----------------------------------------------------------------------------------------------~
// What Happens When Inputs are Blurred
scrollers.Info.scrollElements[0].textInput.on('blur', function () {
    temporaryActivityObject.name = scrollers.Info.scrollElements[0].textInput.getValue();
});
scrollers.Info.scrollElements[1].textArea.on('blur', function () {
    temporaryActivityObject.description = scrollers.Info.scrollElements[1].textArea.getValue();
});
scrollers.Reach.scrollElements[0].numberInput.on('blur', function () {
    temporaryActivityObject.target = scrollers.Reach.scrollElements[0].numberInput.getValue();
});
scrollers.Privacy.scrollElements[6].numberInput.on('blur', function () {

    temporaryActivityObject.deletionDays = scrollers.Privacy.scrollElements[6].numberInput.getValue();

});
scrollers.Where.scrollElements[0].textInput.on('blur', function () {

    temporaryActivityObject.location = scrollers.Where.scrollElements[0].textInput.getValue();

});






//endregion
//====================================================-


//====================================================-
//On Keydown region
//|54|8111|1639|701.54|510.59|1----------------------------------------------------------------------------------------------~
Engine.on('keydown', function (e) {
    BlurTheDateInputBoxes();

    // the add new new status part should loop through the old scroll elements and get the latest timeUpdated from those, then if there are any statuses (after they've been synced) with a time thats later than the lastUpdated value from the scrollElements, then create and add that to the scroller (it will be the same create function, and it will animate it in smoothely) , this part can also loop through and check if any of the timeUpdated parts of the scroll elements are older (actually dw, this is all handled by the server or soemthing, outside of this part)

    // This works, may have to convert it from miliseconds to seconds though


    //-----------------------------------------------------
    // Set New Page Limits Based On Input while Creating an Activity region
    //|-1|100|100|500|500|1--------------------------------------------------~
    if (activityTitleIsFocused) {
        Timer.setTimeout(function () {
            var inputtedText = scrollers.Info.scrollElements[0].textInput.getValue();
            if (inputtedText.length > 0) {
                setNewLockedLimit(draggables.CreateActivity, 4);
            } else {
                setNewLockedLimit(draggables.CreateActivity, 1);
            }
        }, 10);
    }

    if (wherePartIsFocused) {
        Timer.setTimeout(function () {
            var inputtedText = scrollers.Info.scrollElements[0].textInput.getValue();

            if (inputtedText.length > 0) {
                setNewLockedLimit(draggables.CreateActivity, 5);
            } else {
                setNewLockedLimit(draggables.CreateActivity, 4);
            }
        }, 10);
    }

    // endregion
    //-----------------------------------------------------

    if (phoneNumberInputIsFocused) {
        Timer.setTimeout(function () {
            //     	isAValidPhoneNumberBoolean = isValidNumber( theUser.p1, "AU");
            isAValidPhoneNumberBoolean = isValidNumber(scrollers.VerifyPhone.scrollElements[1].numberInput.getValue(), "AU");
            console.log(textButtons.VerifyNext.view);
            console.log(isAValidPhoneNumberBoolean);
            if (isAValidPhoneNumberBoolean) {
                textButtons.VerifyNext.view.SetEnabled();
                //      	phoneNumberInputIsFocused 
            } else {
                textButtons.VerifyNext.view.SetDisabled();

            }
            // scrollers.VerifyPhone.scrollElements[1].numberInput    
        }, 10);

    }

    // Update a Group Title when editing a Group
    // All of this can be put into one function called "HandleGroupNameInput()" in the contacts logic part, that runs here
    if (contactsLogic.textIsFocused) {
        //TODO if there is actual stuff (that arent spaces) in the text box, then have the title change to that, if there is nothing in there, have the place holder text go back to th name of the group that is currently being edited (this will happen automatically, as the placeholder text wouldn't've been updated) and set the title of the contacts page to the name of the group that is currently being edited
        Timer.setTimeout(function () {
            var inputtedText = theViews.BrickContents.Contacts.TextBrick.textInput.getValue();
            theViews.BrickContents.Contacts.title.setContent(inputtedText);
        }, 10);
    }

    //  Searching Logic
    var tempCurrentFocusedTabBar = null;
    var tempRelatedScroller = null;
    if (tabsBars.ActivityUpdates.data.searchIsFocused) {
        tempCurrentFocusedTabBar = tabsBars.ActivityUpdates;
    } 
    else if (tabsBars.Contacts.data.searchIsFocused) {
        tempCurrentFocusedTabBar = tabsBars.Contacts;
    }
    if (tempCurrentFocusedTabBar) {
        Timer.setTimeout(function () {
            if (tempCurrentFocusedTabBar === tabsBars.ActivityUpdates) {
                if (draggables.ActivityUpdates.data.currentPage === 0) {
                    tempRelatedScroller = scrollers.ActivityUpdatesFriends;
                } else if (draggables.ActivityUpdates.data.currentPage === 1) {
                    tempRelatedScroller = scrollers.ActivityUpdatesEveryone;
                } else if (draggables.ActivityUpdates.data.currentPage === 2) {
                    tempRelatedScroller = scrollers.ActivityUpdatesChannels;
                }
            } else if (tempCurrentFocusedTabBar === tabsBars.Contacts) {
                if (draggables.ContactsPages.data.currentPage === 0) {
                    tempRelatedScroller = scrollers.ContactsPeople;
                } else if (draggables.ContactsPages.data.currentPage === 1) {
                    tempRelatedScroller = scrollers.ContactsGroups;
                }
            }

            var currentSeachText = tempCurrentFocusedTabBar.container.mainContainer.searchBarContainer.inputSurface.getValue();
            var tempSearchedScrollerDb = TAFFY(tempRelatedScroller.scrollElements);
            var tempMatchedStatusBundles = tempSearchedScrollerDb({
                name: {
                    like: currentSeachText.toLowerCase()
                }
            }).get();

            for (var k = 0; k < tempRelatedScroller.scrollElements.length; k++) {
                tempRelatedScroller.scrollElements[k].markToHide = false;
            }
            for (var i = 0; i < tempRelatedScroller.scrollElements.length; i++) {
                if (tempMatchedStatusBundles.length > 0) {

                    for (var j = 0; j < tempMatchedStatusBundles.length; j++) {

                        if (tempRelatedScroller.scrollElements[i].searchId === tempMatchedStatusBundles[j].searchId) {
                            tempRelatedScroller.scrollElements[i].markToHide = false;
                            break;
                        } else {
                            if (tempRelatedScroller === scrollers.ContactsGroups) {
                                // if the scrollElement type is a contact, then check if that contact is in any groups, and if it is , mark those groups to not be hidden
                                if (tempRelatedScroller.scrollElements[i].groupsContacts) {
                                    // loop through the groupContacts
                                    for (var m = 0; m < tempRelatedScroller.scrollElements[i].groupsContacts.length; m++) {
                                        if (tempRelatedScroller.scrollElements[i].groupsContacts[m].includesContact === tempMatchedStatusBundles[j].searchId) {
                                            tempRelatedScroller.scrollElements[i].markToHide = false;
                                            break;
                                        } else {
                                            tempRelatedScroller.scrollElements[i].markToHide = true;
                                        }
                                    }
                                } else {
                                    tempRelatedScroller.scrollElements[i].markToHide = true;
                                }
                            } else {
                                tempRelatedScroller.scrollElements[i].markToHide = true;
                            }
                        }
                    }
                } else {
                    tempRelatedScroller.scrollElements[i].markToHide = true;
                }
            }

            for (var l = 0; l < tempRelatedScroller.scrollElements.length; l++) {
                if (tempRelatedScroller.scrollElements[l].markToHide === true) {
                    hideAScrollElement(l, tempRelatedScroller, false);
                } else if (tempRelatedScroller.scrollElements[l].markToHide === false) {
                    showAScrollElement(l, tempRelatedScroller);
                }
            }
        }, 10);
    }

    // Check the word count of a status post
    if (activityContainers.UpdatesTabs.postStatusBrick.isUp === true) {
        var tempStatusLettersAmount = maxStatusLetters - activityContainers.UpdatesTabs.postStatusBrick.largeContainer.textArea.getValue().length;
        activityContainers.UpdatesTabs.postStatusBrick.largeContainer.remainingLettersText.setContent(tempStatusLettersAmount);
        if (tempStatusLettersAmount < 0) {
            activityContainers.UpdatesTabs.postStatusBrick.largeContainer.remainingLettersText.setProperties({
                color: "red"
            });
        } else {
            // FIXME this could possibly be updated so it doesn't RE-set the colour on every key press if it doesn't need to
            activityContainers.UpdatesTabs.postStatusBrick.largeContainer.remainingLettersText.setProperties({
                color: themeColors.dark
            });
        }
    }
    activityContainers.Info.sharingPart.leftContainer.renderController.show(activityContainers.Info.sharingPart.leftContainer.inContainer);
});



//endregion
//====================================================-


//====================================================-
//Resize And Store/Save Image region
//|38|7879|3191|500|500|1----------------------------------------------------------------------------------------------~

// this function takes in the type of thing it's getting an image for (an activity picture or a users picture) and returns the image dataUrl for the image

// if there are issues with the app sending the data to the server before the image is gotten, there could be a variable that marks the state of the image getting, when and image upload button is clicked, the isSelectingImage variable is marked as true, and then when it's marked as false, the server can send the data to the server? (it's marked as false in the imgObj.onload part

//at the moment , the app relies on the user to not immediately send the data to the server 
// one way around this could be to make the syncQue check if the imageHasBeenSelected variable is true and only run the sync command if it is, if it isn't it could set to run the sync command in another 500ms? and when the synccommand is eventually sent , it stops checking and retrying to run the sync commands?

var thePicture;
var imageObj = new Image();
var isActivityPicture = true;

var canvas = document.createElement("canvas");
canvas.width = requestedWidthActivity;
canvas.height = requestedHeightActivity;
var imageDataUrl;
var context = canvas.getContext('2d');

function readTheImageURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();
        reader.onload = function (e) {
            thePicture = e.target.result;
            imageObj.src = thePicture;
        };
        reader.readAsDataURL(input.files[0]);
    }
}

$("#activityPictureFileInput").change(function () {
    isActivityPicture = true;
    heightToWidthRatio = (requestedHeightActivity / requestedWidthActivity);
    widthToHeightRatio = (requestedWidthActivity / requestedHeightActivity);
    canvas.width = requestedWidthActivity;
    canvas.height = requestedHeightActivity;
    //    alert("The Change part has worked");
    if (!imageObj) {
        imageObj = new Image();
    }
    readTheImageURL(this);
});

$("#userPictureFileInput").change(function () {
    isActivityPicture = false;
    heightToWidthRatio = 1;
    widthToHeightRatio = 1;
    canvas.width = requestedWidthUser;
    canvas.height = requestedHeightUser;
    //    alert("The Change part has worked");
    if (!imageObj) {
        imageObj = new Image();
    }
    readTheImageURL(this);
});

imageObj.onload = function () {
    console.log("The width to height ratio!");
    console.log(widthToHeightRatio);
    var sourceX, sourceY, sourceWidth, sourceHeight;
    var destWidth;
    var destHeight;
    var destX = 0;
    var destY = 0;

    if (isActivityPicture) {
        destWidth = requestedWidthActivity;
        destHeight = requestedHeightActivity;
    } else {
        destWidth = requestedWidthUser;
        destHeight = requestedHeightUser;
    }

    console.log("The heigt and width of the loaded image");
    console.log(imageObj.height);
    console.log(imageObj.width);

    if ((imageObj.height) > (imageObj.width * heightToWidthRatio)) {
        sourceX = 0;
        sourceY = (imageObj.height / 2) - ((imageObj.width * heightToWidthRatio) / 2);
        sourceWidth = imageObj.width;
        sourceHeight = imageObj.width * heightToWidthRatio;
    } else if ((imageObj.height) <= (imageObj.width * heightToWidthRatio)) {
        sourceX = (imageObj.width / 2) - ((imageObj.height * widthToHeightRatio) / 2);
        sourceY = 0;
        sourceWidth = imageObj.height * widthToHeightRatio;
        sourceHeight = imageObj.height;
    }

    context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
    imageDataUrl = canvas.toDataURL();
    console.log("image was loaded!");

    if (isActivityPicture) {
        temporaryActivityObject.pictureDataUrl = imageDataUrl;
        //            temporaryActivityObject.dateCreated = Math.floor(Date.now() / 1000);
        ////            temporaryActivityObject.allowAnonymous = scrollers.Where.scrollElements[7].isTicked;
        //            temporaryActivityObject.allowAnonymous = true ? 1 : 0;
    } else {
        // if it's a user image
        temporaryUserObject.pictureDataUrl = imageDataUrl;
        theViews.SettingsPages.Profile.updateImage();
    }

};






//endregion
//====================================================-


//====================================================-
//Testing Area region
//|28|4888|2873|500|500|1----------------------------------------------------------------------------------------------~
// Re-Create the users inAppDatabase based on the connected users from the placeholder data (copied from the server?)
// also gotta edit the server data to associate those contacts (two of them) to mobilizr users (and edit the mobilizr users names so they match the associated contacts? (no entries need to be made to the realtions table because each contact is only connected to one mobilizr user)

//inAppDatabases.mobilizrUsers = TAFFY({});

// Testing storing the TaffyDB in local storage through TaffyDB's built in implementation, may also be able to look more into the path the img cache saves into now that it's known how localStorage now works (with one name per localStorage string entry

var testAddableActivity = '{ "id": "ActivityId4", "name": "Activity Four", "category": "Category1", "description": "This is a description", "videoLink": null, "picture": "./id_1112.png", "createdBy": "7", "dateCreated": 1421071637, "location": "A Location", "timeStarting": 1421071637, "timeEnding": 1421071637, "target": null, "peopleJoined": 56, "peopleInvited": 110, "privacy": 0}';

//localStorage.clear();
//if (!localStorage.taffy_activities) {
//    inAppDatabases.activities.store("activities");
//}
//localStorage.clear();

//inAppDatabases.activities.insert(JSON.parse(testAddableActivity));
//inAppDatabases.activities.insert({TestInsert: "Value"});

// these don't currently have a server id part ?
// Activity in-app database functions?
// This can also be all the syncing functions too? since most of them will sync to the server and then from the server sync back to the in-app database
// If a user creates an activity it wont show up until they are online? OR it just gives it a temporary id (that goes up, for example "tempOflineActivityId1" , then that is the thing that is sent to the server, except it's id isn't send with it, then when it's sent, it recieves back the activityId assigned to it by the server, and changes the activityId of the in-app database activity

// Testing LocalStorage, Turns out the local storage part can be automatically handled by the TaffyDB Library ;) :)
// Each of the in-app databases can be saved in json format to the local storage every time theyre edited
//localStorage.myName = "hugo hugo hugo";
//tempSavedAppDatabases = {};
//tempSavedAppDatabases.phoneContacts = "hugo hugo hugo 2";
//localStorage.savedAppDatabases = JSON.stringify(tempSavedAppDatabases);
//localStorage.databaseTest = JSON.stringify(inAppDatabases.activities().get());

// An object with all the users settings can be saved to localStorage

// The user info object? can be saved to local storage (the phone users user Data won't be saved to the in-app database , the users image is the img cache path



//SendNewUserToServer();
//SyncPhoneContactsToInAppContacts();

function TestFunctionForSyncCommand(alertThis) {
    //    alert(alertThis);  
}

for (var i = 0; i < 9; i++) {
    AddNewActivityInTheApp();
}
//InterpretAndRunSyncCommand(syncCommandQueue[0]);
for (var j = 0; j < syncCommandQueue.length; j++) {
    InterpretAndRunSyncCommand(syncCommandQueue[j]);
}

// This gets all the statuse id's
var tempAllStatuses = inAppDatabases.statuses().order("timeUpdated").select("statusId");

//RemoveActivity("ActivityId1");
//inAppDatabases.phoneContacts = TAFFY([]);
// ifOnMobilizr isn't a property anymore, insteaad it should check if the mobilizrId part isn't blank
//var onlyOnMobilizr = inAppDatabases.phoneContacts({isOnMobilizr:true}).get();
//var theActivityObject = inAppDatabases.activities({id:"ActivityId2"}).first();
//var theActivityConnection = inAppDatabases.activityConnections({activityId:"ActivityId1"}).select("includesUser");
//var theStatusObject = inAppDatabases.statuses({statusId:"StatusId3"}).first();


AddElementsToAllUpdates(tempAllStatuses);
// these should happen every time a new activity is opened
AddElementsToFriendsChanel(1);
AddElementsToEveryoneChannel(1);

//FIXME A working hack for now, somewhere else it sets the current page for the IntroPanelPanels to 1
goToThisPage(draggables.IntroPanelsPanels, 0, true);




// Do Code after 1 second
Timer.setTimeout(function () {
    scrollviewHasBeenTurnedOff = false;
}, 1000);


    inAppDatabases.statuses.insert({
        statusId: 6,
        activityId: 1,
        statusType: 0,
        statusText: "I'm at the fridge , here's a new status",
        timeUpdated: "1425120569",
        amountOfTimesUpdated: 1,
        fromUserId: 1,
        toUserId: null,
        channelPostedTo: 1
    });

    inAppDatabases.statuses.insert({
        statusId: 7,
        activityId: 1,
        statusType: 1,
        statusText: "The Toilets Are Leaking, please use the portaloos, thanks",
        timeUpdated: "1425141881",
        amountOfTimesUpdated: 1,
        fromUserId: 3,
        toUserId: null,
        channelPostedTo: 1
    });



//endregion
//====================================================-


//====================================================-
// Todo region
//|50|6874|-1596|500|200|0----------------------------------------------------------------------------------------------~
//// The typing status part generally works on a phone however, with certain timing the screen still focuses downward on the text input
//
//// in the contacts page, the text input doesnt float above on onscreen keyboard at all (like it does with the status
//// I think to fix it instead of adding the mini bricks to the contacts page, they need to be added to the main view (and be hidden when the contacts page is hidden too)
//
////Stop click throughs happening on the tick boxes for groups
//
//// There's no spacing for the last status in the Activity Updates / Syncs area


//endregion
//====================================================-