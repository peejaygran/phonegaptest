"use strict";

//====================================================-
//Import Dependancies region
//|53|-809|-1115|731.99|392.84|1----------------------------------------------------------------------------------------------~
/*jshint undef: false*/

// Mobilizr Import Dependancies
// Hugo McPhee 2015

moment().format();

// Load native UI library
//var gui = require('nw.gui'); //or global.window.nwDispatcher.requireNwGui() (see https://github.com/rogerwang/node-webkit/issues/707)
// Get the current window
//var win = gui.Window.get();
//win.setAlwaysOnTop(true);
//win.showDevTools();

/*global famous*/
// import dependencies
var Engine = famous.core.Engine;
var Modifier = famous.core.Modifier;
var Transform = famous.core.Transform;
var ImageSurface = famous.surfaces.ImageSurface;

var Entity = famous.core.Entity;

var StateModifier = famous.modifiers.StateModifier;
var Draggable = famous.modifiers.Draggable;
var Transitionable = famous.transitions.Transitionable;
var SnapTransition = famous.transitions.SnapTransition;
var WallTransition = famous.transitions.WallTransition;
var Easing = famous.transitions.Easing;
var View = famous.core.View;
var Modifier = famous.core.Modifier;
var Surface = famous.core.Surface;
var Transform = famous.core.Transform;
var InputSurface = famous.surfaces.InputSurface;
var TextareaSurface = famous.surfaces.TextareaSurface;
var ContainerSurface = famous.surfaces.ContainerSurface;
var ImageSurface = famous.surfaces.ImageSurface;
var Scrollview = famous.views.Scrollview;
var Timer = famous.utilities.Timer;
var Easing = famous.transitions.Easing;
var RenderNode = famous.core.RenderNode;
var RenderController = famous.views.RenderController;
var MouseSync = famous.inputs.MouseSync;
var ModifierChain = famous.modifiers.ModifierChain;
var Flipper = famous.views.Flipper;
//var FastClick          = famous.inputs.FastClick;
//var SizeAwareView = famous.views.SizeAwareView;

var LayoutController = famousflex.LayoutController;
var CollectionLayout = famousflex.layouts.CollectionLayout;
var DatePicker = famousflex.widgets.DatePicker;

var FlexScrollView = famousflex.FlexScrollView;
var ListLayout = famousflex.layouts.ListLayout;

//    var SequentialLayout = famous.views.SequentialLayout;


//endregion
//====================================================-
