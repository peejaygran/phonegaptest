/*jshint undef: false*/

// Mobilizr Resize and post image
// Hugo McPhee 2015

//================================================================================
//region Description
//================================================================================

// this function takes in the type of thing it's getting an image for (an activity picture or a users picture) and returns the image dataUrl for the image

// if there are issues with the app sending the data to the server before the image is gotten, there could be a variable that marks the state of the image getting, when and image upload button is clicked, the isSelectingImage variable is marked as true, and then when it's marked as false, the server can send the data to the server? (it's marked as false in the imgObj.onload part

//at the moment , the app relies on the user to not immediately send the data to the server 
// one way around this could be to make the syncQue heck if the imageHasBeenSelected variable is true and only run the sync command if it is, if it isn't it could set to run the sync command in another 500ms? and when the synccommand is eventually sent , it stops checking and retrying to run the sync commands?

//endregion
//================================================================================

//================================================================================
//region Resize And Store/Save Image
//================================================================================

    var thePicture;
    var imageObj = new Image();
    var isActivityPicture = true;

    var canvas = document.createElement("canvas");
    canvas.width = requestedWidthActivity;
    canvas.height = requestedHeightActivity;
    var imageDataUrl;
    var context = canvas.getContext('2d');

    function ReadTheImageURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function (e) {
                thePicture = e.target.result;
                imageObj.src = thePicture;
            };
            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#activityPictureFileInput").change(function () {
        isActivityPicture = true;
        heightToWidthRatio = (requestedHeightActivity / requestedWidthActivity);
        widthToHeightRatio = (requestedWidthActivity / requestedHeightActivity);
        canvas.width = requestedWidthActivity;
        canvas.height = requestedHeightActivity;
        //    alert("The Change part has worked");
        if (!imageObj) {
            imageObj = new Image();
        }
        ReadTheImageURL(this);
    });

    $("#userPictureFileInput").change(function () {
        isActivityPicture = false;
        heightToWidthRatio = 1;
        widthToHeightRatio = 1;
        canvas.width = requestedWidthUser;
        canvas.height = requestedHeightUser;
        //    alert("The Change part has worked");
        if (!imageObj) {
            imageObj = new Image();
        }
        ReadTheImageURL(this);
    });

    imageObj.onload = function () {
        console.log("The width to height ratio!");
        console.log(widthToHeightRatio);
        var sourceX, sourceY, sourceWidth, sourceHeight;
        var destWidth;
        var destHeight;
        var destX = 0;
        var destY = 0;
        
        if (isActivityPicture) {
            destWidth = requestedWidthActivity;
            destHeight = requestedHeightActivity;
        }
        else {
            destWidth = requestedWidthUser;
            destHeight = requestedHeightUser; 
        }
        
        console.log("The heigt and width of the loaded image");
        console.log(imageObj.height);
        console.log(imageObj.width);
        
        if ((imageObj.height) > (imageObj.width * heightToWidthRatio)) {
            sourceX = 0;
            sourceY = (imageObj.height / 2) - ((imageObj.width * heightToWidthRatio) / 2);
            sourceWidth = imageObj.width;
            sourceHeight = imageObj.width * heightToWidthRatio;
        } 
        else if ((imageObj.height) <= (imageObj.width * heightToWidthRatio)) {
            sourceX = (imageObj.width / 2) - ((imageObj.height * widthToHeightRatio) / 2);
            sourceY = 0;
            sourceWidth = imageObj.height * widthToHeightRatio;
            sourceHeight = imageObj.height;
        }

        context.drawImage(imageObj, sourceX, sourceY, sourceWidth, sourceHeight, destX, destY, destWidth, destHeight);
        imageDataUrl = canvas.toDataURL();
        alert("done!");
        
        if (isActivityPicture) {
            temporaryActivityObject.pictureDataUrl = imageDataUrl;
//            temporaryActivityObject.dateCreated = Math.floor(Date.now() / 1000);
////            temporaryActivityObject.allowAnonymous = scrollers.Where.scrollBundles[7].isTicked;
//            temporaryActivityObject.allowAnonymous = true ? 1 : 0;
        }
        else {
            // if it's a user image
            temporaryUserObject.pictureDataUrl = imageDataUrl;
            theViews.SettingsPages.Profile.updateImage();
        }
        
    };


//endregion
//================================================================================
